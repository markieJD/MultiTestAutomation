using Core;
using OpenQA.Selenium;

namespace AppTargets.Forms
{
    public class SliderPage : FormBase
    {
        public SliderPage() : base(By.Id("Slider"), "Slider page")
        {
            /// Add Elements
            /// 
            Elements.Add("ID", By.Id("sliderContainer"));

            Elements.Add("slider", By.ClassName("range-slider"));
            Elements.Add("slider value", By.Id("sliderValue"));
        }
    }
}

