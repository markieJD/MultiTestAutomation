using Core;
using OpenQA.Selenium;

namespace AppTargets.Forms
{
    public class DatePickerPage : FormBase
    {
        public DatePickerPage() : base(By.Id("DatePicker"), "DatePicker page")
        {
            /// Add Elements datePickerMonthYearInput dateAndTimePickerInput

            Elements.Add("ID", By.Id("datePickerMonthYearInput"));

            Elements.Add("select date", By.Id("datePickerMonthYearInput"));
            Elements.Add("date and time", By.Id("dateAndTimePickerInput"));

            Elements.Add("select date month dropdown", By.ClassName("react-datepicker__month-select"));
            Elements.Add("select date year dropdown", By.ClassName("react-datepicker__year-select"));
            
            Elements.Add("date and time month dropdown", By.ClassName("react-datepicker__month-dropdown-container"));
            Elements.Add("date and time year dropdown", By.ClassName("react-datepicker__year-dropdown-container"));
        }
    }
}

