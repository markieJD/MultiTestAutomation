using Core;
using OpenQA.Selenium;

namespace AppTargets.Forms
{
    public class SelectMenuPage : FormBase
    {
        public SelectMenuPage() : base(By.Id("SelectMenu"), "SelectMenu page")
        {
            Elements.Add("ID", By.XPath("//div[text()='Select Menu']"));
            /// Add Elements

            Elements.Add("select value", By.Id("withOptGroup"));
            Elements.Add("select one", By.Id("selectOne"));
            Elements.Add("old style select menu", By.Id("oldSelectMenu"));
            Elements.Add("multiselect drop down", By.XPath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[2]/div[7]/div[1]/div[1]"));
            Elements.Add("standard multi select", By.Id("cars"));

        }
    }
}
