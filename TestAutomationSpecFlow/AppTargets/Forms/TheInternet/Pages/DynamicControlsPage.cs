using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class DynamicControlsPage : FormBase 
      {
          public DynamicControlsPage() : base(By.Id("DynamicControls"), "DynamicControls page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));

                            Elements.Add("checkbox-example form", By.Id("checkbox-example"));
                               Elements.Add("checkbox", By.Id("checkbox"));
                                  Elements.Add("null 25 input", By.XPath("//input"));
                                  Elements.Add("remove", By.XPath("//button[contains(text(),'Remove')]"));
                                    Elements.Add("message", By.Id("message"));
                               Elements.Add("input-example form", By.Id("input-example"));
                                  Elements.Add("textbox", By.XPath("//form[@id='input-example']/input"));
                                  Elements.Add("enable button", By.XPath("//button[contains(text(),'Enable')]"));
            Elements.Add("spinner", By.Id("loading"));

             // Page Dictionary

         }
      }
  }

