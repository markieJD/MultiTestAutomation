using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class JavaScriptAlertsPage : FormBase 
      {
          public JavaScriptAlertsPage() : base(By.Id("JavaScriptAlerts"), "JavaScriptAlerts page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("null 14 body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img"));
                      Elements.Add("content div", By.Id("content"));
                            Elements.Add("null 20 header3", By.XPath("//h3"));
                            Elements.Add("null 21 section", By.XPath("//p"));
                            Elements.Add("null 22 ul", By.XPath("//ul"));
                               Elements.Add("null 23 list", By.XPath("//li"));
                                  Elements.Add("click for js alert button", By.XPath("//button[text()='Click for JS Alert']"));
                               Elements.Add("null 25 list", By.XPath("//li"));
                                  Elements.Add("click for js confirm", By.XPath("//button[text()='Click for JS Confirm']"));
                               Elements.Add("null 27 list", By.XPath("//li"));
                                  Elements.Add("click for js prompt", By.XPath("//button[text()='Click for JS Prompt']"));
                            Elements.Add("null 29 header4", By.XPath("//h4"));
                            Elements.Add("result section", By.Id("result"));
                   Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 31 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

