using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class DynamicContentPage : FormBase 
      {
          public DynamicContentPage() : base(By.Id("DynamicContent"), "DynamicContent page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("null 14 body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img"));
                      Elements.Add("content div", By.Id("content"));
                         Elements.Add("null 19 style", By.XPath("//style"));
                            Elements.Add("null 21 header3", By.XPath("//h3"));
                            Elements.Add("null 22 section", By.XPath("//p"));
                            Elements.Add("null 23 section", By.XPath("//p"));
                               Elements.Add("null 24 code", By.XPath("//code"));
                               Elements.Add("click here link", By.XPath("//a[text() = 'click here']"));
                            Elements.Add("null 25 hr", By.XPath("//hr"));
                               Elements.Add("content 1 div", By.Id("content"));
                                        Elements.Add("image 1", By.XPath("//div[1]/div[1]/img[1]"));
                                        Elements.Add("image 2", By.XPath("//div[2]/div[1]/img[1]"));
                                        Elements.Add("image 3", By.XPath("//div[3]/div[1]/img[1]"));
                                        Elements.Add("text 1", By.XPath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[2]"));
                                        Elements.Add("text 2", By.XPath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]"));
                                        Elements.Add("text 3", By.XPath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]/div[2]"));
            Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 48 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

