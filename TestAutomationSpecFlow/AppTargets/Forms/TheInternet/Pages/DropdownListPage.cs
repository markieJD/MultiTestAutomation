using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class DropdownListPage : FormBase 
      {
          public DropdownListPage() : base(By.Id("DropdownList"), "DropdownList page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("null 14 body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img"));
                      Elements.Add("content div", By.Id("content"));
                            Elements.Add("null 20 header3", By.XPath("//h3"));
                            Elements.Add("dropdown select", By.Id("dropdown"));
                               Elements.Add("please select an option option", By.XPath("//option[text() = 'Please select an option']"));
                               Elements.Add("option 1 option", By.XPath("//option[text() = 'Option 1']"));
                               Elements.Add("option 2 option", By.XPath("//option[text() = 'Option 2']"));
                   Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 22 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

