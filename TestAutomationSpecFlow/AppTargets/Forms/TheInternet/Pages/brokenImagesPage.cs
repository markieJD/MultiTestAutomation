using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class brokenImagesPage : FormBase 
      {
          public brokenImagesPage() : base(By.Id("brokenImages"), "brokenImages page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("null 14 body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img[0]"));
                      Elements.Add("content div", By.Id("content"));
                         Elements.Add("null 19 style", By.XPath("//style"));
                            Elements.Add("null 21 header3", By.XPath("//h3"));
                            Elements.Add("image 1", By.XPath("//img[1]"));
                            Elements.Add("image 2", By.XPath("//img[2]"));
                            Elements.Add("image 3", By.XPath("//img[3]"));
                   Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 26 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

