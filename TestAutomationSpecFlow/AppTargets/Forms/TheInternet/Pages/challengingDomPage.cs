using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class challengingDomPage : FormBase 
      {
          public challengingDomPage() : base(By.Id("challengingDom"), "challengingDom page")
         {
              /// Add Elements all names in lower case
             Elements.Add("html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img"));
                      Elements.Add("content div", By.Id("content"));
                            Elements.Add("null 20 header3", By.XPath("//h3"));
                            Elements.Add("null 21 section", By.XPath("//p"));
                            Elements.Add("null 22 hr", By.XPath("//hr"));
                                     Elements.Add("baz link", By.Id("ff536680-de86-013a-1967-72f40ae447d5"));
                                     Elements.Add("qux link", By.Id("ff537d00-de86-013a-1968-72f40ae447d5"));
                                     Elements.Add("qux 1 link", By.Id("ff53a1d0-de86-013a-1969-72f40ae447d5"));
                                     Elements.Add("latin table", By.XPath("//table"));
                                        Elements.Add("latin thead", By.XPath("//thead"));
                                        Elements.Add("null 40 tbody", By.XPath("//tbody"));
                                                 Elements.Add("edit link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 1 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 1 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 2 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 2 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 3 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 3 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 4 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 4 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 5 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 5 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 6 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 6 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 7 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 7 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 8 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 8 link", By.XPath("//a[text() = 'delete']"));
                                                 Elements.Add("edit 9 link", By.XPath("//a[text() = 'edit']"));
                                                 Elements.Add("delete 9 link", By.XPath("//a[text() = 'delete']"));
                                           Elements.Add("answer", By.Id("canvas"));
                            Elements.Add("null 123 hr", By.XPath("//hr"));
                   Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 125 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

