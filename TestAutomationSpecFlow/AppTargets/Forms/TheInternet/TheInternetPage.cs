using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class TheInternetPage : FormBase 
      {
          public TheInternetPage() : base(By.Id("TheInternet"), "TheInternet page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("the internet title", By.XPath("//title[text() = 'The Internet']"));
         Elements.Add("ID", By.XPath("//title[text() = 'The Internet']"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("null 12 style", By.XPath("//style"));
                Elements.Add("null 14 body", By.XPath("//body"));
                      Elements.Add("flash-messages div", By.Id("flash-messages"));
                      Elements.Add("null 17 link", By.XPath("//a"));
                         Elements.Add("null 18 image", By.XPath("//img"));
                      Elements.Add("content div", By.Id("content"));
                         Elements.Add("null 19 style", By.XPath("//style"));
                         Elements.Add("null 20 header1", By.XPath("//h1"));
                         Elements.Add("null 21 header2", By.XPath("//h2"));
                         Elements.Add("null 22 ul", By.XPath("//ul"));
                            Elements.Add("null 23 list", By.XPath("//li"));
                               Elements.Add("a/b testing link", By.XPath("//a[text() = 'A/B Testing']"));
                            Elements.Add("null 24 list", By.XPath("//li"));
                               Elements.Add("add/remove elements link", By.XPath("//a[text() = 'Add/Remove Elements']"));
                            Elements.Add("null 25 list", By.XPath("//li"));
                               Elements.Add("basic auth link", By.XPath("//a[text() = 'Basic Auth']"));
                            Elements.Add("null 26 list", By.XPath("//li"));
                               Elements.Add("broken images link", By.XPath("//a[text() = 'Broken Images']"));
                            Elements.Add("null 27 list", By.XPath("//li"));
                               Elements.Add("challenging dom link", By.XPath("//a[text() = 'Challenging DOM']"));
                            Elements.Add("null 28 list", By.XPath("//li"));
                               Elements.Add("checkboxes link", By.XPath("//a[text() = 'Checkboxes']"));
                            Elements.Add("null 29 list", By.XPath("//li"));
                               Elements.Add("context menu link", By.XPath("//a[text() = 'Context Menu']"));
                            Elements.Add("null 30 list", By.XPath("//li"));
                               Elements.Add("digest authentication link", By.XPath("//a[text() = 'Digest Authentication']"));
                            Elements.Add("null 31 list", By.XPath("//li"));
                               Elements.Add("disappearing elements link", By.XPath("//a[text() = 'Disappearing Elements']"));
                            Elements.Add("null 32 list", By.XPath("//li"));
                               Elements.Add("drag and drop link", By.XPath("//a[text() = 'Drag and Drop']"));
                            Elements.Add("null 33 list", By.XPath("//li"));
                               Elements.Add("dropdown link", By.XPath("//a[text() = 'Dropdown']"));
                            Elements.Add("null 34 list", By.XPath("//li"));
                               Elements.Add("dynamic content link", By.XPath("//a[text() = 'Dynamic Content']"));
                            Elements.Add("null 35 list", By.XPath("//li"));
                               Elements.Add("dynamic controls link", By.XPath("//a[text() = 'Dynamic Controls']"));
                            Elements.Add("null 36 list", By.XPath("//li"));
                               Elements.Add("dynamic loading link", By.XPath("//a[text() = 'Dynamic Loading']"));
                            Elements.Add("null 37 list", By.XPath("//li"));
                               Elements.Add("entry ad link", By.XPath("//a[text() = 'Entry Ad']"));
                            Elements.Add("null 38 list", By.XPath("//li"));
                               Elements.Add("exit intent link", By.XPath("//a[text() = 'Exit Intent']"));
                            Elements.Add("null 39 list", By.XPath("//li"));
                               Elements.Add("file download link", By.XPath("//a[text() = 'File Download']"));
                            Elements.Add("null 40 list", By.XPath("//li"));
                               Elements.Add("file upload link", By.XPath("//a[text() = 'File Upload']"));
                            Elements.Add("null 41 list", By.XPath("//li"));
                               Elements.Add("floating menu link", By.XPath("//a[text() = 'Floating Menu']"));
                            Elements.Add("null 42 list", By.XPath("//li"));
                               Elements.Add("forgot password link", By.XPath("//a[text() = 'Forgot Password']"));
                            Elements.Add("null 43 list", By.XPath("//li"));
                               Elements.Add("form authentication link", By.XPath("//a[text() = 'Form Authentication']"));
                            Elements.Add("null 44 list", By.XPath("//li"));
                               Elements.Add("frames link", By.XPath("//a[text() = 'Frames']"));
                            Elements.Add("null 45 list", By.XPath("//li"));
                               Elements.Add("geolocation link", By.XPath("//a[text() = 'Geolocation']"));
                            Elements.Add("null 46 list", By.XPath("//li"));
                               Elements.Add("horizontal slider link", By.XPath("//a[text() = 'Horizontal Slider']"));
                            Elements.Add("null 47 list", By.XPath("//li"));
                               Elements.Add("hovers link", By.XPath("//a[text() = 'Hovers']"));
                            Elements.Add("null 48 list", By.XPath("//li"));
                               Elements.Add("infinite scroll link", By.XPath("//a[text() = 'Infinite Scroll']"));
                            Elements.Add("null 49 list", By.XPath("//li"));
                               Elements.Add("inputs link", By.XPath("//a[text() = 'Inputs']"));
                            Elements.Add("null 50 list", By.XPath("//li"));
                               Elements.Add("jquery ui menus link", By.XPath("//a[text() = 'JQuery UI Menus']"));
                            Elements.Add("null 51 list", By.XPath("//li"));
                               Elements.Add("javascript alerts link", By.XPath("//a[text() = 'JavaScript Alerts']"));
                            Elements.Add("null 52 list", By.XPath("//li"));
                               Elements.Add("javascript onload event error link", By.XPath("//a[text() = 'JavaScript onload event error']"));
                            Elements.Add("null 53 list", By.XPath("//li"));
                               Elements.Add("key presses link", By.XPath("//a[text() = 'Key Presses']"));
                            Elements.Add("null 54 list", By.XPath("//li"));
                               Elements.Add("large & deep dom link", By.XPath("//a[text() = 'Large & Deep DOM']"));
                            Elements.Add("null 55 list", By.XPath("//li"));
                               Elements.Add("multiple windows link", By.XPath("//a[text() = 'Multiple Windows']"));
                            Elements.Add("null 56 list", By.XPath("//li"));
                               Elements.Add("nested frames link", By.XPath("//a[text() = 'Nested Frames']"));
                            Elements.Add("null 57 list", By.XPath("//li"));
                               Elements.Add("notification messages link", By.XPath("//a[text() = 'Notification Messages']"));
                            Elements.Add("null 58 list", By.XPath("//li"));
                               Elements.Add("redirect link link", By.XPath("//a[text() = 'Redirect Link']"));
                            Elements.Add("null 59 list", By.XPath("//li"));
                               Elements.Add("secure file download link", By.XPath("//a[text() = 'Secure File Download']"));
                            Elements.Add("null 60 list", By.XPath("//li"));
                               Elements.Add("shadow dom link", By.XPath("//a[text() = 'Shadow DOM']"));
                            Elements.Add("null 61 list", By.XPath("//li"));
                               Elements.Add("shifting content link", By.XPath("//a[text() = 'Shifting Content']"));
                            Elements.Add("null 62 list", By.XPath("//li"));
                               Elements.Add("slow resources link", By.XPath("//a[text() = 'Slow Resources']"));
                            Elements.Add("null 63 list", By.XPath("//li"));
                               Elements.Add("sortable data tables link", By.XPath("//a[text() = 'Sortable Data Tables']"));
                            Elements.Add("null 64 list", By.XPath("//li"));
                               Elements.Add("status codes link", By.XPath("//a[text() = 'Status Codes']"));
                            Elements.Add("null 65 list", By.XPath("//li"));
                               Elements.Add("typos link", By.XPath("//a[text() = 'Typos']"));
                            Elements.Add("null 66 list", By.XPath("//li"));
                               Elements.Add("wysiwyg editor link", By.XPath("//a[text() = 'WYSIWYG Editor']"));
                   Elements.Add("page-footer div", By.Id("page-footer"));
                         Elements.Add("null 68 hr", By.XPath("//hr"));
                            Elements.Add("elemental selenium link", By.XPath("//a[text() = 'Elemental Selenium']"));

             // Page Dictionary

         }
      }
  }

