using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class NinjaStorePage : FormBase 
      {
          public NinjaStorePage() : base(By.Id("NinjaStore"), "NinjaStore page")
         {
            /// Add Elements
            Elements.Add("top", By.Id("top"));
            Elements.Add("form-currency", By.Id("form-currency"));
            Elements.Add("button 1", By.XPath("//nav[@id='top']/div[1]/div[1]/form[1]/div[1]/button[1]"));
            Elements.Add("code", By.Name("code"));
            Elements.Add("redirect", By.Name("redirect"));
            Elements.Add("top-links", By.Id("top-links"));
            Elements.Add("register", By.XPath("//a[contains(text(),'Register')]"));
            Elements.Add("login", By.XPath("//a[contains(text(),'Login')]"));
            Elements.Add("wishlist-total", By.Id("wishlist-total"));
            Elements.Add("shopping cart", By.XPath("//body/nav[@id='top']/div[1]/div[2]/ul[1]/li[4]/a[1]"));

            Elements.Add("carousel", By.Id("carousel0"));

            Elements.Add("site map", By.XPath("//a[contains(text(),'Site Map')]"));
            Elements.Add("contact us", By.XPath("//a[contains(text(),'Contact Us')]"));

             // Page Dictionary

         }
      }
  }

