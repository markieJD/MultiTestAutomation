using Core;
using OpenQA.Selenium;

  namespace AppTargets.Forms
  {
      public class SwagLoginPage : FormBase 
      {
          public SwagLoginPage() : base(By.Id("SwagLogin"), "SwagLogin page")
         {
              /// Add Elements
             Elements.Add("null html", By.XPath("//html"));
                Elements.Add("null 1 head", By.XPath("//head"));
                   Elements.Add("null 3 link", By.XPath("//link"));
                   Elements.Add("null 4 link", By.XPath("//link"));
                   Elements.Add("null 5 link", By.XPath("//link"));
                   Elements.Add("swag labs title", By.XPath("//title[text() = 'Swag Labs']"));
         Elements.Add("ID", By.XPath("//title[text() = 'Swag Labs']"));
                   Elements.Add("null 6 link", By.XPath("//link"));
                Elements.Add("null 7 body", By.XPath("//body"));
                   Elements.Add("null 8 noscript", By.XPath("//noscript"));
                   Elements.Add("root div", By.Id("root"));
                               Elements.Add("login_button_container div", By.Id("login_button_container"));
                                     Elements.Add("null 14 form", By.XPath("//form"));
                                           Elements.Add("user-name input", By.Id("user-name"));
                                           Elements.Add("password input", By.Id("password"));
                                        Elements.Add("login-button input", By.Id("login-button"));
                                  Elements.Add("login_credentials div", By.Id("login_credentials"));
                                     Elements.Add("null 21 header4", By.XPath("//h4"));
                                     Elements.Add("null 27 header4", By.XPath("//h4"));

             // Page Dictionary

         }
      }
  }

