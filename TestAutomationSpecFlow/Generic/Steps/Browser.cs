﻿using Core;
using Core.Configuration;
using Core.FileIO;
using Core.Logging;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Steps
{
    public static class Browser
    {

        private const string EnvironmentVariable = "ENVIRONMENT";

        public static ChromeVarData ChromeInstance { get; private set; } = new ChromeVarData();

        public class ChromeVarData
        {
            public string Languages { get; set; } = string.Empty;
            public bool SafeBrowsingEnabled { get; set; } = false;
            public bool ProfileDefaultContentSettingsPopups { get; set;} = false;
            public bool DisablePopupBlocking { get; set; } = false;
            public bool PromptForDownload { get; set; } = false;
            public string DownloadDefaultDirectory { get; set;} = string.Empty;
            public bool StartMaximized { get; set; } = false;
            public string[] StartArguments { get; set; } = { "" };
        }



        public static void ReadJson()
        {
            ReadJsonChromeInstance();
        }



        public static ChromeVarData? ReadJsonChromeInstance()
        {
            DebugOutput.Log($"Proc - ReadJson for BROWSER");
            var fileName = $"browser.chrome.{Environment}.json";
            var directory = ".\\AppTargets\\Resources\\Browsers\\";
            var fullFileName = directory + fileName;
            if (!FileChecker.FileCheck(fullFileName))
            {
                DebugOutput.Log($"Unable to find the file {fullFileName}");
                return null;
            }
            var jsonText = File.ReadAllText(fullFileName);
            try
            {
                var obj = JsonConvert.DeserializeObject<ChromeVarData>(jsonText);
                if (obj == null) return null;
                ChromeInstance = obj;
                return ChromeInstance;
            }
            catch
            {
                DebugOutput.Log($"We out ere");
                return null;
            }
        }




        public static void CloseWebBrowser()
        {
            DebugOutput.Log($"proc - CloseWebBrowser");
            if (SeleniumUtil.webDriver == null) return;
            SeleniumUtil.webDriver.Close();
            SeleniumUtil.webDriver.Quit();
            SeleniumUtil.webDriver = null;
        }




        public static void ChromeDriver()
        {
            DebugOutput.Log($"proc - ChromeDriver");
            DebugOutput.Log($"READING CHOMRE {ChromeInstance.Languages}");
            ChromeOptions options = new ChromeOptions();
            if (ChromeInstance.StartMaximized) options.AddArgument("start-maximized");
            if (ChromeInstance.DownloadDefaultDirectory != String.Empty) options.AddUserProfilePreference("download.default_directory", ChromeInstance.DownloadDefaultDirectory);
            if (ChromeInstance.StartArguments.Length > 0 && ChromeInstance.StartArguments[0] != "")
            {
                foreach (var arg in ChromeInstance.StartArguments)
                {
                    DebugOutput.Log($"Adding starting options of {arg}");
                    options.AddArgument(arg);
                }
            }
            SeleniumUtil.webDriver = new ChromeDriver(options);
        }

        public static void EdgeDriver()
        {
            DebugOutput.Log($"proc - EdgeDriver");
            SeleniumUtil.webDriver = new EdgeDriver(EdgeDriverService.CreateDefaultService(@"C:\drivers", "msedgedriver.exe"));
        }

        public static void FireFoxDriver()
        {
            DebugOutput.Log($"proc - FireFoxDriver");
            SeleniumUtil.webDriver = new FirefoxDriver();
        }

        public static void InternetExplorerDriver()
        {
            DebugOutput.Log($"proc - InternetExplorerDriver");
            SeleniumUtil.webDriver = new InternetExplorerDriver(@"C:\drivers\");
            Thread.Sleep(TargetConfiguration.Configuration.PositiveTimeout * 1000 * TargetConfiguration.Configuration.TimeoutMultiplie);
        }

        /// <summary>
        ///     Set up an environment variable called "ENVIRONMENT", and set it to type of environment 
        ///     development, beta, live etc.
        ///     Then you need a targetSettings.ENVIRONMENT.json file in the Resources folder of the AppTargets project
        ///     Different environments need different configuration.  this is how that is controled.
        ///     If there is no ENVIRONMENT environment variable it will use development as default.
        /// </summary>
        private static string Environment =>
            System.Environment.GetEnvironmentVariable(EnvironmentVariable) ?? "development";

    }
}
