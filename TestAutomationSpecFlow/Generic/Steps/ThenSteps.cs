﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Steps
{
    [Binding]
    public class ThenSteps : StepsBase
    {
        public ThenSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Then(@"Wait (.*) Seconds")]
        public void ThenWaitSeconds(int seconds)
        {
            seconds *= 1000;
            Thread.Sleep(seconds);
        }


        [Then(@"Wait ""([^""]*)"" Seconds")]
        public void ThenWaitSeconds(string secondsText)
        {
            var seconds = int.Parse(secondsText);
            seconds *= 1000;
            Thread.Sleep(seconds);
        }

        [Then(@"More To Do Here")]
        public void ThenMoreToDoHere()
        {
            Assert.Inconclusive();
        }

        

        




        



    }
}
