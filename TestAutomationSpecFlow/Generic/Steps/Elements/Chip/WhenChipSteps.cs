﻿using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Chip
{
    [Binding]
    public class WhenChipSteps : StepsBase
    {
        public WhenChipSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [When(@"I Close Chip ""([^""]*)"" In Chip Array ""([^""]*)""")]
        public void WhenICloseChipInChipArray(string chipName, string chipArrayName)
        {
            string proc = $"When I Close Chip {chipName} In Chip Array {chipArrayName}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Chip.CloseChip(chipArrayName, chipName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }




    }
}
