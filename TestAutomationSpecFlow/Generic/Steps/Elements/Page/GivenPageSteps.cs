﻿using Core;
using Core.Logging;
using Core.Transformations;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Page
{
    [Binding]
    public class GivenPageSteps : StepsBase
    {
        public GivenPageSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Given(@"URL is ""([^""]*)""")]
        public void GivenURLIs(string url)
        {
            string proc = $"Given URL Is {url}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (SeleniumUtil.NavigateToURL(url))
                {
                    DebugOutput.Log($"Have navigated");
                    return;
                }
                Assert.Fail($"Could not navigate to {url}");
            }           
        }




        [Given(@"Page Size (.*) x (.*)")]
        public void GivenPageSizeX(int widthPixels, int heightPixels)
        {
            string proc = $"Given Page Size {widthPixels} x {heightPixels}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (SeleniumUtil.SetWindowSize(widthPixels, heightPixels))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"UserName ""([^""]*)"" Password ""([^""]*)"" Navigates To URL ""([^""]*)""")]
        public void GivenUserNamePasswordNavigatesToURL(string username, string password, string url)
        {
            string proc = $"Given UserName {username} Password {password} Navigates To URL {url}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (SeleniumUtil.NavigateToURLAsUserWithPassword(username, password, url))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"Populate Page Object ""([^""]*)"" In Directory ""([^""]*)""")]
        public void GivenPopulatePageObject(string pageName, string directory)
        {
            string proc = $"Given Populate Page Objecte {pageName} In Directory {directory}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (PageObjectValues.PopulatePageObject(pageName, directory))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"Page ""([^""]*)"" In Directory ""([^""]*)"" Has Not Changed")]
        public void GivenPageInDirectoryHasNotChanged(string pageName, string directory)
        {
            string proc = $"Given Page Objecte {pageName} In Directory {directory} Has Not Changed";
            if (CombinedSteps.OuputProc(proc))
            {
                if (PageObjectValues.ComparePageNowAndThen(pageName, directory))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"Images From Elements On Page ""([^""]*)"" Captured")]
        public void GivenImagesFromElementsOnPageCaptured(string pageName)
        {
            string proc = $"Given Image From Elements On Page {pageName} Captured";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Page.GetImagesOfAllElementsInPageFile(pageName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }


    }
}
