﻿using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Page
{
    [Binding]
    public class ThenPageSteps : StepsBase
    {
        public ThenPageSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        
        [Then(@"Page ""(.*)"" Is Not Displayed")]
        public void ThenPageIsNotDisplayed(string pageName)
        {
            string proc = $"Then Page Is Not Displayed {pageName}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!Helpers.Page.IsDisplayed(pageName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }

        }



        [Then(@"Dialog ""([^""]*)"" Is Displayed")]
        [Then(@"Page ""([^""]*)"" Is Displayed")]
        public void ThenPageIsDisplayed(string pageName)
        {
            string proc = $"Then Page {pageName} Is Displayed";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Page.IsExists(pageName))
                {
                    if (Helpers.Page.IsDisplayed(pageName))
                    {
                        DebugOutput.Log($"The page {pageName} ID was found, AND displayed?  Nice!");
                    }
                    else
                    {
                        DebugOutput.Log($"The page {pageName} ID was found, but not displayed?  Try to use another locator!");
                        if (Helpers.Spinner.SpinnerIsGone())
                        {
                            DebugOutput.Log($"There was a spinner! its gone now!");
                            if (Helpers.Page.IsDisplayed(pageName, 1))
                            {
                                DebugOutput.Log($"The page {pageName} ID was found, AND displayed? we had to wait for a spinner but its done now!  Nice!");
                                Helpers.Page.SetCurrentPage(pageName);    
                                return;  
                            }
                            DebugOutput.Log($"Still the ID element for page is NOT displayed!  check it is a DISPLAYED element - it has to be displayed, at the moment, your ID is not visible, so failing to use page {pageName}!");
                        }
                        CombinedSteps.Failure(proc);
                        return;
                    }
                    DebugOutput.Log($"Setting page!");
                    Helpers.Page.SetCurrentPage(pageName);
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Then(@"Message ""([^""]*)"" Is Displayed")]
        [Then(@"Page Displays Message ""([^""]*)""")]
        public void ThenPageDisplaysMessage(string message)
        {
            string proc = $"Then Page Displays Message {message}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Page.IsMessageDisplayed(message))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Then(@"Message ""([^""]*)"" Is Not Displayed")]
        public void ThenMessageIsNotDisplayed(string message)
        {
            string proc = $"Then Page Displays Message {message}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!Helpers.Page.IsMessageDisplayed(message))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }



    }
}
