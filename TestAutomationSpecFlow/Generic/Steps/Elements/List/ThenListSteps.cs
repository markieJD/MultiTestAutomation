﻿using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.List
{
    [Binding]
    public class ThenListSteps : StepsBase
    {
        public ThenListSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Then(@"List ""([^""]*)"" Contains ""([^""]*)""")]
        public void ThenListContains(string listName, string value)
        {
            string proc = $"Then List {listName} Contains {value}";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.List.ListContainsValue(listName, value))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }



    }
}

