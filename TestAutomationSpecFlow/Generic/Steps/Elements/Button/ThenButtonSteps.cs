﻿using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Button
{
    [Binding]
    public class ThenButtonSteps : StepsBase
    {
        public ThenButtonSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Then(@"Button ""([^""]*)"" Is Displayed")]
        public void ThenButtonIsDisplayed(string buttonName)
        {
            string proc = $"Then Button {buttonName} Is Displayed";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Button.IsDisplayed(buttonName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Then(@"Button ""([^""]*)"" Is Not Displayed")]
        public void ThenButtonIsNotDisplayed(string buttonName)
        {
            string proc = $"Then Button {buttonName} Is Displayed";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!Helpers.Button.IsDisplayed(buttonName, 1))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
            Assert.Inconclusive();
        }

        [Then(@"Button ""([^""]*)"" Is Enabled")]
        public void ThenButtonIsEnabled(string buttonName)
        {
            string proc = $"Then Button {buttonName} Is Enabled";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Button.IsEnabled(buttonName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
            Assert.Inconclusive();
        }

        [Then(@"Button ""([^""]*)"" Is Disabled")]
        public void ThenButtonIsDisabled(string buttonName)
        {
            string proc = $"Then Button {buttonName} Is Disabled";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!Helpers.Button.IsEnabled(buttonName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
            Assert.Inconclusive();
        }

        [Then(@"Button ""([^""]*)"" Is Selected")]
        public void GivenButtonIsSelected(string buttonName)
        {
            string proc = $"Then Button {buttonName} Is Selected";
            if (Helpers.Button.IsSelected(buttonName))
            {
                return;
            }
            CombinedSteps.Failure(proc);
            return;
        }


    }
}
