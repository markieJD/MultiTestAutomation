﻿using Core.Logging;
using Core.Transformations;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Datepicker
{
    [Binding]
    public class ThenDatePickerSteps : StepsBase
    {
        public ThenDatePickerSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Then(@"DatePicker ""([^""]*)"" Is Equal To ""([^""]*)""")]
        public void ThenDatePickerIsEqualTo(string datePickerName, string date)
        {
            string proc = $"Then DatePicker {datePickerName} Is Equal To {date}";
            if (CombinedSteps.OuputProc(proc))
            {
                date = StringValues.TextReplacementService(date);   
                if (Helpers.DatePicker.GetCurrentValue(datePickerName, 1) == date)
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }



    }
}
