﻿using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.Table
{
    [Binding]
    public class GivenTableSteps : StepsBase
    {
        public GivenTableSteps(IStepHelpers helpers) : base(helpers)
        {
        }

        [Given(@"Table ""([^""]*)"" Is Displayed")]
        public void GivenTableIsDisplayed(string tableName)
        {
            string proc = $"Given Table {tableName} Is Displayed";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Table.IsDisplayed(tableName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"Table ""([^""]*)"" Is Ordered By Column ""([^""]*)"" Descending")]
        public void GivenTableIsOrderedByColumnDescending(string tableName, string columnName)
        {
            string proc = $"Given Table {tableName} Is Ordered by Column {columnName} Descending";
            if (CombinedSteps.OuputProc(proc))
            {
                if (Helpers.Table.OrderTableByColumn(tableName, columnName))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }




    }
}
