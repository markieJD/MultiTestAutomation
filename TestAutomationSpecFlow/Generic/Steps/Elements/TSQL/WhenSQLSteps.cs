using Core;
using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.TSQL
{
    [Binding]
    public class WhenTSQLSteps
    {
        public WhenTSQLSteps() 
        {            
        }


        [When(@"I SQL Command ""(.*)""")]
        public void WhenISQLCommand(string sqlCommand)
        {
            string proc = $"When I SQL Command {sqlCommand}";
            if (CombinedSteps.OuputProc(proc))
            {
                var table = SQLUtil.GetTableOfResults(sqlCommand);
                if (table != null)
                {
                    DataTableUtil.Hello(table);
                }
            }            
        }


    }
}
