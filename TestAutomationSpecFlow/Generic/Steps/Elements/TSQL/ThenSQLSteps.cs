using Core;
using Core.Logging;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;

namespace Generic.Elements.Steps.TSQL
{
    [Binding]
    public class ThenTSQLSteps
    {
        public ThenTSQLSteps() 
        {            
        }
        
        
        [Then(@"Value ""(.*)"" Is Found In Column ""(.*)"" Using SQL Command ""(.*)""")]
        public void ThenValueIsFoundInColumnUsingSQLCommand(string value,string column,string sqlCommand)
        {
            string proc = $"Then Value {value} Is Found In Column {column} Using SQL Command {sqlCommand}";
            if (CombinedSteps.OuputProc(proc))
            {
                var table = SQLUtil.GetTableOfResults(sqlCommand);
                if (table == null)
                {
                    DebugOutput.Log($"Failed to connect or to make table!");
                    CombinedSteps.Failure(proc);
                    return;
                }
                if (!DataTableUtil.DoesValueExistsInColumn(table, column, value))
                {
                    DebugOutput.Log($"Failed find {value} in {column}");
                    CombinedSteps.Failure(proc);
                    return;
                }
                DebugOutput.Log($"Found it!");
            }
        }



    }
}