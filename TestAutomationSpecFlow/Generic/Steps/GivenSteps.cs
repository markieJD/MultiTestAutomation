﻿
using Core.Configuration;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using BoDi;
using Core;
using Core.FileIO;
using Core.Transformations;
using System.Reflection;
using Generic.Steps.Helpers.Classes;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Appium.Enums;

namespace Generic.Steps
{
    [Binding]
    public class GivenSteps : StepsBase
    {

        public GivenSteps(IStepHelpers helpers) : base(helpers)
        {
        }
    
        [Given(@"I Have Failed")]
        public static void GivenIHaveFailed()
        {
            string proc = "Given I Have Failed";
            CombinedSteps.Failure(proc);
            return;
        }

        
        [Given(@"Application Set As ""(.*)""")]
        public void GivenApplicationSetAs(string applicationName)
        {
            TargetConfiguration.Configuration.AreaPath = applicationName;
            DebugOutput.Log($"Application = TargetConfiguration.Configuration.AreaPath");
        }


        [Given(@"App ""([^""]*)"" Is Open")]
        public void GivenAppIsOpen(string applicationExePath)
        {
            string proc = $"Given App {applicationExePath} Is Open";
            if (CombinedSteps.OuputProc(proc))
            {
                if (SeleniumUtil.winDriver != null)
                {
                    Application.CloseWinDriver();
                }
                if (Application.OpenApplication(applicationExePath)) return;
                CombinedSteps.Failure("There has been a failure when trying to get the windows driver to be active");
            }            
        }

        [Given(@"App Is Closed")]
        public void GivenAppIsClosed()
        {
            string proc = $"Given App Is Close";
            if (!CombinedSteps.OuputProc(proc))
            {
                DebugOutput.Log($"We still want to try and close the app - even after failure");
            }
            if (SeleniumUtil.winDriver == null)
            {
                DebugOutput.Log($"There are no apps currently running in WinAppDriver");
                return;
            }
            Application.CloseWinDriver();
        }

        [Given(@"Browser Is Open")]
        public void GivenBrowserIsOpen()
        {
            string proc = $"Given Browser Is Open";
            DebugOutput.Log(proc);
            if (TargetConfiguration.Configuration == null) return;
            var expectedBrowser = TargetConfiguration.Configuration.Browser;
            GivenBrowserIsOpen(expectedBrowser);
        }


        [Given(@"Browser ""([^""]*)"" Is Open")]
        public void GivenBrowserIsOpen(string browser)
        {
            string proc = $"Given Browser {browser} Is Open";

            if (SeleniumUtil.webDriver != null)
            {
                DebugOutput.Log($"There is already a web driver running!");
                Browser.CloseWebBrowser();
            }

            var compositeBrowserSize = StringValues.BreakUpByDelimited(TargetConfiguration.Configuration.ScreenSize,"x");
            int length = 1200;
            int height = 800;
            if (compositeBrowserSize.Count() != 2)
            {
                DebugOutput.Log($"We need 2 sizes, height and length!  But we won't crash out at this point! We just set it to 800x800");
            }
            else
            {
                try
                {
                    length = Int32.Parse(compositeBrowserSize[0]);
                    height = Int32.Parse(compositeBrowserSize[1]);
                }
                catch
                {
                    DebugOutput.Log($"FAiled to convert {compositeBrowserSize[0]} or {compositeBrowserSize[1]} to an int!  Need an Int");
                }
            }

            if (CombinedSteps.OuputProc(proc))
            {
                switch (browser.ToLower())
                {
                    default:
                    case "chrome":
                        {
                            Browser.ChromeDriver();
                            break;
                        }
                    case "edge":
                        {
                            Browser.EdgeDriver();
                            break;
                        }
                    case "firefox":
                        {
                            Browser.FireFoxDriver();
                            break;
                        }
                    case "ie":
                    case "internet explorer":
                        {
                            Browser.InternetExplorerDriver();
                            break;
                        }
                }
                if (TargetConfiguration.Configuration == null) return ;
                TargetConfiguration.Configuration.ApplicationType = "web";
                SeleniumUtil.SetWindowSize(length, height);
            }
        }

        [Given(@"Browser Is Closed")]
        public void GivenBrowserIsClosed()
        {
            if (SeleniumUtil.webDriver == null)
            {
                DebugOutput.Log($"There is currently no web driver recorded!");
                return;
            }
            Browser.CloseWebBrowser();
            TargetConfiguration.Configuration.AreaPath = "";
        }

        
        [Given(@"File ""(.*)"" Exists")]
        public bool GivenFileExists(string fullFileLocation)
        {
            string proc = $"Given File {fullFileLocation} Exists";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!FileChecker.OSFileCheck(fullFileLocation))
                {
                    DebugOutput.Log($"This is a GIVEN - make it happen if not there!");
                    if (!FileChecker.OSFileCreation(fullFileLocation))
                    {
                        CombinedSteps.Failure($"There has been a failure when trying to create the file {fullFileLocation}");
                        return false;
                    }
                }
                DebugOutput.Log($"File {fullFileLocation} exists!");
                return true;
            }
            return false;
        }

        
        [Given(@"File ""(.*)"" Does Not Exists")]
        public bool GivenFileDoesNotExists(string fullFileLocation)
        {
            string proc = $"Given File {fullFileLocation} Does Not Exists";
            if (CombinedSteps.OuputProc(proc))
            {
                if (!FileChecker.OSFileCheck(fullFileLocation))
                {
                    DebugOutput.Log($"It already does not exist!");
                    return true;
                }
                DebugOutput.Log($"It does exists - this is a given - so get rid of it!");
                if (!FileChecker.OSFileDeletion(fullFileLocation))
                {
                        CombinedSteps.Failure($"Failed to delete {fullFileLocation}");
                        return false;                    
                }
                DebugOutput.Log("Its gone!");
                return true;
            }
            return false;
        }

    }
}
