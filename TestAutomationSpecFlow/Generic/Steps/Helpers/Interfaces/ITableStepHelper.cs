﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Steps.Helpers.Interfaces
{
    public interface ITableStepHelper : IStepHelper
    {
        bool ActionRowByValueInColumnName(string tableName, string action, string columnName, string value);
        bool ActionRowByNumber(string tableName, string action, int rowNumber);
        bool ClikcOnLinkInTableColumnRow(string tableName, int columnNumber, int rowNumber);
        bool ClickOnRow(int rowNumber, string tableName);
        bool DoesRowColumnNumberContainsLink(string tableName, int rowNumber, int columnNumber);
        bool DoesRowContainAction(string tableName, string action, string columnName, string value);
        int GetNumberOfPopulatedRowsDisplayed(string tableName);
        int GetNumberOfRowsDisplayed(string tableName);
        int GetNumberOfRowsWithLocatorFound(string tableName, By locator);
        string GetValueOfGridBox(string tableName, int rowNumber, int columnNumber, bool header);
        string GetValueOfGridBoxUsingColumnTitle(string tableName, string columnTitle, int rowNumber);
        bool Filter(string tableName, string value);
        bool IsColumnContainValue(string tableName, string columnName, string value);
        bool IsDisplayed(string tableName);
        bool IsRowHighlighted(string tableName, int rowNumber);
        bool OrderTableByColumn(string tableName, string columnName);
        bool OrderTableByColumnDesc(string tableName, string columnName);
    }
}
