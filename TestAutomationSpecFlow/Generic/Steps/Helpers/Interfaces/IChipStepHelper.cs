﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Steps.Helpers.Interfaces
{
    public interface IChipStepHelper : IStepHelper
    {
        bool ArraryContainsChip(string chipArraryName, string chipName);
        bool CloseChip(string chipArrayName, string chipName);
        bool IsDisplayed(string chipArraryName);
    }
}
