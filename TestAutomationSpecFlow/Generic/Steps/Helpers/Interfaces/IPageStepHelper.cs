﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Steps.Helpers.Interfaces
{
    public interface IPageStepHelper : IStepHelper
    {
        public List<IWebElement> GetAllPageElements(string pageName, int timeOut = 0);
        IWebElement? GetPageElement(string pageName, string elementName, int timeout = 0);
        string GetCurrentPageName();
        bool GetImagesOfAllElementsInPageFile(string pageName);
        bool IsDisplayed(string pageName, int timeOut = 0);
        bool IsExists(string pageName);
        bool IsMessageDisplayed(string message);
        void SetCurrentPage(string pageName);
        bool SetCurrentPageSize(int width = 800, int height = 600);
    }
}
