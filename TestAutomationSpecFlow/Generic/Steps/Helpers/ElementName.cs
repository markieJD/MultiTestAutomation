﻿using Core;
using Core.Logging;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public static class ElementName
    {

        //public static string GetCorrectAccordianName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectAccordianName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage}");
        //    return null;
        //}

        private static By? PageDicContains(FormBase CurrentPage, string value)
        {
            DebugOutput.Log($"Does the Current Page {CurrentPage.Name} contain the element {value}");
            if (CurrentPage.Elements.ContainsKey(value))
            {
                DebugOutput.Log($"IT DOES! Found it!");
                return CurrentPage.Elements[value];
            }
            DebugOutput.Log($"Nope - not in this page at least!");
            return null;
        }

        public static By? GetElementLocator(string name, FormBase CurrentPage, string type = "")
        {
            name = name.ToLower();
            DebugOutput.Log($"GetCorrectElementName {name} CURRENTPAGE {CurrentPage} and TYPE = {type}");
            if (CurrentPage.Elements.ContainsKey(name))
            {
                DebugOutput.Log($"Found the element name {name} on current page form!");
                return CurrentPage.Elements[name];
            }

            DebugOutput.Log($"Failed to find name as {name} this is common if you have multiple things, with multipe names, so have to give it the element type to narrow it down!");

            switch (type.ToLower())
            {
                default:
                case "":
                    {
                        DebugOutput.Log($"Do not know what TYPE of element! to check!");
                        return null;
                    }
                case "accordion":
                    return PageDicContains(CurrentPage, name + " " + "accordion");
                case "button":
                    return PageDicContains(CurrentPage, name + " " + "button");
                case "chip":
                    return PageDicContains(CurrentPage, name + " " + "chip");
                case "checkbox":
                    return PageDicContains(CurrentPage, name + " " + "checkbox");
                case "datepicker":
                    return PageDicContains(CurrentPage, name + " " + "datepicker");
                case "dropdown":
                    return PageDicContains(CurrentPage, name + " " + "dropdown");
                case "image":
                    return PageDicContains(CurrentPage, name + " " + "image");
                case "input":
                case "text":
                case "textbox":
                    return PageDicContains(CurrentPage, name + " " + "textbox");
                case "list":
                    return PageDicContains(CurrentPage, name + " " + "list");
                case "radiobutton":
                    return PageDicContains(CurrentPage, name + " " + "radiobutton");
                case "slider":
                    return PageDicContains(CurrentPage, name + " " + "slider");
                case "stepper":
                    return PageDicContains(CurrentPage, name + " " + "stepper");
                case "tab":
                case "tabs":
                    return PageDicContains(CurrentPage, name + " " + "tabs");
                case "timepicker":
                    return PageDicContains(CurrentPage, name + " " + "timepicker");
                case "tree":
                    return PageDicContains(CurrentPage, name + " " + "tree");

            }
        }

        //public static string? GetCorrectCheckboxName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectCheckboxName {name}");
        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "checkbox";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    newName = name + " " + "check";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage}");
        //    return null;
        //}

        //public static string? GetCorrectDropdownName(string name, FormBase CurrentPage)
        //{
        //    DebugOutput.Log($"Proc - GetCorrectDropdownName {name}");
        //    name = name.ToLower();

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "select";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    newName = name + " " + "dropdown";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage}");
        //    return null;
        //}


        //public static string? GetCorrectImageName(string name, FormBase CurrentPage)
        //{
        //    DebugOutput.Log($"Proc - GetCorrectImageName {name}");
        //    name = name.ToLower();

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "image";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as an image object!");
        //    return null;
        //}

        //public static string GetCorrectLinkName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectLinkName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "link";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    newName = name + " " + "a";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as a link object, but this could be just a link!");
        //    return name;
        //}

        //public static string? GetCorrectRadioButtonName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectRadioButtonName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "input";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    newName = name + " " + "radiobutton";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }

        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as a RadioButton object");
        //    return null;
        //}

        //public static string? GetCorrectTextBoxName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectTextBoxName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "input";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }
        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as a TextBox object");
        //    return null;
        //}

        //public static string? GetCorrectTreeName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectTreeName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "tree";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }
        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as a Tree object");
        //    return null;
        //}

        //public static string? GetCorrectSliderName(string name, FormBase CurrentPage)
        //{
        //    name = name.ToLower();
        //    DebugOutput.Log($"GetCorrectSliderName {name}");

        //    if (CurrentPage.Elements.ContainsKey(name))
        //    {
        //        return name;
        //    }

        //    var newName = "";

        //    newName = name + " " + "slider";
        //    if (CurrentPage.Elements.ContainsKey(newName))
        //    {
        //        return newName;
        //    }
        //    DebugOutput.Log($"Failed to find {name} in {CurrentPage} as a Slider object");
        //    return null;
        //}


    }
}
