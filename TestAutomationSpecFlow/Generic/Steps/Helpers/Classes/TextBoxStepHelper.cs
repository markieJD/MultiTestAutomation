﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class TextBoxStepHelper : StepHelper, ITextBoxStepHelper
    {
        private readonly ITargetForms targetForms;
        public TextBoxStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public string GetText(string textBoxName)
        {
            DebugOutput.Log($"GetText {textBoxName}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null)
            {
                DebugOutput.Log($"Failed to get TextBox");
                return "";
            }
            string text = GetTextFromElement(textBoxElement);
            if (text == null || text == "")
            {
                DebugOutput.Log($"Failed to get ANY Text");
                Thread.Sleep(1000);
                text = GetTextFromElement(textBoxElement);
                return text;
            }
            DebugOutput.Log($"gotten {text} from textBox {textBoxName}");
            return text;
        }

        private string GetTextFromElement(IWebElement textBoxElement)
        {
            DebugOutput.Log($"GetTextFromElement {textBoxElement}");
            return SeleniumUtil.GetElementText(textBoxElement);
        }

        public bool ClearThenEnterText(string textBoxName, string text)
        {
            DebugOutput.Log($"ClearThenEnterText {textBoxName} {text}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null) return false;
            if (!EnterTextAndKey(textBoxName, "", "clear")) return false;
            if (!EnterText(textBoxName, text)) return false;
            return true;
        }

        public bool EnterText(string textBoxName, string text, string key = "")
        {
            DebugOutput.Log($"EnterText {textBoxName} {text} {key}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null) return false;
            if (!SeleniumUtil.EnterText(textBoxElement, text, key)) return false;
            DebugOutput.Log($"Text Entered");
            return true;
        }

        public bool EnterTextAndKey(string textBoxName, string text, string key)
        {
            if (key == null) return false;
            key = key.ToLower();
            return EnterText(textBoxName, text, key);
        }

        public bool IsDisplayed(string textBoxName)
        {
            DebugOutput.Log($"IsDisplayed {textBoxName}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null) return false;
            return textBoxElement.Displayed;
        }

        public bool Click(string textBoxName)
        {
            DebugOutput.Log($"Click {textBoxName}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null) return false;
            return SeleniumUtil.Click(textBoxElement);
        }

        public bool IsReadOnly(string textBoxName)
        {
            DebugOutput.Log($"Proc - IsReadOnly {textBoxName}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null) return false;
            return !textBoxElement.Enabled;
        }

        public int GetWidthOfTextBox(string textBoxName)
        {
            DebugOutput.Log($"Proc - GetWidthOfTextBox {textBoxName}");
            var textBoxElement = GetTextBoxElement(textBoxName);
            if (textBoxElement == null)
            {
                DebugOutput.Log($"Failed to find the element, let alone get its size!  Return 0");
                 return 0;
            }
            return SeleniumUtil.GetWidthOfElement(textBoxElement);
        }



        ///  PRIVATE
        ///  
        private IWebElement? GetTextBoxElement(string textBoxName)
        {
            DebugOutput.Log($"GetTextBox {textBoxName}");
            var textBoxLocator = ElementName.GetElementLocator(textBoxName, CurrentPage, "textbox");
            if (textBoxLocator == null) return null;
            DebugOutput.Log($"We have the LOCATOR for TextBox {textBoxName} {textBoxLocator}");
            var element = SeleniumUtil.GetElement(textBoxLocator);
            DebugOutput.Log($"TextBox Element {textBoxName} = {element}");
            if (element == null)
            {
                DebugOutput.Log($"WE HAVE FOUND NO TEXT BOX {textBoxName}");
            }
            return element;
        }

    }
}
