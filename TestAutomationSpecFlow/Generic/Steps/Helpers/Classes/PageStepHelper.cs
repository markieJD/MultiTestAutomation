﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class PageElements
    {
        public List<string>? ElementKeys { get; set; }  
        public List<By>? ElementLocators { get; set; }
    }

    public class PageStepHelper : StepHelper, IPageStepHelper
    {
        private readonly ITargetForms targetForms;
        public PageStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        int versionNumber = 0;

        private readonly string[] messageLocatorStart = { $"//*[contains(text(),'" };
        private readonly string outputDir = @"AppTargets\Forms";

        public string GetCurrentPageName()
        {
            DebugOutput.Log($"proc - GetCurrentPageName");
            if (CurrentPage.Name == null) return "NO PAGE REGISTERED";
            return CurrentPage.Name;
        }

        public List<IWebElement> GetAllPageElements(string pageName, int timeOut = 0)
        {
            DebugOutput.Log($"proc - GetAllPageElements '{pageName}'");
            pageName = GetPageName(pageName);
            DebugOutput.Log($"proc - GetAllPageElements AFTER GetPageName '{pageName}' ");
            var listOfElements = new List<IWebElement>();
            try
            {
                var expectedPage = targetForms[pageName];
                if (expectedPage == null) return listOfElements;
                DebugOutput.Log($"Have page {pageName}. Now looking for ALL the elements");
                var pageElements = expectedPage.Elements;
                foreach (KeyValuePair<string, By> entry in pageElements)
                {
                    DebugOutput.Log($"Key {entry.Key} | Locator {entry.Value}  ");    
                    //MUCH MORE TO DO HERE! 
                }
            }
            catch
            {

            }
            return listOfElements;
        }

        public IWebElement? GetPageElement(string pageName, string elementName, int timeOut = 0)
        {
            DebugOutput.Log($"proc - GetPageElement '{pageName}' {elementName}");
            pageName = GetPageName(pageName);
            DebugOutput.Log($"proc - GetPageElement AFTER GetPageName '{pageName}' {elementName}");
            try
            {
                var expectedPage = targetForms[pageName];
                if (expectedPage == null) return null;
                DebugOutput.Log($"Have page {pageName}. Now looking for the element {elementName}");
                var pageLocator = expectedPage.Elements[elementName];
                if (pageLocator == null) return null;
                DebugOutput.Log($"We have the LOCATOR {pageLocator}");
                var element = SeleniumUtil.GetElement(pageLocator, timeOut);
                if (element == null) return null;
                return element;
            }
            catch
            {
                DebugOutput.Log($"FAILED SOMEWHERE AROUND PAGE {pageName} looking for {elementName}");
                return null;
            }
        }

        public bool IsExists(string pageName)
        {
            DebugOutput.Log($"proc - IsExists {pageName}");
            pageName = GetPageName(pageName);
            DebugOutput.Log($"proc - IsExists {pageName}");
            var pageElement = GetForm(pageName);
            if (pageElement == null) return false;
            return true;

        }

        public bool IsDisplayed(string pageName, int timeOut = 0)
        {
            DebugOutput.Log($"proc - IsDisplayed {pageName}");
            if (timeOut == 0)
            {
                timeOut = TargetConfiguration.Configuration.PositiveTimeout;
            }
            pageName = GetPageName(pageName);
            DebugOutput.Log($"proc - IsDisplayed {pageName}");
            var pageElement = GetPageElement(pageName, "ID", timeOut);
            if (pageElement == null) return false;
            DebugOutput.Log($"WE have the ID element we return if displayed or not!");
            return pageElement.Displayed;
        }

        public bool IsMessageDisplayed(string message)
        {
            message = StringValues.TextReplacementService(message);
            DebugOutput.Log($"proc - IsMessageDisplayed {message}");
            var messageXPath  = messageLocatorStart[versionNumber] + $"{message}')]";
            var messageElement = SeleniumUtil.GetElement(By.XPath(messageXPath),1);
            if (messageElement == null) return IsMessageDisplayedName(message);
            DebugOutput.Log($"We have the message element {messageElement}");
            return true;
        }

        public void SetCurrentPage(string pageName)
        {
            pageName = GetPageName(pageName);
            FormBase? expectedPage;
            string pageNameNew = "";
            try
            {
                expectedPage = targetForms[pageName];
                CurrentPage = expectedPage;
            }
            catch
            {
                try
                {
                    pageNameNew = TargetConfiguration.Configuration.AreaPath + " " + pageName;
                    expectedPage = targetForms[pageNameNew];
                    CurrentPage = expectedPage;
                }
                catch
                {
                    DebugOutput.Log($"THIS HAS NOT SET THE PAGE NAME _ CHECK THERE IS A PAGE MODEL FOR {pageName} OR {pageNameNew}");
                }
            }
            DebugOutput.Log($"Current page now set to {CurrentPage}");
        }

        public bool SetCurrentPageSize(int width = 800, int height = 600)
        {
            return SeleniumUtil.SetWindowSize(width, height);
        }



        //PRIVATE METHODS BELOW

        private FormBase? GetForm(string pageName)
        {
            pageName = GetPageName(pageName);
            FormBase? expectPage = null;
            DebugOutput.Log($"Proc - GetForm {pageName}");
            try
            {
                expectPage = targetForms[pageName];
            }
            catch
            {
                DebugOutput.Log($"Issue with targetForm {pageName}");
            }
            return expectPage;
        }

        private PageElements GetAllPageElements(string pageName)
        {
            pageName = GetPageName(pageName);
            int counter = 0;
            var expectedPage = targetForms[pageName];
            List<string> elementKeys = new List<string>();
            List<By> elementLocators = new List<By>();
            foreach (var item in expectedPage.Elements)
            {
                string elementKey = item.Key;
                elementKeys.Add(elementKey);
                By elementLocator = item.Value;
                elementLocators.Add(elementLocator);
                counter++;
            }
            return new PageElements { ElementKeys = elementKeys, ElementLocators = elementLocators };
        }

        public bool GetImagesOfAllElementsInPageFile(string pageName)
        {
            pageName = GetPageName(pageName);
            PageElements pageElements = GetAllPageElements(pageName);
            if (pageElements.ElementKeys == null) return false;
            bool allImagesTaken = true;
            int counter = 0;
            int x = 0;
            int y = 0;
            x = SeleniumUtil.GetPageWidth();
            y = SeleniumUtil.GetPageHeight();
            foreach (var elementName in pageElements.ElementKeys)
            {
                if (pageElements.ElementLocators == null) return false;
                By elementLocator = pageElements.ElementLocators[counter];
                var element = SeleniumUtil.GetElement(elementLocator, 1);
                if (element == null) return false;
                if (element.Displayed)
                {
                    if (TargetConfiguration.Configuration == null) return false;    
                    if (TargetConfiguration.Configuration.ApplicationType.ToLower() == "web")
                    {
                        SeleniumUtil.SetWindowSize(3000, 3000);
                        var elementHeight = element.Size.Height;
                        if (!SeleniumUtil.MoveToElement(element)) return false;
                        if (!SeleniumUtil.ScreenShotElement(element, elementName, pageName, outputDir))
                        {
                            DebugOutput.Log($"Failed, scroll down slightly! Try again!");
                            int moveY = element.Size.Height;
                            if (!SeleniumUtil.MoveToElement(element, 0, moveY)) return false;
                            if (!SeleniumUtil.ScreenShotElement(element, elementName, pageName, outputDir)) return false;
                        }
                    }
                }
                counter++;
            }
            SeleniumUtil.SetWindowSize(x, y);

            DebugOutput.Log($"WE have {pageElements.ElementKeys.Count} elements on Page!");
            return allImagesTaken;
        }

        private string GetPageName(string pageName)
        {
            DebugOutput.Log($"Proc - GetPageName {pageName}");
            var app = GetApplication(true);
            if (app == null) return pageName;
            DebugOutput.Log($"We are checking given page name verses if an app is requred to find it (i.e. multiple app testing) {pageName.ToLower()} VERSES {app}");
            if (pageName.ToLower().Contains(app)) return pageName;
            DebugOutput.Log($"The page {pageName} does not contain {app.ToLower()} even in lower case?  {pageName.ToLower()}");
            var newPageName = app + " " + pageName;
            DebugOutput.Log($"NEW PAGE NAME = {newPageName}");
            return newPageName;
        }

        private string? GetApplication(bool lowerCase = false)
        {
            if (TargetConfiguration.Configuration.AreaPath == null) return null;
            var app = TargetConfiguration.Configuration.AreaPath;
            if (lowerCase) return app.ToLower();
            return app;
        }



        private bool IsMessageDisplayedName(string message)
        {
            DebugOutput.Log($"proc - IsMessageDisplayedName {message}");
            var locator = By.Name(message);
            var messageElement = SeleniumUtil.GetElement(locator, 1);
            if (messageElement == null) return IsMessageDisplayedPartOfName(message);
            DebugOutput.Log($"Found by NAME!");
            return true;
        }

        private bool IsMessageDisplayedPartOfName(string message)
        {
            DebugOutput.Log($"proc - IsMessageDisplayedPartOfName {message}");
            var cssLocator = By.CssSelector("[Name*='" + message + "']");
            var messageElement = SeleniumUtil.GetElement(cssLocator, 1);
            if (messageElement == null) return IsMessageDisplayedPartOfAnyName(message);
            DebugOutput.Log($"Found by PART of NAME!");
            return true;
        }

        private bool IsMessageDisplayedPartOfAnyName(string message)
        {
            return SeleniumUtil.ListOfElementNamesContains(message);
        }

    }
}
