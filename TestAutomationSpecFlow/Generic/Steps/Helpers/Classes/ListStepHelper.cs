﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class ListStepHelper : StepHelper, IListStepHelper
    {
        private readonly ITargetForms targetForms;
        public ListStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private readonly By[] ListItemLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.ListItemLocator);


        public bool ListContainsValue(string list, string value)
        {
            DebugOutput.Log($"proc - ListContainsValue {list} {value}");
            bool foundItem = false;
            var listElement = GetListElement(list);
            if (listElement == null) return foundItem;
            ///WE HAVE OUR LIST,  NOW WHAT IS IN IT!
            ///
            var listItems = GetListItems(listElement);
            if (listItems.Count == 0) return foundItem;
            DebugOutput.Log($"Have {listItems.Count} items");
            foreach (var listItem in listItems)
            {
                var text = SeleniumUtil.GetElementText(listItem);
                DebugOutput.Log($"We GOTTEN = '{text}'");
                if (text == value)
                {
                    DebugOutput.Log($"MATHCING");
                    foundItem = true;
                    return foundItem;
                }
                DebugOutput.Log($"Some lists contain the text below in a different element inside the list item");
                By textElementLocator = By.ClassName("Text");
                var textElements = SeleniumUtil.GetElementsUnder(listItem, textElementLocator, 1);
                if (textElements.Count > 0)
                {
                    DebugOutput.Log($"FOUND {textElements.Count} text elements under the list item");
                    var textText = textElements[0].GetAttribute("Name");
                    DebugOutput.Log($"FOUND IT!  textText  {textElements[0].GetAttribute("Name")}");
                    if (textText == value)
                    {
                        DebugOutput.Log($"MATHC");
                        foundItem = true;
                        return foundItem;
                    }
                }
            }
            return foundItem;
        }

        private List<IWebElement> GetListItems(IWebElement listElement)
        {
            int counter = 0;
            int numberOfListItemLocators = ListItemLocator.Count();
            List<IWebElement> listItems = new List<IWebElement>();
            while (counter < numberOfListItemLocators)
            {
                var locator = ListItemLocator[counter];
                listItems = SeleniumUtil.GetElementsUnder(listElement, locator, 1);
                if (listItems.Count() > 0) return listItems;
                counter++;
            }
            return listItems;
        }

        private IWebElement? GetListElement(string listName)
        {
            var listLocator = ElementName.GetElementLocator(listName, CurrentPage, "list");
            if (listLocator == null) return null;
            var listElement = SeleniumUtil.GetElement(listLocator, 2);
            if (listElement == null)
            {
                DebugOutput.Log($"Can not find element by {listLocator} on page {CurrentPage}");
                return null;
            }
            return listElement;
        }

    }
}
