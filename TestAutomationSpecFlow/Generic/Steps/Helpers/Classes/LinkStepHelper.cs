﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class LinkStepHelper : StepHelper, ILinkStepHelper
    {
        private readonly ITargetForms targetForms;
        public LinkStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool ClickLink(string linkName)
        {
            DebugOutput.Log($"proc - IsDisplayed {linkName}");
            var linkElement = GetLinkElement(linkName);
            if (linkElement == null) return false;
            return SeleniumUtil.Click(linkElement);
        }

        public bool IsDisplayed(string linkName)
        {
            DebugOutput.Log($"proc - IsDisplayed {linkName}");
            var linkElement = GetLinkElement(linkName);
            if (linkElement == null) return false;
            return linkElement.Displayed;
        }

        // PRIVATE

        private IWebElement? GetLinkElement(string linkName)
        {
            DebugOutput.Log($"GetLinkElementByXPath {linkName}");
            ///with a link we can just supply the href, so do not need a named link
            ///BUT it can change, so maybe you supply a named link?
            ///
            By? locator = ElementName.GetElementLocator(linkName, CurrentPage, "link"); 
            if (linkName == null || locator == null)
            {
                //var stringXPath = $"//*[contains(text(),'{linkName}')]";
                var stringXPath = $"//*[text()='{linkName}']";
                locator = By.XPath(stringXPath);
            }

            var element = SeleniumUtil.GetElement(locator, 1);
            DebugOutput.Log($"Link Element By {locator}");
            return element;
        }

    }
}
