﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class TabStepHelper : StepHelper, ITabStepHelper
    {
        private readonly ITargetForms targetForms;
        public TabStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }


        private readonly By[] TabLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.TabLocator);
        private readonly By[] TabTextLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.TabTextLocator);


        public bool IsDisplayed(string tabName)
        {
            DebugOutput.Log($"Proc - IsDisplayed {tabName}");
            var tabElement = GetTabElement(tabName, 1);
            if (tabElement == null) return false;
            return tabElement.Displayed;
        }

        public bool TabContainedInTabs(string tabs, string tabName)
        {
            DebugOutput.Log($"Proc - TabContainedInTabs {tabs} {tabName}");
            var tabElement = GetSingleTagElementFromTabs(tabs, tabName);
            if (tabElement == null) 
            {
                DebugOutput.Log($"Failed to find {tabName} in {tabs}");
                return false;
            }
            return tabElement.Displayed;
        }



        public bool SelectTab(string tabs, string tabName)
        {
            DebugOutput.Log($"Proc - SelectTab {tabs} {tabName}");
            var tabElement = GetSingleTagElementFromTabs(tabs, tabName);
            if (tabElement == null) 
            {
                DebugOutput.Log($"Failed to find tabName {tabName} in tabs {tabs}");
                return false;
            }
            DebugOutput.Log($"Attempting to click on tabName {tabName}");
            return SeleniumUtil.Click(tabElement);
        }

        public string WhatTabIsSelected(string tabName)
        {
            DebugOutput.Log($"Proc - WhatTabIsSelected {tabName}");
            var tabsElement = GetTabElement(tabName, 1);
            if (tabsElement == null) return "";
            var tabElements = GetAlLTabsInTab(tabsElement, 1);
            if (tabElements.Count == 0) return "";
            foreach (var tabElement in tabElements)
            {
                if (SeleniumUtil.IsSelected(tabElement))
                {
                    DebugOutput.Log($"we have found the selected tab!");
                    var returnedText = GetTabText(tabElement);
                    DebugOutput.Log($"And its name is {returnedText}"); 
                    return returnedText;
                }
            }
            DebugOutput.Log($"Failed to find selected tab!");
            return "";
        }

        public int GetNumberOfTabsInTabs(string tabsName)
        {
            DebugOutput.Log($"Proc - GetNumberOfTabsInTabs {tabsName}");
            var tabsElement = GetTabElement(tabsName, 1);
            if (tabsElement == null) return -1;
            var tabElements = GetAlLTabsInTab(tabsElement, 1);
            return tabElements.Count();
        }


        //Private 

        private string GetTabText(IWebElement tabElement)
        {
            DebugOutput.Log($"GetTabText {tabElement}");
            DebugOutput.Log($"In windows there can be no text, it is just the name!");
            var textBackFromName = SeleniumUtil.GetElementText(tabElement);
            if (!string.IsNullOrEmpty(textBackFromName)) return textBackFromName;
            DebugOutput.Log($"Will need to check locators as null or empty name!");
            foreach (var tabTextloc in TabTextLocator)
            {
                DebugOutput.Log($"Checking {tabTextloc} locators for text");
                var tabTextElement = SeleniumUtil.GetElementUnderElement(tabElement, tabTextloc, 1);
                if (tabTextElement != null)
                {
                    DebugOutput.Log($"We have locator found {tabTextloc}");
                    var textBack = SeleniumUtil.GetElementText(tabTextElement);
                    if (textBack != "0" || textBack != null)
                    {
                        return textBack;
                    }
                }
            }

            DebugOutput.Log($"Failed to find Text for tabElement {tabElement} using locator");
            return "";
        }


        private List<IWebElement> GetAlLTabsInTab(IWebElement tabsElement, int timeout)
        {
            DebugOutput.Log($"GetAlLTabsInTab {tabsElement}");
            List<IWebElement> result = new List<IWebElement>();
            var numberOfTabLocators = TabLocator.Length;
            if (numberOfTabLocators == 0) return result;
            DebugOutput.Log($"Going through possible {numberOfTabLocators} tab locators");
            foreach (var tab in TabLocator)
            {
                DebugOutput.Log($"Checking {tab}");
                var tabElements = SeleniumUtil.GetElementsUnder(tabsElement, tab, timeout);
                if (tabElements.Count > 0)
                {
                    DebugOutput.Log($"Have found {tabElements.Count} tabs!");
                    foreach (var element in tabElements)
                    {
                        result.Add(element);
                    }
                    return result;
                }
            }
            DebugOutput.Log($"Does not find ANY tabs!");
            return result;
        }

        private IWebElement? GetTabElement(string tabName, int timeout)
        {
            DebugOutput.Log($"GetTabElement {tabName}");
            var tabLocator = ElementName.GetElementLocator(tabName, CurrentPage, "tab");
            if (tabLocator == null) return null;
            var namedTabElement = SeleniumUtil.GetElement(tabLocator, timeout);
            if (namedTabElement == null)
            {
                DebugOutput.Log($"Can not find element by {tabLocator} on page {CurrentPage}");
                return null;
            }
            return namedTabElement;
        }

        
        private IWebElement? GetSingleTagElementFromTabs(string tabs, string tabName)
        {            
            DebugOutput.Log($"Proc - GetSingleTagElementFromTabs {tabs} {tabName}");
            var tabsElement = GetTabElement(tabs, 1); 
            if (tabsElement == null) return null;;
            DebugOutput.Log($"Gotten tabs element");
            var tabElements = GetAlLTabsInTab(tabsElement, 1);
            if (tabElements.Count == 0) return null;
            DebugOutput.Log($"Gotten ALL tab element");
            DebugOutput.Log($"We have {tabElements.Count} tab elements under tab {tabs}");
            foreach (var tabElement in tabElements)
            {
                var tabText = GetTabText(tabElement);
                if (tabText.ToLower() == tabName.ToLower())
                {
                    DebugOutput.Log($"We have that text for tab name");
                    return tabElement;
                }
                DebugOutput.Log($"We have the name of {tabText} looking for {tabName} using {tabElement}");
            }
            DebugOutput.Log($"Did not find that tab {tabName}");
            return null;
        }


    }
}
