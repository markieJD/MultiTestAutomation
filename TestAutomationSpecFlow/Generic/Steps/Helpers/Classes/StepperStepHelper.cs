﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class StepperStepHelper : StepHelper, IStepperStepHelper
    {
        private readonly ITargetForms targetForms;
        public StepperStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private readonly By[] StepperStepLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.StepperStepLocator);
        private readonly By[] StepperStepLabelLocator = { By.ClassName("item-label") };
        private readonly By StepValid = By.ClassName("item-valid");
        private readonly By StepEmpty = By.ClassName("item-empty");
        private readonly By StepDisabled = By.ClassName("item-disabled");
        private readonly By StepProgress = By.ClassName("item-progress");


        public bool IsDisplayed(string stepperName)
        {
            DebugOutput.Log($"IsDisplayed {stepperName}");
            var stepperElement = GetStepperElement(stepperName);
            if (stepperElement == null) return false;
            DebugOutput.Log($"We have the element for stepper {stepperName}");
            return stepperElement.Displayed;
        }

        public bool IsStepDisplayed(string stepperName, string stepName)
        {
            DebugOutput.Log($"IsStepDisplayed {stepperName} {stepName}");
            var stepperElement = GetStepperElement(stepperName);
            if (stepperElement == null) return false;
            var stepNumber = GetWhatStepByName(stepperElement, stepName);
            if (stepNumber < 0) return false;
            DebugOutput.Log($"Found in step {stepNumber}");
            return true;
        }

        public int GetNumberOfSteps(string stepperName)
        {
            DebugOutput.Log($"IsDisplayed {stepperName}");
            var stepperElement = GetStepperElement(stepperName);
            if (stepperElement == null) return -1;
            foreach(var stepLocator in StepperStepLocator)
            {
                DebugOutput.Log($"Trying for {stepLocator}");
                var stepElements = SeleniumUtil.GetElementsUnder(stepperElement, stepLocator);
                if (stepElements.Count > 0)
                {
                    DebugOutput.Log($"We have {stepElements.Count} elements using locator {stepLocator}");
                    return stepElements.Count;
                }
            }
            DebugOutput.Log($"Failed to find ANY steps under Stepper Element");
            return 0;
        }

        public string? GetStatusOfStep(string stepperName, string stepName)
        {
            DebugOutput.Log($"IsDisplayed {stepperName}");
            var stepperElement = GetStepperElement(stepperName);
            if (stepperElement == null) return null;
            var stepElements = GetStepElements(stepperElement);
            if (stepElements.Count < 1) return null;
            var stepNumber = GetWhatStepByName(stepperElement, stepName);
            if (stepNumber < 0) return null;
            var stepElement = stepElements[stepNumber];
            if (stepElement == null) return null;
            var classReturn = SeleniumUtil.GetElementAttributeValue(stepElement, "class");
            DebugOutput.Log($"WE have the class OF '{classReturn}   '");
            var actualStatus = FixStatusByClass(classReturn);
            return actualStatus;
        }


        //PRIVATE

        private IWebElement? GetStepperElement(string stepperName, int timeOut = 0)
        {
            DebugOutput.Log($"GetStepperElement {stepperName} {timeOut}");
            var stepperLocator = ElementName.GetElementLocator(stepperName, CurrentPage, "stepper");
            if (stepperLocator == null) return null;
            return SeleniumUtil.GetElement(stepperLocator, timeOut);
        }

        private List<IWebElement> GetStepElements(IWebElement stepperElement)
        {
            DebugOutput.Log($"GetStepElements {stepperElement} ");
            List<IWebElement> stepElements = new List<IWebElement>();
            foreach (var stepLocator in StepperStepLocator)
            {
                var labelElements = SeleniumUtil.GetElementsUnder(stepperElement, stepLocator);
                if (labelElements.Count > 0)
                {
                    stepElements = labelElements;
                    return stepElements;
                }
            }
            DebugOutput.Log($"Failed to find any Step labels!");
            return stepElements;
        }

        private int GetWhatStepByName (IWebElement stepperElement, string stepName)
        {
            DebugOutput.Log($"GetWhatStepByName {stepperElement} {stepName} ");
            if (stepperElement == null) return -1;
            //Get All Step Elements (li)
            var stepElements = GetStepElements(stepperElement);
            if (stepElements.Count < 1) return -1;
            int counter = 0;
            foreach (var stepLabelLocator in StepperStepLabelLocator)
            {
                var stepLabelElements = SeleniumUtil.GetElementsUnder(stepperElement,stepLabelLocator);
                if (stepLabelElements.Count() > 0)
                {
                    foreach (var stepLabelElement in stepLabelElements)
                    {
                        var title = SeleniumUtil.GetElementText(stepLabelElement);
                        if (stepName == title) return counter;
                        DebugOutput.Log($"Step Name read as {stepName} does not match {title}");
                        counter++;
                    }
                }
            }
            DebugOutput.Log($"Failed to find it!");
            return -1;
        }

        private string? FixStatusByClass(string classText)
        {
            DebugOutput.Log($"GetStepElements {classText} ");
            classText = classText.ToLower();
            if (classText.Contains("invalid")) return "invalid";
            if (classText.Contains("valid")) return "valid";
            if (classText.Contains("empty")) return "invalid";
            if (classText.Contains("disabled")) return "disabled";
            if (classText.Contains("progress")) return "progress";
            return null;
        }



    }
}
