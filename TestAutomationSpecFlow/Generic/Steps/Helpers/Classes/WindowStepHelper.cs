﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class WindowStepHelper : StepHelper, IWindowStepHelper
    {
        private readonly ITargetForms targetForms;
        public WindowStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool SizeOfWindow(int width, int height)
        {
            DebugOutput.Log($"SizeOfWindow {width} {height} ");
            return SeleniumUtil.SetWindowSize(width, height);
        }

        public bool SizeOfWindowString(string compositeSize)
        {
            DebugOutput.Log($"SizeOfWindow {compositeSize}");
            int width = 800;
            int height = 800;
            if (compositeSize.ToLower() == "default")
            {
                compositeSize =  TargetConfiguration.Configuration.ScreenSize;
            }
            var sizes = StringValues.BreakUpByDelimited(compositeSize,"x");
            if (sizes.Count() != 2)
            {
                DebugOutput.Log($"You need an x and y! Delimited by X You gave us {compositeSize}");
                return false;
            }
            try
            {
                width = Int32.Parse(sizes[0]);
                height = Int32.Parse(sizes[1]);
            }
            catch
            {
                DebugOutput.Log($"Failed to convert {compositeSize} to ints");
                return false;
            }
            return SeleniumUtil.SetWindowSize(width, height);      
        }

        public bool CloseWindow(string windowsName)
        {
            DebugOutput.Log($"CloseWindow ");
            var element = GetWindowElement(windowsName);
            if (element != null)
            {
                SeleniumUtil.SendKey(element, "close");
                return true;
            }
            var windowsLocator = By.XPath("//Window/");
            var elements = SeleniumUtil.GetElements(windowsLocator, 0);
            if (elements == null) return false;
            if (elements.Count == 0 || elements.Count > 1)
            {
                DebugOutput.Log($"No window elements found! OR too many {elements.Count}");
                return false;   
            }
            element = elements[0];
            return SeleniumUtil.SendKey(element, "close");
        }

        public bool CloseTopElement()
        {
            DebugOutput.Log($"CloseTopElement ");
            var element = GetWindowElement("EditableTextBox");
            if (element == null)
            {
                DebugOutput.Log($"Failed to find by id");
                return false;
            }
            if (!SeleniumUtil.SendKey(element, "close"))
            {
                DebugOutput.Log($"have the element - just failed to close!");
                return false;
            }
            var yesButtonLocator = By.Name("Yes");
            var yesButtonElement = SeleniumUtil.GetElement(yesButtonLocator);
            if (yesButtonElement != null)
            {
                return SeleniumUtil.Click(yesButtonElement);
            }
            DebugOutput.Log($"failedty!");
            return false;
        }

        public bool IsDisplayed(string windowsName)
        {
            DebugOutput.Log($"IsDisplayed {windowsName}");
            var element = GetWindowElement(windowsName);
            if (element == null)
            {
                DebugOutput.Log($"No element to check");
                return false;
            }
            return element.Displayed;
        }

        public bool WriteInWindow(string documentName, string text)
        {
            By windowsLocator = By.Name(documentName);
            var element = SeleniumUtil.GetElement(windowsLocator);
            if (element == null) return false;
            return SeleniumUtil.EnterText(element, text);
        }

        private IWebElement? GetWindowElement(string windowsName)
        {
            By windowsLocator = By.Name(windowsName);
            var element = SeleniumUtil.GetElement(windowsLocator);
            if (element == null)
            {
                DebugOutput.Log($"Contains the name? {windowsName}");
                var xPath = $"//Window[contains(@Name,'{windowsName}')]";
                By xPathLocator = By.XPath(xPath);
                element = SeleniumUtil.GetElement(xPathLocator, 1);
            }
            return element;
        }

    }
}
