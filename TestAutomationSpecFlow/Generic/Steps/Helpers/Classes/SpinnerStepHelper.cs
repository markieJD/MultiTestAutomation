﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class SpinnerStepHelper : StepHelper, ISpinnerStepHelper
    {
        private readonly ITargetForms targetForms;
        public SpinnerStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private By[] SpinnerLocators = LocatorValues.locatorParser(TargetConfiguration.Configuration.SpinnerLocator);
        private int increments = 250;
        private int negativeTimeOut = TargetConfiguration.Configuration.NegativeTimeout;
        private int positiveTimeOut = TargetConfiguration.Configuration.PositiveTimeout;

        public bool StillDisplayed()
        {
            DebugOutput.Log($"Proc - StillDisplayed");
            //A spinner should apear, then disapear.
            bool displayed = false;
            var spinnerElement = GetSpinnerElement();
            if (spinnerElement == null)
            {
                DebugOutput.Log($"Can not find the element for spinner");
                return displayed;
            }
            if (!spinnerElement.Displayed)
            {
                if (!LoopTillDisplayed(spinnerElement, negativeTimeOut))
                {
                    DebugOutput.Log($"Element was never seen!  It exists but hidden (maybe quickly)");
                    return true;
                }
            }
            DebugOutput.Log($"Spinner is displayed!");
            if (LoopTillNotDisplayed(spinnerElement, positiveTimeOut))
            {
                DebugOutput.Log($"No longer displayed!");
                displayed = true;
                return displayed;
            }
            DebugOutput.Log($"We hit a timeout {positiveTimeOut} seconds");
            return displayed;
        }

        public bool SpinnerIsGone()
        {
            DebugOutput.Log($"Proc - SpinnerIsGone");
            bool spinnerGone = true;
            var spinnerElement = GetSpinnerElement();
            if (spinnerElement == null)
            {
                DebugOutput.Log($"Can Not find a spinner element! If we do not have an element, it can't be here! so gone is true!");
                return spinnerGone;
            }
            DebugOutput.Log($"We have a spinner element - is it displayed?");
            if (!SeleniumUtil.IsDisplayed(spinnerElement))
            {
                DebugOutput.Log($"We have a spinner - but its not displayed! Its gone!");
                return spinnerGone;
            }
            if (LoopTillNotDisplayed(spinnerElement, positiveTimeOut)) return spinnerGone;
            DebugOutput.Log($"WE have WAITED {positiveTimeOut} seconds and SPINNER is still spinning!");
            spinnerGone = false;
            return spinnerGone;
        }


        //Private

        private bool LoopTillDisplayed(IWebElement spinnerElement, int seconds)
        {
            DebugOutput.Log($"Proc - LoopTillDisplayed {spinnerElement} {seconds}");
            DebugOutput.Log($"Increments = {increments}");
            int secondsToMilli = seconds * 1000;
            int counter = 0;
            for (counter = 0; counter < secondsToMilli; counter = counter + increments)
            {
                if (spinnerElement.Displayed)   return true;
                Thread.Sleep(increments);
            }
            DebugOutput.Log($"Timed out! We waiting {seconds} for the spinner to appear ! NOTHING!");
            return false;
        }

        private bool LoopTillNotDisplayed(IWebElement spinnerElement, int seconds)
        {
            DebugOutput.Log($"Proc - LoopTillNotDisplayed {spinnerElement} {seconds}");
            DebugOutput.Log($"Increments = {increments}");
            int secondsToMilli = seconds * 1000;
            int counter = 0;
            for (counter = 0; counter < secondsToMilli; counter = counter + increments)
            {
                if (!spinnerElement.Displayed) return true;
                Thread.Sleep(increments);
            }
            DebugOutput.Log($"Timed out! And its still spinning!");
            return false;
        }

        private IWebElement? GetSpinnerElement()
        {
            DebugOutput.Log($"Proc - GetSpinnerElement");
            try
            {
                if (CurrentPage == null) return null;
            }
            catch
            {
                DebugOutput.Log($"We have no current page - first page loading I guess!");
                return null;
            }
            DebugOutput.Log($"Checking spinner for page {CurrentPage.Name}");
            if (CurrentPage.Elements.TryGetValue("spinner", out var spinnerLocator))
            if (spinnerLocator != null)
            {
                spinnerLocator = CurrentPage.Elements["spinner"];
                var spinnerElement = SeleniumUtil.GetElement(spinnerLocator,1);
                if (spinnerElement != null) return spinnerElement;
            }
            DebugOutput.Log($"No or not found the pages OWN spinner!");
            var howManyOtherSpinnerLocators = SpinnerLocators.Length;
            DebugOutput.Log($"Checking the other {howManyOtherSpinnerLocators} locators");
            foreach(var spinner in SpinnerLocators)
            {
                var spinnerElement = SeleniumUtil.GetElement(spinner, 1);
                if (spinnerElement != null) return spinnerElement;
            }
            DebugOutput.Log($"Failed to find ANY Spinner!");
            return null;
        }

    }
}
