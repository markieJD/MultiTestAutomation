﻿using Core;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class ChipStepHelper : StepHelper, IChipStepHelper
    {
        private readonly ITargetForms targetForms;
        public ChipStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private By ChipLocator { get; set; } = By.TagName("alert");
        private By ChipTextLocator { get; set; } = By.ClassName("alert-info");
        private By ChipCloseLocator { get; set; } = By.TagName("button");

        public bool ArraryContainsChip(string chipArraryName, string chipName)
        {
            DebugOutput.Log($"ArraryContainsChip {chipArraryName} {chipName}");
            var chipArrayElement = GetChipArraryElement(chipArraryName);
            if (chipArrayElement == null) return false;
            var chips = GetAllChipsInArrary(chipArrayElement);
            if (chips.Count < 1) return false;
            DebugOutput.Log($"Get chips name!");
            foreach (var chip in chips)
            {
                var chipTextElement = SeleniumUtil.GetElementUnderElement(chip, ChipTextLocator, 1);
                if (chipTextElement != null)
                {
                    var chipText = SeleniumUtil.GetElementText(chipTextElement);
                    DebugOutput.Log($"We have the text of {chipText} from the chip {chip}");
                    if (chipText.Contains(chipName)) return true;
                }
            }
            DebugOutput.Log($"No match with {chipName} chip by name");
            return false;
        }

        public bool CloseChip(string chipArrayName, string chipName)
        {
            DebugOutput.Log($"CloseChip {chipArrayName} {chipName}");
            var chipArrayElement = GetChipArraryElement(chipArrayName);
            if (chipArrayElement == null) return false;
            var chips = GetAllChipsInArrary(chipArrayElement);
            if (chips.Count < 1) return false;
            DebugOutput.Log($"Get chips name!");
            foreach (var chip in chips)
            {
                var chipTextElement = SeleniumUtil.GetElementUnderElement(chip, ChipTextLocator, 1);
                if (chipTextElement != null)
                {
                    var chipText = SeleniumUtil.GetElementText(chipTextElement);
                    DebugOutput.Log($"We have the text of {chipText} from the chip {chip}");
                    if (chipText.Contains(chipName))
                    {
                        var closeElement = SeleniumUtil.GetElementUnderElement(chip, ChipCloseLocator, 1);
                        if (closeElement == null) return false;
                        return SeleniumUtil.Click(closeElement);
                    }
                }
            }
            DebugOutput.Log($"No match with {chipName} chip by name");
            return false;

        }

        public bool IsDisplayed(string chipArrayName)
        {
            DebugOutput.Log($"IsDisplayed {chipArrayName}");
            var chipArrayElement = GetChipArraryElement(chipArrayName);
            if (chipArrayElement == null) return false;
            return chipArrayElement.Displayed;
        }


        //PRIVATE

        private List<IWebElement> GetAllChipsInArrary(IWebElement chipArraryElement)
        {
            DebugOutput.Log($"GetAllChipsInArrary {chipArraryElement}");
            List<IWebElement> chips = new List<IWebElement>();
            if (chipArraryElement == null) return chips;
            chips = SeleniumUtil.GetElementsUnder(chipArraryElement, ChipLocator);
            DebugOutput.Log($"We have {chips.Count()} chippies found in array");
            return chips;
        }

        private IWebElement? GetChipArraryElement(string chipArraryName, int timeout = 1)
        {
            DebugOutput.Log($"GetChipArraryElement {chipArraryName}");
            var chipArrayLocator = ElementName.GetElementLocator(chipArraryName, CurrentPage, "chip");
            if (chipArrayLocator == null) return null;
            var namedChipArraryElement = SeleniumUtil.GetElement(chipArrayLocator, timeout);
            if (namedChipArraryElement == null)
            {
                DebugOutput.Log($"Can not find chip arrary by {chipArrayLocator} on page {CurrentPage}");
                return null;
            }
            return namedChipArraryElement;
        }

    }
}
