﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class TreeStepHelper : StepHelper, ITreeStepHelper
    {
        private readonly ITargetForms targetForms;
        public TreeStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        //1st is web, 2nd is Windows
        private readonly By[] TreeNodeLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.TreeNodeLocator);
        private readonly By[] TreeNodeToggleLocator = LocatorValues.locatorParser(TargetConfiguration.Configuration.TreeNodeToggleLocator);
        private readonly By[] TreeNodeSelector = LocatorValues.locatorParser(TargetConfiguration.Configuration.TreeNodeSelector);
        private readonly String[] TreeAddNodeText = TargetConfiguration.Configuration.TreeAddNodeText;
        private readonly String[] TreeAddNodeButton = TargetConfiguration.Configuration.TableNextPageButton;

        /// <summary>
        /// Is the tree displayed
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        public bool IsDisplayed(string treeName)
        {
            DebugOutput.Log($"IsDisplayed {treeName}");
            var treeElement = GetTreeElement(treeName);
            if (treeElement == null) return false;
            return treeElement.Displayed;
        }

        /// <summary>
        /// Checks the list of Node Names to see if the nodeName is in there (case sensative)
        /// </summary>
        /// <param name="treeName"></param>
        /// <param name="nodeName"></param>
        /// <returns>true if found!</returns>
        public bool IsNodeExist(string treeName, string nodeName)
        {
            DebugOutput.Log($"Proc - IsNodeDisplayed {treeName} {nodeName}");
            var listOfNodeNames = NodesList(treeName);
            if (listOfNodeNames.Contains(nodeName)) return true;
            return false;
        }

        /// <summary>
        /// Get all the node names
        /// </summary>
        /// <param name="treeName"></param>
        /// <param name="nodeNameValue"></param>
        /// <returns></returns>
        public List<string> NodesList(string treeName)
        {
            DebugOutput.Log($"Proc - NodesDisplayed {treeName}");
            List<string> nodeNames = new List<string>();
            var treeElement = GetTreeElement(treeName);
            if (treeElement == null) return nodeNames;
            nodeNames = NodesList(treeElement);
            return nodeNames;
        }

        private List<string> NodesList(IWebElement treeElement)
        {
            DebugOutput.Log($"Proc - NodesDisplayed IwebElement {treeElement}");
            List<string> nodeNames = new List<string>();
            var treeNodeElements = GetAllNodeElements(treeElement);
            foreach (var nodeElement in treeNodeElements)
            {
                var text = SeleniumUtil.GetElementText(nodeElement);
                if (text != null)
                {
                    nodeNames.Add(text);
                }
            }
            return nodeNames;
        }

        public bool ConfirmNode(string fullNode, string treeName)
        {
            DebugOutput.Log($"Proc - ConfirmNode {fullNode} {treeName}");
            var nodes = StringValues.BreakUpByDelimited(fullNode, "|");
            DebugOutput.Log($"Found, {nodes.Length} NODE names in node '|' is a delimiter");
            var treeElement = GetTreeElement(treeName);
            if (treeElement == null)
            {
                DebugOutput.Log($"NO tree found!");
                return false;
            }
            var topElement = GetTopNodeOfTree(treeElement);
            if (topElement == null)
            {
                DebugOutput.Log($"No top Element!");
                return false;
            }
            int counter = 1;
            IWebElement parentNodeElement = topElement;
            string parentNodeName = GetNameOfNode(parentNodeElement);
            if (parentNodeName == fullNode)
            {
                DebugOutput.Log($"TOP OF THE TREE ONLY!");
                return true;
            }
            foreach (var node in nodes)
            {
                DebugOutput.Log($"{counter} node {node} ");
                if (counter != 1)
                {
                    if (!NodeExistUnderNode(parentNodeElement, node))
                    {
                        DebugOutput.Log($"Node {node} not found under parent {parentNodeName}");
                        EnterNodeDetails(node);
                    }
                    var allNodesElement = GetNodesUnderNode(parentNodeElement);
                    foreach (var nodeElement in allNodesElement)
                    {
                        var name = GetNameOfNode(nodeElement);
                        if (name == node)
                        {
                            DebugOutput.Log($"We have a match - lets make it parent!");
                            parentNodeElement = nodeElement;
                        }
                    }
                }
                else
                {
                    DebugOutput.Log($"TOP IS A ASSUMPTION!");
                }
                if (!SeleniumUtil.Click(parentNodeElement))
                {
                    DebugOutput.Log($"FAILED TO CLICK ON PARENT! {parentNodeElement}");
                    return false;
                }
                counter++;
            }
            DebugOutput.Log($"To get to here - it must exist!");
            return true;
        }

        public bool EnterNodeDetails(string nodeName)
        {
            DebugOutput.Log($"Proc - EnterNodeDetails {nodeName}");
            IWebElement? textElement = null;
            IWebElement? addButtonElement = null;
            foreach (var textElementName in TreeAddNodeText)
            {
                textElement = GetTreeElement(textElementName);
                if (textElement != null)
                    break;
            }
            foreach (var buttonName in TreeAddNodeButton)
            {
                addButtonElement = GetTreeElement(buttonName);
                if (addButtonElement != null)
                    break;
            }
            if (textElement == null || addButtonElement == null)
            {
                DebugOutput.Log($"Failed to get either the button to text element!");
                return false;
            }
            if (!SeleniumUtil.ClearThenEnterText(textElement, nodeName, "tab"))
            {
                DebugOutput.Log($"Failed to enter text into text box to create node!");
                return false;
            }
            if (!SeleniumUtil.Click(addButtonElement))
            {
                DebugOutput.Log($"Failed to click after entering text!");
                return false;
            }
            DebugOutput.Log($"The node {nodeName} should now exist!");
            return true;
        }

        public bool NodeExistUnderNode(IWebElement parentNode, string nodeName)
        {
            DebugOutput.Log($"Proc - NodeExistUnderNode {parentNode} {nodeName}");
            var allNodesUnderParent = GetNodesUnderNode(parentNode);
            List<string> nameOfNodes = new List<string>();
            foreach (var node in allNodesUnderParent)
            {
                var name = GetNameOfNode(node);
                if (name == "")
                {
                    DebugOutput.Log($"Failure here!");
                    return false;
                }
                if (name == nodeName) return true;
            }
            DebugOutput.Log($"Cycled through all the nodes, it does not exist!");
            return false;
        }

        public bool SelectNodeFromTree(string nodeName, string treeName)
        {
            DebugOutput.Log($"Proc - SelectNodeFromTree {nodeName} {treeName}");
            if (nodeName == null) return false;
            var treeElement = GetTreeElement(treeName);
            if (treeElement == null) return false;
            var nodeElements = GetAllNodeElements(treeElement);
            if (nodeElements.Count == 0) return false;
            //use the TreeNodeSelector
            DebugOutput.Log($"We have gotten {nodeElements.Count()} Node elements returned!");

            foreach (var nodeElement in nodeElements)
            {
                var nameOfElement = SeleniumUtil.GetElementtextDirect(nodeElement);
                //Leaf is bringing back all text UNDER the element too!
                if (nodeName == null) return false;
                var nodeNameLength = nodeName.Length;
                if (nameOfElement != null) 
                {
                    if (nameOfElement.Length > nodeNameLength)
                    {
                        nameOfElement = nameOfElement.Substring(0, nodeNameLength);
                    }
                }
                DebugOutput.Log($"<<<>>> {nameOfElement}");
                if (nameOfElement == nodeName)
                {
                    DebugOutput.Log($"Found the element for {nodeName} now to do something with it, like click on it!");
                    foreach (var locator in TreeNodeSelector)
                    {
                        DebugOutput.Log($"Trying to us {locator} first to see if something specific!");
                        var nodeClickableBit = SeleniumUtil.GetElementsUnder(nodeElement, locator);
                        if (nodeClickableBit.Count() == 1)
                        {
                            if (SeleniumUtil.Click(nodeClickableBit[0]))
                            {
                                DebugOutput.Log($"WE have clicked on something ! Using {locator}");
                                return true;
                            }
                        }
                    }
                    DebugOutput.Log($"Not found by locator so jumping in HARD CICK!");
                    return SeleniumUtil.Click(nodeElement);
                }
            }
            DebugOutput.Log($"Failed to find the node in tree!");
            return false;
        }

        public bool ExpandNodeInTree(string treeName, string nodeName)
        {
            DebugOutput.Log($"Proc - SelectNodeFromTree {nodeName} {treeName}");
            var treeElement = GetTreeElement(treeName);
            if (treeElement == null) return false;
            var nodeElements = GetAllNodeElements(treeElement);
            if (nodeElements.Count == 0) return false;
            DebugOutput.Log($"WE have found {nodeElements.Count} node elements in the tree {treeName}");
            //Find all nodes, find the one called nodeName, then expand it using TreeNodeToggleLocator
            foreach (var nodeElement in nodeElements)
            {
                var nameOfElement = SeleniumUtil.GetElementText(nodeElement);
                if (nameOfElement == nodeName)
                {
                    DebugOutput.Log($"Found the element for {nodeName}");
                    return ExpandNode(nodeElement);
                }
            }
            DebugOutput.Log($"Failed to find the node in tree!");
            return false;
        }

        public bool ExpandNode(IWebElement nodeElement)
        {
            DebugOutput.Log($"Proc - ExpandNode {nodeElement}");
            if (nodeElement == null) return false;
            foreach (var locator in TreeNodeToggleLocator)
            {
                DebugOutput.Log($"Using the locator {locator} to find the expander thing");
                var expanderElements = SeleniumUtil.GetElementsUnder(nodeElement, locator);
                if (expanderElements.Count() > 0)
                {
                    DebugOutput.Log($"Have found a button/expander thing!");
                    if (expanderElements.Count() > 1)
                    {
                        DebugOutput.Log($"Some would say that {expanderElements.Count()} is actually too many! and I would be one of them!");
                        return false;
                    }
                    return SeleniumUtil.Click(expanderElements[0]);
                }
            }
            DebugOutput.Log($"Failed to find the buttons things to expand using these locators!");
            return false;
        }



        // PRIVATE

        private string GetNameOfNode(IWebElement nodeElement)
        {
            DebugOutput.Log($"Proc - GetNameOfNode {nodeElement}");
            if (nodeElement == null) return "";
            var value = SeleniumUtil.GetElementText(nodeElement);
            if (value != null)
            {
                DebugOutput.Log($"Name of the node {nodeElement} is {value}");
                return value;
            }
            DebugOutput.Log($"We still have null for name of {nodeElement}");
            return "";
        }

        private List<IWebElement> GetNodesUnderNode(IWebElement parentNode)
        {
            DebugOutput.Log($"Proc - GetNodesUnderNode {parentNode}");
            List<IWebElement> nodesUnderNode = new List<IWebElement>();
            foreach (var nodeLocator in TreeNodeLocator)
            {
                nodesUnderNode = SeleniumUtil.GetElementsUnder(parentNode, nodeLocator);
                if (nodesUnderNode.Count > 0) return nodesUnderNode;
            }
            DebugOutput.Log($"Failed to get anything to return here!");
            return nodesUnderNode;
        }

        private IWebElement? GetTopNodeOfTree(IWebElement treeElement)
        {
            DebugOutput.Log($"Proc - GetTopNodeOfTree {treeElement}");
            var numberOfNodeLocators = TreeNodeLocator.Length;
            Thread.Sleep(3/1000);
            DebugOutput.Log($"WE HAVE {numberOfNodeLocators} node locators to go through!");
            foreach(var nodeLocator in TreeNodeLocator)
            {
                DebugOutput.Log($"Getting top element using {nodeLocator}");
                var allNodeElements = SeleniumUtil.GetElementsUnder(treeElement, nodeLocator);
                if (allNodeElements.Count > 0)
                {
                    DebugOutput.Log($"Found {allNodeElements.Count} looking under tree, we want the top 1");
                    return allNodeElements[0];
                }
            }
            DebugOutput.Log($"Failed to get any element!");
            return null;
        }

        private List<IWebElement> GetAllNodeElements(IWebElement treeElement)
        {
            DebugOutput.Log($"Proc - GetAllNodeElements {treeElement}");
            List<IWebElement> result = new List<IWebElement>();
            var numberOfNodeLocators = TreeNodeLocator.Length;
            DebugOutput.Log($"WE HAVE {numberOfNodeLocators} node locators to check through!");
            Thread.Sleep(3/1000);
            foreach (var locator in TreeNodeLocator)
            {
                DebugOutput.Log($"Using locator {locator}");
                var nodeElements = SeleniumUtil.GetElementsUnder(treeElement, locator);
                DebugOutput.Log($"WE have {nodeElements.Count} nodes returned!");
                if (nodeElements.Count > 0)
                {
                    foreach (var element in nodeElements)
                    {
                        result.Add(element);
                    }
                    return result;
                }
            }
            return result;
        }

        /// <summary>
        /// Find the tree element
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        private IWebElement? GetTreeElement(string treeName, string elementType = "tree")
        {
            DebugOutput.Log($"GetTreeElement {treeName}");
            var treeLocator = ElementName.GetElementLocator(treeName, CurrentPage, elementType);
            if (treeLocator == null) return null;   
            DebugOutput.Log($"We have the LOCATOR for Tree {treeName} {treeLocator}");
            var element = SeleniumUtil.GetElement(treeLocator, 1);
            DebugOutput.Log($"Tree Element {treeLocator} = {element}");
            return element;
        }
    }
}
