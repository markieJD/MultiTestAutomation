﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class AlertStepHelper : StepHelper, IAlertStepHelper
    {
        private readonly ITargetForms targetForms;
        public AlertStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool IsDisplayed(string alertMessage)
        {
            return SeleniumUtil.AlertDisplayed(alertMessage);
        }

        public bool Accept()
        {
            return SeleniumUtil.AlertClickAccept();
        }

        public bool Cancel()
        {
            return SeleniumUtil.AlertClickCancel();
        }

        public bool SendKeys(string text)
        {
            return SeleniumUtil.AlertInput(text);
        }


    }


}
