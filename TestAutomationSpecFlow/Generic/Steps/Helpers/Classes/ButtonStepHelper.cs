﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class ButtonStepHelper : StepHelper, IButtonStepHelper
    {
        private readonly ITargetForms targetForms;
        public ButtonStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool IsSelected(string buttonName, int timeout =0)
        {
            DebugOutput.Log($" Proc - IsActive {buttonName}");
            var buttonElement = GetButtonElement(buttonName, timeout);
            if (buttonElement == null) return false;
            if (buttonElement.Selected == true) return true;
            DebugOutput.Log($"Not selected - is active?");
            var className = SeleniumUtil.GetElementAttributeValue(buttonElement, "class");
            DebugOutput.Log($"FULL CLASS = {className}");
            if (className.ToLower().Contains(("active"))) return true;
            return false;
        }

        public bool IsNotSelected(string buttonName, int timeout = 0)
        {
            DebugOutput.Log($" Proc - IsActive {buttonName}");
            var buttonElement = GetButtonElement(buttonName, timeout);
            if (buttonElement == null) return false;
            if (buttonElement.Selected == true) return false;
            DebugOutput.Log($"Not selected - is active?");
            var className = SeleniumUtil.GetElementAttributeValue(buttonElement, "class");
            DebugOutput.Log($"FULL CLASS = {className}");
            if (className.ToLower().Contains(("active"))) return false;
            return true;
        }

        public bool IsDisplayed(string buttonName, int timeout = 0)
        {
            DebugOutput.Log($"IsDisplayed {buttonName}");
            var buttonElement = GetButtonElement(buttonName, timeout);
            if (buttonElement == null) return false;
            return SeleniumUtil.IsDisplayed(buttonElement);
        }

        public bool IsEnabled(string buttonName)
        {
            DebugOutput.Log($"IsEnabled {buttonName}");
            var buttonElement = GetButtonElement(buttonName, 1);
            if (buttonElement == null)
            {
                DebugOutput.Log($"Failure to get ELEMENT for button {buttonName}");
                return false;
            }
            return SeleniumUtil.IsEnabled(buttonElement);
        }

        public bool ClickButton(string buttonName)
        {
            DebugOutput.Log($"ClickButton {buttonName}");
            var buttonElement = GetButtonElement(buttonName);
            if (buttonElement == null)
            {
                DebugOutput.Log($"Failure to get ELEMENT for button {buttonName}");
                return false;
            }
            return SeleniumUtil.Click(buttonElement);
        }

        public bool DoubleClick(string buttonName)
        {
            DebugOutput.Log($"ClickButton {buttonName}");
            var buttonElement = GetButtonElement(buttonName);
            if (buttonElement == null) return false;
            return SeleniumUtil.DoubleClick(buttonElement);
        }

        public bool RightClick(string buttonName)
        {
            DebugOutput.Log($"ClickButton {buttonName}");
            var buttonElement = GetButtonElement(buttonName);
            if (buttonElement == null) return false;
            return SeleniumUtil.RightClick(buttonElement);
        }

        public bool MouseOver(string buttonName)
        {
            DebugOutput.Log($"MouseOver {buttonName}");
            var buttonElement = GetButtonElement(buttonName);
            if (buttonElement == null) return false;
            return SeleniumUtil.MoveToElement(buttonElement);
        }

        public bool ClickNthButton(string buttonName, string nTh)
        {
            DebugOutput.Log($"ClickNthButton {buttonName} {nTh}");
            var buttonLocator = ElementName.GetElementLocator(buttonName, CurrentPage, "button");
            if (buttonLocator == null) return false;
            var buttonElement = SeleniumUtil.GetNthElementBy(buttonLocator, nTh);
            if (buttonElement == null) return false;
            return SeleniumUtil.Click(buttonElement);
        }

        public bool DragAToB(string buttonAName, string buttonBName)
        {
            DebugOutput.Log($"DragAToB {buttonAName} {buttonBName}");
            var buttonAElement = GetButtonElement(buttonAName);
            if (buttonAElement == null) return false;
            var buttonBElement = GetButtonElement(buttonBName);
            if (buttonBElement == null) return false;
            return SeleniumUtil.DragElementToElement(buttonAElement, buttonBElement);
        }


        //  PRIVATE

        private IWebElement? GetButtonElement(string buttonName, int timeout = 0)
        {
            DebugOutput.Log($"GetButtonElement {buttonName}");
            var buttonLocator = ElementName.GetElementLocator(buttonName, CurrentPage, "button");
            if (buttonLocator != null)
            {
                DebugOutput.Log($"Have the locator for the button {buttonLocator} from page {CurrentPage}");
                var namedButtonElement = SeleniumUtil.GetElement(buttonLocator, timeout);
                if (namedButtonElement != null) return namedButtonElement;
            }
            return GetButtonElementBackup(buttonName);
        }

        private IWebElement? GetButtonElementBackup(string buttonName)
        {
            DebugOutput.Log($"GetButtonElementBackup {buttonName}");
            var locator = By.Name(buttonName);
            var namedButtonElement = SeleniumUtil.GetElement(locator, 1);
            if (namedButtonElement == null)
            {
                locator = By.TagName("button");
                var allButtonElements = SeleniumUtil.GetElements(locator,1);
                if (allButtonElements != null)
                {
                    foreach (var buttonElement in allButtonElements)
                    {
                        var allElementsBelowButton = SeleniumUtil.GetAllElementsUnderElement(buttonElement);
                        var textGotten = SeleniumUtil.GetElementText(buttonElement);
                        if (textGotten.ToLower() == buttonName.ToLower()) return buttonElement;
                        if (textGotten.ToLower().Contains(buttonName.ToLower())) return buttonElement;     
                        foreach (var subElement in allElementsBelowButton)
                        {
                            textGotten = SeleniumUtil.GetElementText(subElement);
                            if (textGotten.ToLower() == buttonName.ToLower()) return buttonElement;
                            if (textGotten.ToLower().Contains(buttonName.ToLower())) return buttonElement;                            
                        }
                    }
                }
            }
            return namedButtonElement;
        }
    }
}
