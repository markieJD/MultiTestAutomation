﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class DatePickerStepHelper : StepHelper, IDatePickerStepHelper
    {
        private readonly ITargetForms targetForms;
        public DatePickerStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private readonly By[] OpenDatePickerButton = LocatorValues.locatorParser(TargetConfiguration.Configuration.DatePickerButtonOpen);
        private readonly By[] DatePickerText = LocatorValues.locatorParser(TargetConfiguration.Configuration.DatePickerText);

        public bool IsDisplayed(string datePickerName, int timeout = 0)
        {
            DebugOutput.Log($"IsDisplayed {datePickerName}");
            var datePickerElement = GetDatePickerElement(datePickerName, timeout);
            if (datePickerElement == null) return false;
            return datePickerElement.Displayed;
        }

        public bool SetDateValue(string datePickerName, string date, string dateFormat = "", int timeOut = 0)
        {
            DebugOutput.Log($"SetDateValue {datePickerName} {date}");
            date = StringValues.TextReplacementService(date);
            DebugOutput.Log($"SetDateValue texted {datePickerName} {date}");
            var currentValue = GetCurrentValue(datePickerName, timeOut);
            if (date == currentValue)
            {
                DebugOutput.Log($"These already match ! {date}");
                return true;
            }
            DebugOutput.Log($"They do not CURRENTLY match {date} wanted but got {currentValue}");
            return EnterValueInDatePicker(datePickerName, date, timeOut);
        }

        public bool EnterValueInDatePicker(string datePickerName, string date, int timeOut)
        {
            DebugOutput.Log($"EnterValueInDatePicker {datePickerName} {date}");
            var datePickerElement = GetDatePickerElement(datePickerName, timeOut);
            if (datePickerElement == null) return false;
            //Click On Text
            if (!SeleniumUtil.Click(datePickerElement)) return false;
            // We open the datepicker, we need the text element below it!
            int counter = 0;
            int numberOfDatePickerText = DatePickerText.Count();
            while (counter < numberOfDatePickerText)
            {
                DebugOutput.Log($"Get the text element below");
                var datePickerTextElements = SeleniumUtil.GetElementsUnder(datePickerElement, DatePickerText[counter], 1);
                if (datePickerTextElements.Count == 1)
                {
                    if (!SeleniumUtil.ClearThenEnterText(datePickerTextElements[0], date)) return false;
                    var newValue = GetCurrentValue(datePickerName, timeOut);
                    DebugOutput.Log($"Comparing DatePICKER {newValue} with {date}");
                    if (newValue == date) return true;
                    DebugOutput.Log($"Failed to be the same {newValue} is current value");

                }
                counter++;
            }
            DebugOutput.Log($"Cycled through - failed!");
            return false;
        }

        public string GetCurrentValue(string datePickerName, int timeOut)
        {
            DebugOutput.Log($"GetCurrentValue {datePickerName}");
            var datePickerElement = GetDatePickerElement(datePickerName, timeOut);
            if (datePickerElement == null) return "";
            //We have the element, now we want its value.  It is contained in an element below the element
            int counter = 0;
            int numberOfTexts = DatePickerText.Count();
            while (counter < numberOfTexts)
            {
                var datePickerTextElement = SeleniumUtil.GetElementsUnder(datePickerElement, DatePickerText[counter], 1);
                var datePickerTextValue = SeleniumUtil.GetElementText(datePickerTextElement[counter]);
                if (datePickerTextValue != null)
                {
                    DebugOutput.Log($"Returning current date of datepicker as {datePickerTextValue}");
                    return datePickerTextValue;
                }
                counter++;
            }
            DebugOutput.Log($"Failed to find any text");
            return "";
        }


        private IWebElement? GetDatePickerElement(string datePickerName, int timeOut)
        {
            DebugOutput.Log($"GetDatePickerElement {datePickerName}");

            var datePickerLocator = ElementName.GetElementLocator(datePickerName, CurrentPage, "datepicker");
            if (datePickerLocator == null) return null;
            var datePickerElement = SeleniumUtil.GetElement(datePickerLocator, timeOut);
            if (datePickerElement == null)
            {
                DebugOutput.Log($"Can not find element by {datePickerElement} on page {CurrentPage}");
                return null;
            }
            return datePickerElement;
        }


    }
}
