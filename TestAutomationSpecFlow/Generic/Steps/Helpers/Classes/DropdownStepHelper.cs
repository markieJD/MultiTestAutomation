﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class DropdownStepHelper : StepHelper, IDropdownStepHelper
    {
        private readonly ITargetForms targetForms;
        public DropdownStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        private readonly By[] DropDownItemLocators = LocatorValues.locatorParser(TargetConfiguration.Configuration.DropDownItemLocators);

        public bool Click(string dropdownName)
        {
            DebugOutput.Log($"Click {dropdownName}");
            var dropdownElement = GetDropdownElement(dropdownName);
            if (dropdownElement == null) return false;
            return SeleniumUtil.Click(dropdownElement);
        }

        public bool IsDisplayed(string dropdownName, int timeout = 0)
        {
            DebugOutput.Log($"IsDisplayed {dropdownName}");
            var dropdownElement = GetDropdownElement(dropdownName, timeout);
            if (dropdownElement == null) return false;
            return dropdownElement.Displayed;
        }

        public bool EnterThenSelectFrom(string selecting, string dropDownName, int timeOut = 0)
        {
            DebugOutput.Log($"Proc - EnterThenSelectFrom {selecting} {dropDownName}");
            var dropdownElement = GetDropdownElement(dropDownName, timeOut);
            if (dropdownElement == null) return false;
            DebugOutput.Log($"Have to open it up");
            if (!SeleniumUtil.Click(dropdownElement)) return false;
            if (!SeleniumUtil.EnterText(dropdownElement, selecting)) return false;
            if (!SeleniumUtil.SendKey(dropdownElement, "enter")) return false;
            return true;
        }

        public bool SelectingFromWithoutText(string selecting, string dropdownName, int timeout = 0, bool topOptionAlreadySelected = false)
        {
            return SelectingFrom(selecting, dropdownName, timeout, topOptionAlreadySelected, false);
        }

        public bool SelectingFrom(string selecting, string dropdownName, int timeout = 0, bool topOptionAlreadySelected = false, bool textEntry = true)
        {
            DebugOutput.Log($"Proc - SelectingFrom {selecting} {dropdownName}");
            var dropdownElement = GetDropdownElement(dropdownName, timeout);
            if (dropdownElement == null) return false;
            bool alreadyOpen = false;
            foreach (var locator in DropDownItemLocators)
            {
                var allOptions = SeleniumUtil.GetElementsUnder(dropdownElement, locator, 5);
                if (allOptions.Count > 0)
                {
                    alreadyOpen = true;
                }
            }
            if (!alreadyOpen)
            {
                DebugOutput.Log($"Lets click on the drop down to open it up!");
                if (!SeleniumUtil.Click(dropdownElement)) return false;
            }
            var howManyWaysToFindOptions = DropDownItemLocators.Length;
            if (textEntry)
            {
                if (TypeInValue(dropdownElement, selecting))
                {
                    SeleniumUtil.SendKey(dropdownElement, "enter");
                    return true;
                } 
            }
            foreach (var locator in DropDownItemLocators)
            {
                var allOptions = SeleniumUtil.GetElementsUnder(dropdownElement, locator, 5);
                DebugOutput.Log($"There are {allOptions.Count()} options under this dropdown! {locator}");
                if (allOptions.Count > 0)
                {
                    int optionCounter = 0;
                    //Some times the dropdown list, the top option is already selected
                    if (!topOptionAlreadySelected)
                    {
                        optionCounter = 1;
                    }
                    bool found = false;
                    foreach (var option in allOptions)
                    {
                        var text = SeleniumUtil.GetElementAttributeValue(option, "Name");
                        if (selecting == text)
                        {
                            DebugOutput.Log($"Match on NAME");
                            found = true;
                            if (SeleniumUtil.Click(option)) return true;
                        }
                        text = SeleniumUtil.GetElementText(option);
                        if (selecting == text)
                        {
                            DebugOutput.Log($"Match on TEXT");
                            found = true;
                            if (SeleniumUtil.Click(option)) return true;
                        }
                        if (found)
                        {
                            DebugOutput.Log($"We have a match at option {optionCounter} but failed to click on it (sometimes the option is displayed behind 1 element)");
                            for (int i = 0; i < optionCounter; i++)
                            {
                                SeleniumUtil.SendKey(dropdownElement, "down arrow");
                                Thread.Sleep(100);
                            }
                            return SeleniumUtil.SendKey(dropdownElement, "enter");
                        }
                        optionCounter++;
                    }
                }
                else
                {
                    DebugOutput.Log($"no options to select for locator {locator}!");
                }
            }
            var allElements = SeleniumUtil.GetAllElementsUnderElement(dropdownElement);
            DebugOutput.Log($"FOUND {allElements.Count()} elements under the dropdown!");
            foreach(var element in allElements)
            {
                var elementName = SeleniumUtil.GetElementAttributeValue(element, "Name");
                DebugOutput.Log($"NAME = {elementName}");
                if (elementName == selecting)
                {
                    return SeleniumUtil.Click(element);
                }
            }
            DebugOutput.Log($"The option {selecting} was simply not found!");
            return false;
        }

        public string? GetCurrentValue(string dropdownName, int timeout = 0)
        {
            DebugOutput.Log($"Proc - GetCurrentValue {dropdownName}");
            var dropdownElement = GetDropdownElement(dropdownName, timeout);
            if (dropdownElement == null) return null;
            By optionLocator = GetOptionsLocator();
            var options = SeleniumUtil.GetElementsUnder(dropdownElement, optionLocator, 1);
            if (options.Count == 0)
            {
                DebugOutput.Log($"No options need to check text!");
                By Locator = By.ClassName("css-1uccc91-singleValue");
                var dropdownTextElement = SeleniumUtil.GetElementUnderElement(dropdownElement, Locator, 1);
                if (dropdownTextElement == null) 
                {
                    Locator = By.ClassName("display-text");
                    dropdownTextElement = SeleniumUtil.GetElementUnderElement(dropdownElement, Locator, 1);
                    if (dropdownTextElement == null) return null;
                }
                return dropdownTextElement.Text;
            }
            foreach (var option in options)
            {
                if (option.Selected)
                {
                    return option.Text;
                }
            }
            return null;
        }

        public bool ContainsValue(string dropdownName, string value, int timeout = 0)
        {
            DebugOutput.Log($"Proc - ContainsValue {dropdownName} {value}");
            var listOfOptions = GetAllValues(dropdownName);
            if (listOfOptions.Count() == 0) return false;
            foreach (var option in listOfOptions)
            {
                DebugOutput.Log($"Comparing {value} with {option}");
                if (option == value) return true;
            }
            DebugOutput.Log($"Failed to find {value}");
            return false;
        }

        public List<string> GetAllValues(string dropdownName, int timeout = 0)
        {
            DebugOutput.Log($"Proc - GetCurrentValue {dropdownName}");
            var options = new List<string>();
            var dropdownElement = GetDropdownElement(dropdownName, timeout);
            if (dropdownElement == null) return options;
            bool foundOptions = false;
            foreach (var locator in DropDownItemLocators)
            {
                var allOptions = SeleniumUtil.GetElementsUnder(dropdownElement, locator, 5);
                if (allOptions.Count > 0)
                {
                    foundOptions = true;
                }
            }
            if (!foundOptions)
            {
                if (!SeleniumUtil.Click(dropdownElement)) return options;
                DebugOutput.Log($"Click to open the options!");
            }
            foreach(var locator in DropDownItemLocators)
            {
                var allOptions = SeleniumUtil.GetElementsUnder(dropdownElement, locator, 5);
                DebugOutput.Log($"There are {allOptions.Count()} options under this dropdown! {locator}");
                if (allOptions.Count > 0)
                {
                    foreach (var option in allOptions)
                    {
                        var elementText = SeleniumUtil.GetElementText(option);
                        DebugOutput.Log($"Adding {elementText} to list");
                        options.Add(elementText);
                    }
                    return options;
                }
                DebugOutput.Log($"No options found for this locator");
            }
            DebugOutput.Log($"Failed to find any options!");
            return options;
        }



        /// <summary>
        /// PRIVATE BELOW
        /// </summary>
        /// <param name="dropdownElement"></param>
        /// <param name="optionName"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        /// 

        private By GetOptionsLocator()
        {
            return By.TagName("option");
        }

        private IWebElement? GetDropdownOption(IWebElement dropdownElement, string optionName, int timeout = 0)
        {
            DebugOutput.Log($"Proc - GetDropdownOption {dropdownElement}");
            var optionXPath = By.XPath($"//option[text() = '{optionName}']");
            return SeleniumUtil.GetElementUnderElement(dropdownElement, optionXPath, timeout);
        }

        private bool TypeInValue(IWebElement dropdownElement, string selecting, int timeout = 0)
        {
            DebugOutput.Log($"Proc - GetDropdownOption {dropdownElement}");
            if (SeleniumUtil.EnterText(dropdownElement, selecting))
            {
                DebugOutput.Log($"Have sent {selecting} to dropdown");
                return true;
            }
            DebugOutput.Log($"Unable to send {selecting} to the dropdown element");
            return false;
        }

        private IWebElement? GetDropdownElement(string dropdownName, int timeout = 0)
        {
            DebugOutput.Log($"Proc - Getdropdown {dropdownName}");
            var dropdownLocator = ElementName.GetElementLocator(dropdownName, CurrentPage, "dropdown");
            if (dropdownLocator == null)
            {
                DebugOutput.Log($"No record on page {CurrentPage} for locator {dropdownName}");
                return null;
            }
            var namedDropdownElement = SeleniumUtil.GetElement(dropdownLocator, timeout);
            if (namedDropdownElement == null)
            {
                DebugOutput.Log($"Can not find element by {dropdownLocator} on page {CurrentPage}");
                return null;
            }
            return namedDropdownElement;
        }

    }
}
