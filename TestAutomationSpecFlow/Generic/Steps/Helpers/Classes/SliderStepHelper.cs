﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class SliderStepHelper : StepHelper, ISliderStepHelper
    {
        private readonly ITargetForms targetForms;
        public SliderStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool IsDisplayed(string sliderName)
        {
            DebugOutput.Log($"proc - IsDisplayed {sliderName}");
            var sliderElement = GetSliderElement(sliderName);
            if (sliderElement == null) return false;
            DebugOutput.Log($"We have the Slider Element");
            return sliderElement.Displayed;
        }

        public bool SetSliderValue(string sliderName, string value)
        {
            DebugOutput.Log($"proc - SetSliderValue {sliderName}");
            var sliderElement = GetSliderElement(sliderName);
            if (sliderElement == null) return false;
            if (Int32.TryParse(value, out int intValue))
            {
                return SeleniumUtil.MoveSliderElement(sliderElement, intValue);
            }
            return false;
        }

        public bool EnterSliderValue(string sliderName, string value)
        {
            DebugOutput.Log($"proc - EnterSliderValue {sliderName}");
            var sliderElement = GetSliderElement(sliderName);
            if (sliderElement == null) return false;
            return SeleniumUtil.EnterText(sliderElement, value);
        }

        ////Private Below

        private IWebElement? GetSliderElement(string sliderName)
        {
            var sliderLocator = ElementName.GetElementLocator(sliderName, CurrentPage, "slider");
            if (sliderLocator == null) return null;
            var namedButtonElement = SeleniumUtil.GetElement(sliderLocator);
            if (namedButtonElement == null)
            {
                DebugOutput.Log($"Can not find element by {sliderLocator} on page {CurrentPage}");
                return null;
            }
            return namedButtonElement;
        }
    }
}
