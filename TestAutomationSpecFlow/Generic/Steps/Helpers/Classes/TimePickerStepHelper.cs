﻿using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class TimePickerStepHelper : StepHelper, ITimePickerStepHelper
    {
        private readonly ITargetForms targetForms;
        public TimePickerStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }



        private readonly By[] TimePickerButtonOpen = LocatorValues.locatorParser(TargetConfiguration.Configuration.TimePickerButtonOpen);
        private readonly By[] TimePickerText = LocatorValues.locatorParser(TargetConfiguration.Configuration.TimePickerText);


        public bool IsDisplayed(string timePickerName, int timeout = 0)
        {
            DebugOutput.Log($"IsDisplayed {timePickerName}");
            var datePickerElement = GetTimePickerElement(timePickerName, timeout);
            if (datePickerElement == null) return false;
            return datePickerElement.Displayed;
        }

        public bool SetTimeValue(string timePickerName, string time, int timeOut = 0)
        {
            DebugOutput.Log($"SetTimeValue {timePickerName} {time}");
            var currentValue = GetCurrentValue(timePickerName, timeOut);
            if (time == currentValue)
            {
                DebugOutput.Log($"These already match ! {time}");
                return true;
            }
            DebugOutput.Log($"They do not CURRENTLY match {time} Time wanted but got {currentValue}");
            return EnterValueInTimePicker(timePickerName, time, timeOut);
        }


        public bool EnterValueInTimePicker(string timePickerName, string time, int timeOut)
        {
            DebugOutput.Log($"EnterValueInDatePicker {timePickerName} {time}");
            var timePickerElement = GetTimePickerElement(timePickerName, timeOut);
            if (timePickerElement == null) return false;
            //Click On Text
            if (!SeleniumUtil.Click(timePickerElement)) return false;
            // We open the datepicker, we need the text element below it!
            int counter = 0;
            int numberOfDatePickerText = TimePickerText.Count();
            while (counter < numberOfDatePickerText)
            {
                DebugOutput.Log($"Get the text element below");
                var timePickerTextElements = SeleniumUtil.GetElementsUnder(timePickerElement, TimePickerText[counter], 1);
                if (timePickerTextElements.Count == 1)
                {
                    if (!SeleniumUtil.ClearThenEnterText(timePickerTextElements[0], time)) return false;
                    var newValue = GetCurrentValue(timePickerName, timeOut);
                    DebugOutput.Log($"Comparing TIMEPICKER {newValue} with {time}");
                    if (newValue == time) return true;
                    DebugOutput.Log($"Failed to be the same {newValue} is current value");

                }
                counter++;
            }
            DebugOutput.Log($"Cycled through - failed!");
            return false;
        }

        public string? GetCurrentValue(string timePickerName, int timeOut)
        {
            DebugOutput.Log($"GetCurrentValue {timePickerName}");
            var datePickerElement = GetTimePickerElement(timePickerName, timeOut);
            if (datePickerElement == null) return "";
            //We have the element, now we want its value.  It is contained in an element below the element
            int counter = 0;
            int numberOfTexts = TimePickerText.Count();
            while (counter < numberOfTexts)
            {
                var timePickerTextElement = SeleniumUtil.GetElementsUnder(datePickerElement, TimePickerText[counter], 1);
                var timePickerTextValue = SeleniumUtil.GetElementText(timePickerTextElement[counter]);
                if (timePickerTextValue != null) return timePickerTextValue;
                counter++;
            }
            DebugOutput.Log($"Failed to find any text");
            return "";

        }

        private IWebElement? GetTimePickerElement(string timePickerName, int timeOut)
        {
            DebugOutput.Log($"GetTimePickerElement {timePickerName}");

            var timePickerLocator = ElementName.GetElementLocator(timePickerName, CurrentPage, "timepicker");
            if (timePickerLocator == null) return null;
            var timePickerElement = SeleniumUtil.GetElement(timePickerLocator, timeOut);
            if (timePickerElement == null)
            {
                DebugOutput.Log($"Can not find element by {timePickerElement} on page {CurrentPage}");
                return null;
            }
            return timePickerElement;
        }

    }
}
