﻿using Core;
using Core.FileIO;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class OSStepHelper : StepHelper, IOSStepHelper
    {
        private readonly ITargetForms targetForms;
        public OSStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }


    }
}
