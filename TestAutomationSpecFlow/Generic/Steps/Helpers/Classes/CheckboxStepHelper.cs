﻿using Core;
using Core.Logging;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Generic.Steps.Helpers.Classes
{
    public class CheckboxStepHelper : StepHelper, ICheckboxStepHelper
    {
        private readonly ITargetForms targetForms;
        public CheckboxStepHelper(FeatureContext featureContext, ITargetForms targetForms) : base(featureContext)
        {
            this.targetForms = targetForms;
        }

        public bool IsDisplayed(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"IsDisplayed {checkboxName}");
            var checkboxElement = GetCheckboxElement(checkboxName, timeout);
            if (checkboxElement == null) return false;
            if (checkboxElement.Displayed) return true;
            DebugOutput.Log($"Wee issue seeing the check box....  Some people have the input of a checkbox hidden below another element!");
            var parentElement = SeleniumUtil.GetElementParent(checkboxElement);
            if (parentElement == null) return false;
            if (parentElement.Displayed) return true;
            DebugOutput.Log($"Even the parent is hidden? Guess you need more!");
            return false;

        }

        public bool IsSelected(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"IsSelected {checkboxName}");
            var checkboxElement = GetCheckboxElement(checkboxName, timeout);
            if (checkboxElement == null) return false;
            return checkboxElement.Selected;
        }

        public bool Select(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"Select {checkboxName}");
            var checkboxElement = GetCheckboxElement(checkboxName, timeout);
            if (checkboxElement == null) return false;
            if (SeleniumUtil.Click(checkboxElement)) return true;
            DebugOutput.Log($"Wee issue clicking on the check box....  Some people have the input of a checkbox hidden below another element!");
            var parentElement = SeleniumUtil.GetElementParent(checkboxElement);
            if (parentElement == null) return false;
            DebugOutput.Log($"WE HAVE the parent!");
            By labelElementLocator = By.ClassName("form-check-label");
            var labelElement = SeleniumUtil.GetElementUnderElement(parentElement, labelElementLocator, 1);
            if (labelElement == null) return false;
            if (SeleniumUtil.Click(labelElement)) return true;
            DebugOutput.Log($"Even the parents LABEL is hiddent? Guess you need more!");
            return false;
        }

        public bool Selected(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"Selected {checkboxName}");
            if (IsSelected(checkboxName)) return true;
            //Not selected, needs to be selected
            return Select(checkboxName, timeout);
        }

        public bool SelectedNot(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"SelectedNot {checkboxName}");
            if (!IsSelected(checkboxName)) return true;
            //Is selected, needs to be NOT selected, so we click on it!
            return Select(checkboxName, timeout);
        }

        private IWebElement? GetCheckboxElement(string checkboxName, int timeout = 0)
        {
            DebugOutput.Log($"GetCheckBoxElement {checkboxName}");
            var checkboxLocator = ElementName.GetElementLocator(checkboxName, CurrentPage, "checkbox");
            if (checkboxLocator == null) return GetCheckboxElementNotElement(checkboxName);
            var namedCheckboxElement = SeleniumUtil.GetElement(checkboxLocator, timeout);
            if (namedCheckboxElement == null)
            {
                DebugOutput.Log($"Can not find element by {checkboxLocator} on page {CurrentPage}");
                namedCheckboxElement = GetCheckboxElementNotElement(checkboxName);
                if (namedCheckboxElement == null ) 
                {
                    return GetCheckboxElementNotElementNotText(checkboxName);

                }
            }
            DebugOutput.Log($"Returning the Element");
            return namedCheckboxElement;
        }

        private IWebElement? GetCheckboxElementNotElement(string checkboxName)
        {
            DebugOutput.Log($"GetCheckboxElementNotElement {checkboxName}");
            var element = SeleniumUtil.GetAllInputElementsByText(checkboxName);
            if (element == null) return GetCheckboxElementNotElementNotText(checkboxName);
            return element;
        }

        private IWebElement? GetCheckboxElementNotElementNotText(string checkboxName)
        {
            DebugOutput.Log($"GetCheckboxElementNotElementNotText {checkboxName}");
            return SeleniumUtil.GetInputElementByParentText(checkboxName);
        }

    }

}
