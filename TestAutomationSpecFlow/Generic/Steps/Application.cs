﻿using Core;
using Core.Configuration;
using Core.Logging;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generic.Steps
{
    public static class Application
    {

        public static bool OpenApplication(string applicationExePath)
        {
            DebugOutput.Log($"proc - OpenApplication");
            var driverOptions = new AppiumOptions();
            driverOptions.AddAdditionalCapability(MobileCapabilityType.App, $"{applicationExePath}");
            var newDriver = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723/"), driverOptions);
            var allWindowHandles = newDriver.WindowHandles;
            newDriver.SwitchTo().Window(allWindowHandles[0]);
            SeleniumUtil.winDriver = newDriver;
            if (TargetConfiguration.Configuration == null) return false;
            TargetConfiguration.Configuration.ApplicationType = "windows";
            return true;
        }

        public static void CloseWinDriver()
        {
            DebugOutput.Log($"proc - CloseWinDriver");
            if (SeleniumUtil.winDriver == null) return;
            SeleniumUtil.winDriver.Close();
            SeleniumUtil.winDriver.Quit();
            SeleniumUtil.winDriver = null;
        }
    }
}
