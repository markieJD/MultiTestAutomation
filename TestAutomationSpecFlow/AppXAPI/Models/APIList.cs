﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppXAPI.Models
{
    public class APIList
    {
        /// <summary>
        /// Response from a GET
        /// </summary>
        public static HttpResponseMessage? fullResponse { get; set; }
        
        /// Payloads
        public static string payloadLocation { get; set; } = @"./AppXAPI/Models/Payloads/";


        public static List<CatFacts>? catFacts { get; set; }
        public static List<TodDoFromAtlas>? todoActiveList { get; set; }


        ///EID Use
        public static string EIDURL { get; set; } = @"https://eidapi01.bssdev.local:4431/POLE.API.DEV/api/v2";
        /// USERS
        public static string domainUserName = @"BSSDEV\cmtest";
        public static string domainUserPassword = "313vat10n";
        /// Neo
            /// Links
            public static string NeoLinkURLEndPoint { get; set; } = @"POLE/";
            /// Search
            public static string SearchEndPoint { get; set; }  = "Search";
            //MasterChildren
            public static string MasterChildren { get; set; } = @"/MasterChildren?EID_SourceSystem=";
            public static string MasterChildrenName { get; set; } = "MasterChildren";
            public static string UsersEndPoint { get; set; } = @"Users/";
            public static string CodeGuid { get; set; } = "80020dce-b843-4456-a00d-dc5f94c8c858";
        public static string Source { get; set; } = "Atlas CM";
        public static string Source1 { get; set; } = "Atlas CM-PND";
        public static string Source2 { get; set; } = "Atlas KB-PND";
        public static string Source3 { get; set; } = "Atlas KB-PND2";
        public static string Master { get; set; } = "Master-PND";
        public static string SecondaryUser { get; set; } = "soitest";
        public static string ModelVersion = "1";


        // SOI Tool
        public static string soiLocation { get; set; } = @"D:\SOITOOL\Live\";
        public static string soiExecutor { get; set; } = "EIDSOITOOL";
        public static string soiJsonFileLocation { get; set; } = @"Data\PND";

        // XML Tool
        public static string PNDToolLocation = @"D:\PNDTOOL\LIVE\ConsoleProject\";
        public static string PNDExecutor { get; set; } = "EIDPNDConsoleApp.bat";
        public static string PNDXMLFileLocation = @"D:\XMLOut\";

        public static string PNDXMLExportFileLocation = @"D:\XMLOut\";



    }


}
