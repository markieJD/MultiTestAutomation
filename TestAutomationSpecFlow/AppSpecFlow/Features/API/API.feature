﻿@all @Pipeline1 @pipeline2 @API0001
Feature: API-API Test
	A Quick walk through API Get Push etc
	

	
#Scenario Outline: API0001-0025 To Do
#	When I Call GET API "todo from Atlas"
#	Then API Status Code 200
#	Then Output All API Return
#	Then API "todo from Atlas" Is Correct Format
#
Scenario Outline: API0001-0035 Cat Facts
	When I Call GET API "cat facts"
	Then API Status Code 200
	Then Output All API Return
	Then API "cat facts" Is Correct Format

#Scenario Outline: API0001-0045 Delay
#	When I Call GET API "delay"
#	Then API Status Code 200
#	Then API "Delay" Is Correct Format

#Scenario Outline: API0001-0055 User Not Found
#	When I Call GET API "user not found"
#	Then API Status Code 404
#
#Scenario Outline: API0001-0065 Jira Read
#	Given Approved
#	When I Call GET API "Jira Licence"
#	Then API Status Code 200
#	Then Output All API Return

Scenario Outline: API0001-0075 GitLab Read
	Given Approved
	When I Call GET API "GitLab"
	Then API Status Code 200
	Then Output All API Return



Scenario Outline: API0001-9930 Close Browser
	Given Browser Is Closed