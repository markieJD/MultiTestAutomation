﻿@all @Pipeline1 @pipeline2 @NOTE0011
Feature: NOTE0011-Notepad Test
	Quick walk through Notepad	
	dotnet test --filter:"TestCategory=NOTE0011" --logger "trx;logfilename=NOTE0011.trx"

	
Scenario Outline: NOTE0011-0010 Confirm Page
	Given App "C:\Windows\System32\notepad.exe" Is Open
	Given Window "Notepad" Is Displayed
	When I Enter "HELLO THERE" In Document
	When I Enter " HELLO THERE Too" In Document
	
Scenario Outline: NOTE0011-9999 Close
	When I Close Window "Notepad"