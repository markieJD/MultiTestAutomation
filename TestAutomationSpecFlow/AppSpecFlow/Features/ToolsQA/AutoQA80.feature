﻿@all @Pipeline1 @pipeline2 @QA000080
Feature: QA000080-Slider
	Test Slider
	dotnet test --filter:"TestCategory=QA000080" --logger "trx;logfilename=QA000080.trx"


Scenario Outline: QA000080-0000 START
	Given Browser "chrome" Is Open
	Given URL is "https://demoqa.com/elements"
	Given Page "ToolsQA" Is Displayed
	Given URL is "https://demoqa.com/slider"
	Then Page "Slider" Is Displayed
	Then Slider "Slider" Is Displayed
	
Scenario Outline: QA000080-0010 Set Slider
	When I Set Slider "Slider" To "70"
	Given Button "HellO" Is Displayed
	
Scenario Outline: QA000080-0020 Send Text
	When I Enter "32" In Slider "Slider"