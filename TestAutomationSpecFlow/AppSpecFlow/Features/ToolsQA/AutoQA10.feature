﻿@all @Pipeline1 @pipeline2 @QA000010
Feature: QA000010-QATool All Test
	Test Everything
	dotnet test --filter:"TestCategory=QA000010" --logger "trx;logfilename=QA000010.trx"
	


Scenario Outline: QA000010-0000 START
	# Given Browser "chrome" Is Open
	Given Browser Is Open
	Given URL is "https://demoqa.com/elements"
	Then Page "ToolsQA" Is Displayed
	Then Accordion "accordian" Is Displayed

	When I Click Group "Elements" In Accordion "accordian"
	Then Wait "1" Seconds
	Then Group "Elements" In Accordion "accordian" Is Not Expanded
	Then Wait "1" Seconds
	
	When I Click Group "Elements" In Accordion "accordian"
	Then Wait "2" Seconds
	
	Then Group "Elements" In Accordion "accordian" Is Expanded
	Then Button "Text Box" In Accordion "accordian" Displayed
	
	When I Click Button "Text Box" In Accordion "accordian"
	Then TextBox "Full Name" Is Displayed
	
	When I Enter "Mark Duffy" In TextBox "Full Name"
	Then Wait "2" Seconds
	Then TextBox "Full Name" Is Equal To "Mark Duffy"
	
	When I Enter "mark.duffy@thisemail.com" Then Press "Enter" In TextBox "email"
	Then TextBox "EMAIL" Is Equal To "mark.duffy@thisemail.com"
	Then TextBox "Email" Is Not Equal To "Rubbish email"
	
	When I Clear Then Enter "joe.bloggs@email.com" In TextBox "email"
	Then TextBox "EMAIL" Is Equal To "joe.bloggs@email.com"
	
	When I Enter "END" Then Press "Enter" In TextBox "email"
	Then TextBox "EMAIL" Is Equal To "joe.bloggs@email.comEND"
	
	When I Click Button "Check Box" In Accordion "accordian"
	Then Tree "CheckBox" Is Displayed
	
	When I Click Button "Radio Button" In Accordion "accordian"
	Then RadioButton "Yes" Is Displayed
	Then RadioButton "Impressive" Is Displayed
	Then RadioButton "No" Is Displayed
	
	Then RadioButton "No" Is Read Only
	Then RadioButton "Yes" Is Enabled
	Then RadioButton "Yes" Is Not Selected
	
	When I Click On RadioButton "Impressive"
	Then RadioButton "Impressive" Is Selected
	Then RadioButton "Yes" Is Not Selected	
	
	When I Click Button "Buttons" In Accordion "accordian"
	Then Button "Double Click Me" Is Displayed
	Then Button "Right Click Me" Is Displayed
	Then Button "Click Me" Is Displayed
	
	When I Click On Button "Click Me"
	Then Page Displays Message "You have done a dynamic click"
	
	When I Double Click On Button "Double Click Me"
	Then Page Displays Message "You have done a double click"
	
	When I Right Click On Button "Right Click Me"
	Then Page Displays Message "You have done a dynamic click"
