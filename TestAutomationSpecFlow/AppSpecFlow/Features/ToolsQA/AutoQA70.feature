﻿@all @Pipeline1 @pipeline2 @QA000070
Feature: QA000070-Select Menu
	Test Buttons
	dotnet test --filter:"TestCategory=QA000070" --logger "trx;logfilename=QA000070.trx"


Scenario Outline: QA000070-0000 START
	Given Browser "chrome" Is Open
	Given URL is "https://demoqa.com/elements"
	Given Page "ToolsQA" Is Displayed
	Given URL is "https://demoqa.com/date-picker"
	Then Page "Date Picker" Is Displayed


Scenario Outline: QA000070-0010 First Date Picker Enter
	When I Clear Then Enter "" In TextBox "select date"
	When I Enter "16/01/2022" Then Press "Enter" In TextBox "select date"
	Then TextBox "select date" Is Equal To "01/01/2022"

Scenario Outline: QA000070-0020 Open Date Picker
	When I Click On TextBox "Select Date"
