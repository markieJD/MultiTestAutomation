﻿@all @Pipeline1 @pipeline2 @QA000060
Feature: QA000060-Select Menu
	Test Buttons
	dotnet test --filter:"TestCategory=QA000060" --logger "trx;logfilename=QA000060.trx"


Scenario Outline: QA000060-0000 START
	Given Browser "chrome" Is Open
	Given URL is "https://demoqa.com/elements"
	Given Page "ToolsQA" Is Displayed
	Given URL is "https://demoqa.com/select-menu"
	Then Page "Select Menu" Is Displayed
	Then Dropdown "Select Value" Is Displayed
	Then Dropdown "Select One" Is Displayed
	Then Dropdown "Old Style Select Menu" Is Displayed
	Then Dropdown "Multiselect drop down" Is Displayed
	Then Dropdown "Standard multi select" Is Displayed
	
Scenario Outline: QA000060-0010 Select Value
	When I Select "A root option" From Dropdown "Select Value"

Scenario Outline: QA000060-0020 Confirm Value
	Then Dropdown "Select Value" Is Equal To "A root option"
	
Scenario Outline: QA000060-0030 Select One
	When I Select "Prof." From Dropdown "Select One"

Scenario Outline: QA000060-0040 Confirm Value
	Then Dropdown "Select One" Is Equal To "Prof."
	
Scenario Outline: QA000060-0050 Old Style Select Menu
	When I Select "Yellow" From Dropdown "Old Style Select Menu"

Scenario Outline: QA000060-0060 Confirm Value
	Then Dropdown "Old Style Select Menu" Is Equal To "Yellow"	
	
Scenario Outline: QA000060-0070 Multiselect drop down Blue
	When I Select "Green" From Dropdown "Multiselect drop down"
	
Scenario Outline: QA000060-0075 Multiselect drop down Black
	When I Select "Blue" From Dropdown "Multiselect drop down"
	
Scenario Outline: QA000060-0090 Multiselect Cars
	When I Select "Volvo" From Dropdown "Standard multi select"
	When I Select "Audi" From Dropdown "Standard multi select"
	Then Page Image Is Captured