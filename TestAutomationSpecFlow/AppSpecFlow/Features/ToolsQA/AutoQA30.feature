﻿@all @Pipeline1 @pipeline2 @QA000030
Feature: QA000030-CheckBoxTests
	Testing Checkboxes
	dotnet test --filter:"TestCategory=QA000030" --logger "trx;logfilename=QA000030.trx"


Scenario Outline: QA000030-0000 START
	Given Browser "chrome" Is Open
	Given URL is "https://demoqa.com/elements"
	Given Page "ToolsQA" Is Displayed
	When I Click Button "Check Box" In Accordion "accordian"
	Then Tree "CheckBox" Is Displayed
	