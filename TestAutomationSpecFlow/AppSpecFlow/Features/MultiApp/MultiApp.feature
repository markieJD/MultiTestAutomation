﻿@all @Pipeline1 @pipeline2 @MULTI001
Feature: MULTI001-Mutiple Applications
	Controlling Multi applications using drivers.	
	dotnet test --filter:"TestCategory=MULTI001" --logger "trx;logfilename=MULTI001.trx"

	
Scenario Outline: MULTI001-0010 Open Windows Application
	Given App "c:\windows\system32\notepad.exe" Is Open
	
Scenario Outline: MULTI001-0020 Confirm Windows Application
	Given Window "Notepad" Is Displayed
	
Scenario Outline: MULTI001-0025 Close Windows Application
	Given App Is Closed
	
Scenario Outline: MULTI001-0030 Reopen Windows Application
	Given App "c:\windows\system32\notepad.exe" Is Open
	
Scenario Outline: MULTI001-0040 Reconfirm Windows Application
	Given Window "Notepad" Is Displayed
	
Scenario Outline: MULTI001-0050 Close Windows Application
	Given App Is Closed
	
Scenario Outline: MULTI001-0110 Open Web Application
	Given Browser "Chrome" Is Open
	
Scenario Outline: MULTI001-0120 Open Web Application And Navigate
	Given Browser "Chrome" Is Open
	Given URL is "http://news.bbc.co.uk"
	
Scenario Outline: MULTI001-0130 Close Web Application
	Given Browser Is Closed	
	
Scenario Outline: MULTI001-0300 Browser As In Default
	Given Browser Is Open
	
Scenario Outline: MULTI001-0310 Browser As In Default Closed
	Given Browser Is Closed
	