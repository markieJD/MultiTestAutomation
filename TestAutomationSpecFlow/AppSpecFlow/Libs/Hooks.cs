﻿using Core.Configuration;
using BoDi;
using Core;
using Core.Logging;
using System.Reflection;
using Generic.Steps.Helpers.Interfaces;
using Generic.Steps.Helpers.Classes;
using AppSpecFlow.AppSteps;
using OpenQA.Selenium;
using AppSpecFlow.AppSteps.Vars;
using Generic.Steps;

namespace AppSpecFlow.Libs
{
    [Binding]
    public class Hooks
    {

        [BeforeTestRun]
        public static void TestSetup()
        {
            var assembly = Assembly.Load("AppTargets");
            TargetForms.Instance.PopulateList(assembly);
            System.Environment.SetEnvironmentVariable("ENVIRONMENT", "development");  //normally this is set on environment - but requires a reboot - so for testing hard coded!
            TargetConfiguration.ReadJson();
            AppVars.ReadJson();
            Browser.ReadJson();
            SetDebug(TargetConfiguration.Configuration.Debug);
        }


        [AfterTestRun]
        public static void TestCleanUp()
        {

        }

        [BeforeFeature]
        public static void FeatureSetup(FeatureContext featureContext)
        {
            SetEpoch();
        }

        [AfterFeature]
        public static void FeatureCloseDown(FeatureContext featureContext)
        {
            DebugOutput.Log($"CLOSE FEATURE");
            if (SeleniumUtil.webDriver != null)
            {
                CloseSSA();
            }
            if (SeleniumUtil.webDriver != null)
            {
                SeleniumUtil.webDriver.Dispose();
                SeleniumUtil.webDriver = null;
            }
            if (SeleniumUtil.winDriver != null)
            {
                SeleniumUtil.winDriver.Dispose();
                SeleniumUtil.winDriver = null;
            }
            var feature = featureContext.FeatureInfo.Title;
            DebugOutput.Log($"END OF {feature}");
        }

        [BeforeScenario]
        public void ScenarioSetup(ScenarioContext scenarioContext)
        {
            RegisterTypes(scenarioContext);
            var scenario = scenarioContext.ScenarioInfo.Title;
            DebugOutput.Log($"Scenario - {scenario}");
        }

        [AfterScenario]
        public void ScenarioCleanUp(ScenarioContext scenarioContext)
        {

        }

        [BeforeStep]
        public void StepSetup(ScenarioContext scenarioContext)
        {
            var stepContext = scenarioContext.StepContext;
            DebugOutput.Log("Step - " + stepContext.StepInfo.StepDefinitionType + " " + stepContext.StepInfo.Text);
        }

        [AfterStep]
        public void StepCleanUp(ScenarioContext scenarioContext)
        {

        }

        /// <summary>
        /// Set the epoch value for each feature file so unique text is available
        /// </summary>
        private static void SetEpoch()
        {
            var epochNumber = DateTime.UtcNow.Ticks / 10000000 - 63082281600;
            var epoch = epochNumber.ToString();
            EPOCHControl.Epoch = epoch;
            DebugOutput.Log($"This feature test unique id is equal to {epoch}");
        }

        private static void SetDebug(int debugLevelFromFile = 5)
        {
            DebugOutput.debugLevel = debugLevelFromFile;
            DebugOutput.Log($"Debug Level set to {debugLevelFromFile}");    
        }

        private static void RegisterTypes(ScenarioContext scenarioContext)
        {
            var container = (IObjectContainer)scenarioContext.GetBindingInstance(typeof(IObjectContainer));
            container.RegisterTypeAs<StepHelpers, IStepHelpers>();
            container.RegisterInstanceAs<ITargetForms>(TargetForms.Instance);
        }

        /// <summary>
        /// PROJECT SPECIFIC HOOKS
        /// </summary>
        private static void CloseSSA()
        {
            DebugOutput.Log($"CLOSING SSA!");
            var driver = SeleniumUtil.winDriver;
            if (driver == null) return;
            var closeElement = driver.FindElement(By.Id("Close"));
            closeElement.Click();
            Thread.Sleep(5000);
            var yesElement = driver.FindElement(By.Name("Yes"));
            if (yesElement != null)
            {
                yesElement.Click();
            }
        }

    }
}