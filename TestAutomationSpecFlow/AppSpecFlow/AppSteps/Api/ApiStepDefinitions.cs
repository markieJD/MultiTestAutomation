﻿
using AppXAPI.Models;
using Core;
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Generic.Steps;

namespace AppSpecFlow.AppSteps
{
    [Binding]
    public sealed class ApiStepDefinitions
    {
        Dictionary<string, string> apiUrls = new Dictionary<string, string>()
        {
            {"todo from atlas", "https://jsonplaceholder.typicode.com/todos" },
            {"cat facts", "/fact" },
            {"delay" , "https://reqres.in/api/users?delay=3" },
            {"user not found", "https://reqres.in/api/users/23" },

            {"jira licence", "https://triggersbroom.org/jira/rest/raven/2.0/api/xraylicense" },

            {"gitlab", "https://gitlab.com/api/v4/projects" }
        };

        [Given(@"Approved")]
        public void GivenApproved()
        {
            var proc = $"Given Approved";

        }




        [When(@"I Call GET API todo From Atlas")]
        public async Task WhenICallGETAPITodoFromAtlas()
        {
            var proc = $"When I Call GET API todo From Atlas";
            await APIUtil.Get("https://jsonplaceholder.typicode.com/todos", "todo");
        }

        [Then(@"API Status Code (.*)")]
        public void ThenAPIStatusCode(int statusCode)
        {
            var proc = $"Then Status Code {statusCode}";
            try
            {
                if (APIList.fullResponse == null) return;
                var code = APIList.fullResponse.StatusCode;
                var lastStatusCode = (int)code;
                if (lastStatusCode != statusCode)
                {
                    DebugOutput.Log($"wanted {statusCode} got {lastStatusCode}");
                    CombinedSteps.Failure(proc);
                    return;
                }
                DebugOutput.Log($"Status Code was {statusCode} as expected");
            }
            catch
            {
                DebugOutput.Log($"We do not have a response status code");
                CombinedSteps.Failure(proc);
            }
        }

        [Then(@"API ""([^""]*)"" Is Correct Format")]
        public void ThenAPIIsCorrectFormat(string apiName)
        {
            apiName = apiName.ToLower();    
            var proc = $"Then API {apiName} Is Correct Format";
            if (APIList.fullResponse == null)
            {
                DebugOutput.Log($"Failed to get response! Have you done your API CALL yet?");
                CombinedSteps.Failure(proc);
                return;
            }
            var jSonText = JsonValues.ReadAPIJsonFile(apiName);
            if (jSonText == null) return;
            switch (apiName)
            {
                default:
                    {
                        CombinedSteps.Failure(proc);
                        return;
                    }
                case "todo from atlas":
                    {
                        var items = ToDoFromAtlasAPP.MakeModel(jSonText);
                        if (items == null) return;
                        if (items.Count < 1)
                        {
                            CombinedSteps.Failure(proc);
                        }
                        return;
                    }
                case "cat facts":
                    {
                        var items = CatFactsAPP.MakeModel(jSonText);
                        if (items == null) return ;
                        if (items.Count < 1)
                        {
                            CombinedSteps.Failure(proc);
                        }
                        return;
                    }
                case "delay":
                    {
                        var items = DelayAPP.MakeModel(jSonText);
                        if (items == null) return;
                        if (items.Count < 1)
                        {
                            CombinedSteps.Failure(proc);
                        }
                        return;
                    }
            }
        }



    }
}
