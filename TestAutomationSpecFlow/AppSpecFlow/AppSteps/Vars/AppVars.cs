﻿using Core.FileIO;
using Core.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSpecFlow.AppSteps.Vars
{
    public class AppVars
    {

        private const string EnvironmentVariable = "ENVIRONMENT";

        public static AppVarsData Instance { get; private set; } = new AppVarsData();

        public class AppVarsData
        {
            public string SSAVersion { get; set; } = String.Empty;
            public string SSALocation { get; set; } = String.Empty;
            public int SSAWidth { get; set; } = 0;
            public int SSAHeight { get; set; } = 0;

        }

        public static AppVarsData? ReadJson()
        {
            DebugOutput.Log($"Proc - ReadJson for AppVars");
            var fileName = $"vars.{Environment}.json";
            var directory = ".\\AppSpecFlow\\AppSteps\\Vars\\Json\\";
            var fullFileName = directory + fileName;
            if (!FileChecker.FileCheck(fullFileName))
            {
                DebugOutput.Log($"Unable to find the file {fullFileName}");
                return null;
            }
            var jsonText = File.ReadAllText(fullFileName);
            try
            {
                var obj = JsonConvert.DeserializeObject<AppVarsData>(jsonText);
                if (obj == null) return null;
                Instance = obj;
                DebugOutput.Log($">>>> {obj.SSAVersion}  ... {Instance.SSAVersion}");
                return Instance;
            }
            catch
            {
                DebugOutput.Log($"We out ere");
                return null;
            }
        }


        /// <summary>
        ///     Set up an environment variable called "ENVIRONMENT", and set it to type of environment 
        ///     development, beta, live etc.
        ///     Then you need a targetSettings.ENVIRONMENT.json file in the Resources folder of the AppTargets project
        ///     Different environments need different configuration.  this is how that is controled.
        ///     If there is no ENVIRONMENT environment variable it will use development as default.
        /// </summary>
        private static string Environment =>
            System.Environment.GetEnvironmentVariable(EnvironmentVariable) ?? "development";
    }
}
