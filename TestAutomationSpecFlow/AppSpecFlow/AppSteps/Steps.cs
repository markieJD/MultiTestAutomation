
using AppSpecFlow.AppSteps.DataFiles;
using AppSpecFlow.AppSteps.Vars;
using AppXAPI.APIApps;
using AppXAPI.Models;
using Core;
using Core.Configuration;
using Core.FileIO;
using Core.Logging;
using Core.Transformations;
using Generic.Elements.Steps.Button;
using Generic.Elements.Steps.Page;
using Generic.Elements.Steps.Textbox;
using Generic.Steps;
using Generic.Steps.Helpers.Interfaces;
using OpenQA.Selenium;

namespace AppSpecFlow.AppSteps
{
    [Binding]
    public class Steps : StepsBase
    {
        public Steps(IStepHelpers helpers,
            GivenSteps givenSteps,
            ThenSteps thenSteps,

            WhenButtonSteps whenButtonSteps,
            GivenPageSteps givenPageSteps,
            WhenPageSteps whenPageSteps,
            ThenPageSteps thenPageSteps,
            WhenTextBoxSteps whenTextBoxSteps

            ) : base(helpers)
        {
            GivenSteps = givenSteps;
            ThenSteps = thenSteps;

            WhenButtonSteps = whenButtonSteps;
            GivenPageSteps = givenPageSteps;
            WhenPageSteps = whenPageSteps;
            ThenPageSteps = thenPageSteps;
            WhenTextBoxSteps = whenTextBoxSteps;

        }

        private GivenSteps GivenSteps { get; }
        private ThenSteps ThenSteps { get; }

        private WhenButtonSteps WhenButtonSteps { get; }
        private GivenPageSteps GivenPageSteps { get; }
        private WhenPageSteps WhenPageSteps { get; }
        private ThenPageSteps ThenPageSteps { get; }
        private WhenTextBoxSteps WhenTextBoxSteps { get; }

        
        [Given(@"STEPS HERE")]
        public void GivenSTEPSHERE()
        {
            DebugOutput.Log("NOTHING HERE");
        }

        [Given(@"All Elements Counted")]
        public void GivenAllElementsCounted()
        {
            string proc = $"Given All Elements Counted";
            if (CombinedSteps.OuputProc(proc))
            {
                var numberOfPageElements = PageObjectValues.HowManyPageElementsReturned();
                if (numberOfPageElements > 0)
                {
                    DebugOutput.Log($"We have {numberOfPageElements}");
                    return;
                }
                DebugOutput.Log($"We have zero elements!");
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"Elements Counted By Type ""([^""]*)""")]
        public void GivenElementsCountedByType(string type)
        {
            string proc = $"Given Elements Counted By Type {type}";
            if (CombinedSteps.OuputProc(proc))
            {
                var elements = PageObjectValues.GetAllPageElementsOfType(type);
                DebugOutput.Log($" We have {elements.Count}");
                return;
            }
        }

        [Given(@"PreCondition Data ""([^""]*)"" Exists")]
        public void GivenPreConditionDataExists(string nameOfPreconditionDataDirectory)
        {
            var proc = $"Given PreConditionData {nameOfPreconditionDataDirectory} Exists";
            if (CombinedSteps.OuputProc(proc))
            {
                if (PreConditionData.PreConditionDataExists(nameOfPreconditionDataDirectory))
                {
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }


        [When(@"I Create PreCondition Data (.*)")]
        public static void WhenICreatePreConditionData(int preConditionNumber)
        {
            var proc = $"When I Create PreConditionData {preConditionNumber}";
            if (CombinedSteps.OuputProc(proc))
            {
                string preConNumberAsString = preConditionNumber.ToString();
                string newNameOfPreconditionDataDirectory = PreConditionData.dataDir + preConNumberAsString;
                DebugOutput.Log($"Creating {newNameOfPreconditionDataDirectory}");
                if (!PreConditionData.PreConditionDataExists(newNameOfPreconditionDataDirectory))
                {
                    if (PreConditionData.CreatePreConditionData(newNameOfPreconditionDataDirectory, preConditionNumber))
                    {
                        return;
                    }
                }
                else
                {
                    DebugOutput.Log($"The directory {newNameOfPreconditionDataDirectory} already exists!  We can not create new precon data on top of already created data!");
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        [Given(@"PreCondition Data ""([^""]*)"" Is Used")]
        [When(@"I Use PreCondition Data ""([^""]*)""")]
        public static void WhenIUsePreConditionData(string nameOfPreconditionDataDirectory)
        {
            var proc = $"When I Use PreConditionData {nameOfPreconditionDataDirectory}";
            if (CombinedSteps.OuputProc(proc))
            {
                var localDataDir = PreConditionData.fulldatDir;
                if (FileChecker.OSDeleteDirectoryIfExists(localDataDir))
                {
                    if (PreConditionData.PreConditionDataExists(nameOfPreconditionDataDirectory))
                    {
                        if (PreConditionData.UsePreConditionData(nameOfPreconditionDataDirectory))
                        {
                            return;
                        }
                    }
                    else
                    {
                        DebugOutput.Log($"PreCondata does not exists! {localDataDir}");
                        CombinedSteps.Failure(proc);
                        return;

                    }
                }
                else
                {
                    DebugOutput.Log($"Failed deleting {localDataDir}");
                    CombinedSteps.Failure(proc);
                    return;
                }
                CombinedSteps.Failure(proc);
                return;
            }
        }

        private void Sync()
        {
            WhenButtonSteps.WhenIClickOnButton("get data from atlas");
            ThenSteps.ThenWaitSeconds("5");
            ThenPageSteps.ThenMessageIsNotDisplayed("You MUST download master data from Atlas before using this application");
        }




    }
}