﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Logging
{
    public class EPOCHControl
    {
        public static string? Epoch { get; set; }   

        public static bool SetEpoch()
        {
            var epochNumber = DateTime.UtcNow.Ticks / 10000000 - 63082281600;
            var epoch = epochNumber.ToString();
            EPOCHControl.Epoch = epoch;
            DebugOutput.Log($"Mid Test EPOCH SET TO  {epoch}");
            return true;
        }

    }
}
