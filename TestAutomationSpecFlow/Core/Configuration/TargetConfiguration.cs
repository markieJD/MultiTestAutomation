﻿using Core.FileIO;
using Core.Logging;
using Newtonsoft.Json;
using OpenQA.Selenium;
using System.Collections;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace Core.Configuration
{
    public class TargetConfiguration
    {
        /// <summary>
        ///     Environment variable that is read to get the target environment name
        /// </summary>
        private const string EnvironmentVariable = "ENVIRONMENT";
        public static TargetConfigurationData Configuration { get; private set; } = new TargetConfigurationData();

        public class TargetConfigurationData
        {
            public string ApplicationType { get; set; } = String.Empty;
            public string Browser { get; set; } = String.Empty;
            public string StartUrlx { get; set; } = String.Empty;
            public string StartUrl { get; set; } = String.Empty;
            public string ApiUrl { get; set; } = String.Empty;
            public bool HandleBugs { get; set; } = true;
            public int PositiveTimeout { get; set; } = 15;
            public int NegativeTimeout { get; set; } = 2;
            public string DateFormat { get; set; } = String.Empty;
            public string AreaPath { get; set; } = String.Empty;
            public bool OutputOnly { get; set; } = false;
            public bool FeatureFileOnly { get; set; } = false;
            public string ApiDatabaseName { get; set; } = String.Empty;
            public string InstallLocation { get; set; } = String.Empty;
            public int TimeoutMultiplie { get; set; } = 1;
            public bool SkipOnFailure { get; set; } = true;
            public string FirstPage { get; set; } = String.Empty;
            public int Debug { get; set; } = 7;
            public string ProjectName { get; set; } = String.Empty;
            public string Version { get; set; } = "0.0.1";
            public string ScreenSize { get; set; } = "1200x800";
            public string[] DatePickerButtonOpen { get; set; } = { "By.Name(\"PART_DropDownButton\")" };
            public string[] DatePickerText { get; set; } = { "By.ClassName(\"RadWatermarkTextBox\")", "" };
            public string[] DropDownItemLocators { get; set; } = { "By.TagName(\"option\")", "" };
            public string[] ListItemLocator { get; set; } = { "By.ClassName(\"RadListBoxItem\")" };
            public string[] SpinnerLocator { get; set; } = { "By.Id(\"loading\")" };
            public string[] StepperStepLocator { get; set; } = { "By.TagName(\"a\")" };
            public string[] TabLocator { get; set; } = { "By.ClassName(\"RadTabItem\")" };
            public string[] TabTextLocator { get; set; } = { "By.XPath(\"//span\")" };
            public int TablePrimaryColumnNumber { get; set; } = 1;
            public string[] TableActions { get; set; } = { "none" };
            public string[] TableActionsLocator { get; set; } = { "none" }; 
            public string[] TableHeadLocator { get; set; } = { "By.ClassName(\"GridViewHeaderRow\")" };
            public string[] TableHeadCellsLocator { get; set; } = { "By.ClassName(\"GridViewHeaderCell\")" };
            public string[] TableBodyLocator { get; set; } = { "By.ClassName(\"PART_GridViewVirtualizingPanel\")" };
            public string[] TableBodyRowLocator { get; set; } = { "By.ClassName(\"GridViewRow\")" };
            public string[] TableBodyRowSubLocator { get; set; } = { "By.ClassName(\"ExhibitsSearchScene.JsonDAL.Models.Search\")" };
            public string[] TableBodyCellsLocator { get; set; } = { "By.ClassName(\"GridViewCell\")" };
            public string[] TableNextPageButton { get; set; } = { "By.XPath(\"//button[contains(text(),'Next')]\")" };
            public string[] TablePreviousPageButton { get; set; } = { "By.XPath(\"//button[contains(text(),'Previous')]\")" };
            public string[] TableFilterLocator { get; set; } = { "By.Id(\"searchBox\")" };
            public string[] TimePickerButtonOpen { get; set; } = { "By.Name(\"PART_DropDownButton\")" };
            public string[] TimePickerText { get; set; } = { "By.ClassName(\"RadWatermarkTextBox\")" };
            public string[] TreeNodeLocator { get; set; } = { "By.ClassName(\"RadTreeViewItem\")" };
            public string[] TreeNodeSelector { get; set;} = { "By.ClassName(\"org\")" };
            public string[] TreeNodeToggleLocator { get; set; } = { "By.Id(\"Expander\")" };
            public string[] TreeAddNodeText { get; set; } = { "new location" };
            public string[] TreeAddNodeButton { get; set; } = { "add new location" };
        }

        //public static void TargetConfiguration()
        public static TargetConfigurationData? ReadJson()
        {
            var fileName = $"targetSettings.{Environment}.json";
            var directory = ".\\AppTargets\\Resources\\";
            var fullFileName = directory + fileName;
            if (!FileChecker.FileCheck(fullFileName))
            {
                DebugOutput.Log($"Unable to find the file {fullFileName}");
                return null;
            }
            var jsonText = File.ReadAllText(fullFileName);
            try
            {
                var obj = JsonConvert.DeserializeObject<TargetConfigurationData>(jsonText);
                if (obj == null) return null;
                Configuration = obj;
                DebugOutput.Log($">>>> {obj.AreaPath}  ... {Configuration.AreaPath}");
                return Configuration;
            }
            catch
            {
                DebugOutput.Log($"We out ere");
                return null;
            }
        }


        /// <summary>
        ///     Set up an environment variable called "ENVIRONMENT", and set it to type of environment 
        ///     development, beta, live etc.
        ///     Then you need a targetSettings.ENVIRONMENT.json file in the Resources folder of the AppTargets project
        ///     Different environments need different configuration.  this is how that is controled.
        ///     If there is no ENVIRONMENT environment variable it will use development as default.
        /// </summary>
        private static string Environment =>
            System.Environment.GetEnvironmentVariable(EnvironmentVariable) ?? "development";

    }
}