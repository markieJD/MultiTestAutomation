﻿using AppXAPI.APIApps;
using AppXAPI.Models;
using Core.FileIO;
using Core.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class APIUtil
    {
        public static async Task PatchWithWindowsAuth(string url, string jSonText, string apiName = "default", string userName = "", string userPassword = "", bool addSquareBrckets = true)
        {
            DebugOutput.Log($"PatchWithWindowsAuth {url}");
            if (addSquareBrckets)
            {
                jSonText = "[" + jSonText + "]";
            }   
            DebugOutput.Log($"JSON = {jSonText}");
            var credentials = new NetworkCredential( userName, userPassword );
            using (HttpClient client = new HttpClient(new HttpClientHandler() { Credentials = credentials }))
            try
            {
                HttpResponseMessage response = new HttpResponseMessage();
                response = await client.PatchAsync(url, new StringContent(jSonText, Encoding.UTF8, "application/json"));
                APIList.fullResponse = response;
                var statusCode = (int)response.StatusCode;
                DebugOutput.Log($"Status Code of {statusCode} received");
                if (response.IsSuccessStatusCode)
                {
                    DebugOutput.Log($"We are DONE Sucessfully!");
                    return;
                }
                else
                {
                    DebugOutput.Log($"Failure code received from API {statusCode}");
                }
            }
            catch
            {

            }

        }


        public static async Task PostWithWindowsAuth(string url, string jSonText, string apiName = "default", string userName = "", string userPassword = "", bool addSquareBrckets = true)
        {
            DebugOutput.Log($"PostWithWindowsAuth {url}");
            if (addSquareBrckets)
            {
                jSonText = "[" + jSonText + "]";
            }   
            DebugOutput.Log($"JSON = {jSonText}");
            var credentials = new NetworkCredential( userName, userPassword );
            try
            {
                using (HttpClient client = new HttpClient(new HttpClientHandler() { Credentials = credentials }))
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    response = await client.PostAsync(url, new StringContent(jSonText, Encoding.UTF8, "application/json"));
                    APIList.fullResponse = response;
                    var statusCode = (int)response.StatusCode;
                    DebugOutput.Log($"Status Code of {statusCode} received");
                    if (response.IsSuccessStatusCode)
                    {
                        return;
                    }
                    else
                    {
                        DebugOutput.Log($"Failure code received from API {statusCode}");
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"Failed when posting Json!");
            }
            return;
        }

        public static async Task WaitForAPICompletion(string url, string completionCheck, string userName = "", string userPassword = "")
        {
            DebugOutput.Log($"Proc - WaitForAPICompletion {url} {completionCheck} {userName} {userPassword}");
            var credentials = new NetworkCredential( userName, userPassword );            
            try
            {
                using (HttpClient client = new HttpClient(new HttpClientHandler() { Credentials = credentials }))
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    try
                    {
                        response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
                        APIList.fullResponse = response;
                        var statusCode = (int)response.StatusCode;
                        DebugOutput.Log($"Status Code of {statusCode} received");
                        if (response.IsSuccessStatusCode)
                        {
                            DebugOutput.Log($"We have a success code - but is it completed?");
                            int waitTimer = 500;
                            int fullTimer = 10000;
                            for (int x = 0; x < fullTimer; x = x + waitTimer)
                            {
                                var fullJson = await client.GetStringAsync(url);
                                DebugOutput.Log($"FULL JSON = {fullJson}");
                                if (fullJson.Contains("ProcessedJournal"))
                                {
                                    DebugOutput.Log($"DONE!");
                                    return;
                                } 
                                DebugOutput.Log($"NOT DONE YET! {x}");
                                Thread.Sleep(500);
                            }
                            return;
                        }
                        else
                        {
                            DebugOutput.Log($"Failure code received from API {statusCode}");
                        }
                    }
                    catch
                    {
                        DebugOutput.Log($"Issue here with CHECK API");
                        return;
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"Issue USING httpClient");
                return;
            }
            DebugOutput.Log($"DONE!");

        }


        public static async Task GetWithWindowsAuth(string url, string apiName = "unknownAPI", string userName = "", string userPassword = "")
        {
            DebugOutput.Log($"Proc - GetWithWindowsAuth {url} {apiName} {userName} {userPassword}");
            var credentials = new NetworkCredential( userName, userPassword );
            try
            {
                using (HttpClient client = new HttpClient(new HttpClientHandler() { Credentials = credentials }))
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    try
                    {
                        response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
                        APIList.fullResponse = response;
                        var statusCode = (int)response.StatusCode;
                        DebugOutput.Log($"Status Code of {statusCode} received");
                        if (response.IsSuccessStatusCode)
                        {
                            var fullJson = await client.GetStringAsync(url);
                            WriteAPIJsonToFile(fullJson, apiName);
                            return;
                        }
                        else
                        {
                            DebugOutput.Log($"Failure code received from API {statusCode}");
                        }
                    }
                    catch
                    {
                        DebugOutput.Log($"Issue here with API {apiName}");
                        return;
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"Issue USING httpClient");
                return;
            }
            DebugOutput.Log($"DONE!");
        }

        public static async Task Get(string url, string apiName = "unknownAPI")
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = new HttpResponseMessage();
                    try
                    {
                        //var byteArray = Encoding.ASCII.GetBytes("duffym:wqewqrq!wrwqrwq");
                        //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                        response = await client.GetAsync(url);
                        APIList.fullResponse = response;
                        var statusCode = (int)response.StatusCode;
                        DebugOutput.Log($"Status Code of {statusCode} received");
                        if (response.IsSuccessStatusCode)
                        {
                            var fullJson = await client.GetStringAsync(url);
                            WriteAPIJsonToFile(fullJson, apiName);
                            return;
                        }
                        else
                        {
                            DebugOutput.Log($"Failure code received from API {statusCode}");
                        }
                    }
                    catch
                    {
                        DebugOutput.Log($"Issue here with API {apiName}");
                        return;
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"Issue USING httpClient");
                return;
            }
        }

        private static bool WriteAPIJsonToFile(string text, string apiName)
        {
            DebugOutput.Log($"WriteAPIJsonToFile {apiName}");
            try
            {
                var fileName = $"{apiName}.json";
                var directory = "\\AppXAPI\\APIOutFiles\\";
                var fullFileName = directory + fileName;
                if (!FileChecker.FileDeletion(fullFileName))
                {
                    DebugOutput.Log($"FAIL");
                    return false;
                }
                if (!FileChecker.FilePopulate(fullFileName, text))
                {
                    DebugOutput.Log($"FAIL to create file");
                    return false;
                }
            }
            catch
            {
                DebugOutput.Log($"Also a problem in or around the files");
                return false;
            }
            return true;
        }
    }
}
