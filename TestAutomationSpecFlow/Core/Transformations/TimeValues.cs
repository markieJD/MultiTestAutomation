﻿using Core.Configuration;
using Core.Logging;
using System;

namespace Core.Transformations
{
    public static class TimeValues
    {
		public static string ReturnNowTimeAsString(string format = "HH:mm")
		{
			DebugOutput.Log($"Proc - ReturnNowTimeAsString");
			DateTime now = DateTime.Now;
			var time = now.ToString(format);
			DebugOutput.Log($"sending back time as  {time}");
			return time;
		}
	}
}
