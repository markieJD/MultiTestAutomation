﻿using Core.FileIO;
using Core.Logging;
using Newtonsoft.Json;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Transformations.PageModels
{
    /// <summary>
    /// What attributes are we recording for an element
    /// Can not use the word Class - replaced for tagClass
    /// </summary>
    public class ElementModels
    {
        public int Counter { get; set; }
        public string? Id { get; set; }
        public string? LocatorName { get; set; }
        public string? Name { get; set; }
        public string? Title { get; set; }  
        public string? Text { get; set; }
        public string? Tag { get; set; }
        public string? TagClass { get; set; }   
        public string? Href { get; set; }   
        public string? Value { get; set; }
        public string? FullXPath { get; set; }
        public string? ElementId { get; set; }   
    }
    public class ElementModelsApp
    {

        private static string replacChar = "|";

        public static string ReplacChar { get => replacChar; set => replacChar = value; }

        /// <summary>
        /// If adding or subtracting from this list make sure to populate the methods below!
        /// </summary>
        /// <returns></returns>
        public static List<string> GetListAttributes()
        {
            var attributeList = new List<string>
            {
                "counter",
                "elementid",
                "locatorname",
                "fullxpath",
                "id",
                "name",
                "title",
                "text",
                "tag",
                "tagclass",
                "href",
                "value"
            };
            return attributeList;
        }

        private static int IsAttributePopulated(string value)
        {
            if (value == null || value == "")
                return 0;
            return 1;
        }

        public static int HowManyAttributesPopulated(ElementModels model)
        {
            int counter = 0;
            var elementList = GetListAttributes();
            if (elementList == null) return 0;
            foreach (var attribute in elementList)
            {
                var value = "";
                if (attribute == "elementid")
                {
                    value = model.ElementId;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "fullxpath")
                {
                    value = model.FullXPath;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "id")
                {
                    value = model.Id;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "locatorname")
                {
                    value = model.LocatorName;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "name")
                {
                    value = model.Name;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "title")
                {
                    value = model.Title;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "text")
                {
                    value = model.Text;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "tag")
                {
                    value = model.Tag;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "tagclass")
                {
                    value = model.TagClass;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "href")
                {
                    value = model.Href;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
                if (attribute == "value")
                {
                    value = model.Value;
                    if (value != null)
                        counter += IsAttributePopulated(value);
                }
            }
            return counter;
        }

        public static void OutPutAllAttributeValues(ElementModels elementModel)
        {
            DebugOutput.Log($" {elementModel.Counter} | {elementModel.ElementId}  | {elementModel.Id} | {elementModel.Name} | {elementModel.LocatorName} |{elementModel.Title} | {elementModel.Text} | {elementModel.Tag} | {elementModel.TagClass} | {elementModel.Href} | {elementModel.Value} | {elementModel.FullXPath}");
        }

        public static bool IsEqual(List<ElementModels> elementModelList1, List<ElementModels> elementModelList2)
        {
            if (elementModelList1.Count != elementModelList2.Count) return false;
            if (!IsEqualElement(elementModelList1, elementModelList2)) return false;
            return false;
        }

        private static bool IsEqualElement(List<ElementModels> elementModelList1, List<ElementModels> elementModelList2)
        {
            bool totalMatch = false;
            int counter = 0;
            foreach (ElementModels elementModel1 in elementModelList1)
            {
                foreach (string attribute in GetListAttributes())
                {
                    if (attribute != "elementid" )  //elementid is changed every run!
                    {
                        if (attribute == "locatorname")
                        {
                            if (elementModel1.LocatorName != elementModelList2[counter].LocatorName)
                            {
                                DebugOutput.Log($"Failed as LocatorName {elementModel1.LocatorName} does not equal {elementModelList2[counter].LocatorName} for element counter {counter}");
                                return totalMatch;
                            }
                            if (elementModel1.FullXPath != elementModelList2[counter].FullXPath)
                            {
                                DebugOutput.Log($"Failed as FullXPath {elementModel1.FullXPath} does not equal {elementModelList2[counter].FullXPath} for element counter {counter}");
                                return totalMatch;
                            }
                            if (elementModel1.Id != elementModelList2[counter].Id)
                            {
                                DebugOutput.Log($"Failed as Id {elementModel1.Id} does not equal {elementModelList2[counter].Id} for element counter {counter}");
                                return totalMatch;
                            }
                            if (elementModel1.TagClass != elementModelList2[counter].TagClass)
                            {
                                DebugOutput.Log($"Failed as TagClass {elementModel1.TagClass} does not equal {elementModelList2[counter].TagClass} for element counter {counter}");
                                return totalMatch;
                            }
                            if (elementModel1.Href != elementModelList2[counter].Href)
                            {
                                DebugOutput.Log($"Failed as Href {elementModel1.Href} does not equal {elementModelList2[counter].Href} for element counter {counter}");
                                return totalMatch;
                            }
                        }
                    }
                }
                counter++;
            }
            totalMatch = true;
            return totalMatch;
        }

        public static List<ElementModels>? GetElementModelsFromJson(string fileNameAndLocation)
        {
            List<ElementModels>? previousPageElementModels = new();
            var jsonInput = FileChecker.GetFileContentsAsString(fileNameAndLocation);
            if (jsonInput == null)
            {
                DebugOutput.Log($"Failed ont he reading part!");
                return previousPageElementModels;
            }
            try
            {
                previousPageElementModels = JsonConvert.DeserializeObject<List<ElementModels>>(jsonInput);
            }
            catch
            {
                DebugOutput.Log($"Been a problem deserializing the File to Json");
            }
            return previousPageElementModels;
        }

        private static string RemoveSlashes(string input, bool noSlash)
        {
            if (noSlash)
            {
                return StringValues.ReplaceAllSlashes(input, ReplacChar);
            }
            return input;
        }

        /// <summary>
        /// populate the Element Model Class for each Element WANTED on page 
        /// </summary>
        /// <param name="element"></param>
        /// <param name="noSlash"></param>
        /// <returns></returns>
        public static ElementModels GetElementModelValues(IWebElement element, int counter, bool noSlash = false)
        {
            var returnElement = new ElementModels();
            var attributeList = GetListAttributes();
            string value;
            foreach (var attribute in attributeList)
            {
                if (attribute == "counter")
                    returnElement.Counter = counter;
                value = SeleniumUtil.GetElementAttributeValue(element, attribute);
                if (value != "")
                {
                    if (attribute == "locatorname")
                        returnElement.LocatorName = value;
                    if (attribute == "elementid")
                        returnElement.ElementId = value;
                    if (attribute == "fullxpath")
                        returnElement.FullXPath = RemoveSlashes(value, noSlash);
                    if (attribute == "id")
                        returnElement.Id = RemoveSlashes(value, noSlash);
                    if (attribute == "name")
                        returnElement.Name = RemoveSlashes(value, noSlash);
                    if (attribute == "title")
                        returnElement.Title = RemoveSlashes(value, noSlash);
                    if (attribute == "text")
                        returnElement.Text = RemoveSlashes(value, noSlash);
                    if (attribute == "tag")
                        returnElement.Tag = RemoveSlashes(value, noSlash);
                    if (attribute == "tagclass")
                        returnElement.TagClass = RemoveSlashes(value, noSlash);
                    if (attribute == "href")
                        returnElement.Href = RemoveSlashes(value, noSlash);
                    if (attribute == "value")
                        returnElement.Value = RemoveSlashes(value, noSlash);
                }
            }
            return returnElement;
        }
    }
}
