﻿using Core.Configuration;
using Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Core.Transformations
{
    public static class StringValues
    {
		/// <summary>
		/// Given Text we will replace certain TEXT with other 
		/// </summary>
		/// <param name="value"></param>
		/// <returns>text supplied changed</returns>
		public static string TextReplacementService(string value)
        {
			DebugOutput.Log($"TextReplacementService {value}");
			if (value.Contains("%APPDATA"))
			{
				var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
				value = value.Replace("%APPDATA", appData);
			}
			if (value.Contains("<DATEACTION>"))
			{
				//103 is dd/MM/yyyy
				var dateChange = DateValues.ReturnNowDateAsString("103");
				value = value.Replace("<DATEACTION>", dateChange);
			}
			if (value.Contains("DATEACTION"))
            {
				//103 is dd/MM/yyyy
				var dateChange = DateValues.ReturnNowDateAsString("103");
				value = value.Replace("DATEACTION", dateChange);
			}
			if (value.Contains("<DATEREVERSE>"))
			{
				//23 is yyyy-mm-dd
				var dateChange = DateValues.ReturnNowDateAsString("23");
				value = value.Replace("<DATEREVERSE>", dateChange);
			}
			if (value.Contains("DATEREVERSE"))
			{
				//23 is yyyy-mm-dd
				var dateChange = DateValues.ReturnNowDateAsString("23");
				value = value.Replace("DATEREVERSE", dateChange);
			}
			if (value.Contains("<DATEWARRANT>"))
            {
				var dateChange = DateValues.ReturnFirstOfThisMonth("23");
				value = value.Replace("<DATEWARRANT>", dateChange);
			}
			if (value.Contains("DATEWARRANT"))
			{
				var dateChange = DateValues.ReturnFirstOfThisMonth("23");
				value = value.Replace("DATEWARRANT", dateChange);
			}
			if (value.Contains("<EPOCH>"))
			{
				var featureEpoch = EPOCHControl.Epoch;
				DebugOutput.Log($"Replacing EPOCH with Epoch number {featureEpoch} ");
				value = value.Replace("<EPOCH>", featureEpoch);
			}
			if (value.Contains("EPOCH"))
            {
				var featureEpoch = EPOCHControl.Epoch;
				if (featureEpoch == null) featureEpoch = "000001";
				if (value.Contains("|"))
				{
					string[] brokenUp = value.Split("|");					
					DebugOutput.Log($"We don't want the WHOLE of the EPOCH");
					int number = 0;
					try
					{
						number = Int32.Parse(brokenUp[1]);
						DebugOutput.Log($"We only want the last {number} chars of EPOCH ");
						var toBeReturned = featureEpoch.Substring(featureEpoch.Length - number);
						if (toBeReturned != null)
						{
							return toBeReturned;
						} 
					}
					catch
					{
						DebugOutput.Log($"Tried to break up EPOCH - FAILED!");
					}
				}
				DebugOutput.Log($"Replacing EPOCH with Epoch number {featureEpoch} ");
				value = value.Replace("EPOCH", featureEpoch);
			}
			if (value.Contains("FIRSTOFMOTH") || value.Contains("FIRSTOFTHEMONTH"))
			{
				var date = DateValues.ReturnFirstOfThisMonth("103");
				DebugOutput.Log($"Replacing FIRSTOFTHEMONTH with {date} ");
				return date;
			}
			if (value.Contains("NOW"))
			{
				var timeChange = TimeValues.ReturnNowTimeAsString();
				value = value.Replace("NOW", timeChange);
			}
			if (value.Contains("CURRENTHOUR"))
			{
				var timeChange = TimeValues.ReturnNowTimeAsString("HH");
				value = value.Replace("CURRENTHOUR", timeChange);
			}
			if (value.Contains("CURRENTMINUTE"))
			{
				var timeChange = TimeValues.ReturnNowTimeAsString("mm");
				value = value.Replace("CURRENTMINUTE", timeChange);
			}
			if (value.Contains("TODAY"))
            {
				var countryCode = "101";
				var dateFormatCountry = TargetConfiguration.Configuration.DateFormat;
				if (dateFormatCountry == "UK") countryCode = "103";
				if (value.Contains("+") || value.Contains("-"))
                {
					DebugOutput.Log($"We have some maths to do!");
					bool plus = false;
					if (value.Contains("+")) plus = true;
					/// splits TODAY+10 into TODAY and 10,  TOMORROW-2 into TOMORROW and 2
					string[] brokenUpText;
					string returnDate;
					if (plus)
                    {
						brokenUpText = value.Split("+");
						returnDate = DateValues.MathsToDate(brokenUpText[1], "+");
					}
					else
					{
						brokenUpText = value.Split("-");
						returnDate = DateValues.MathsToDate(brokenUpText[1], "-");
					}
					value = returnDate;
					return returnDate;
				}
				var dateChange = DateValues.ReturnNowDateAsString(countryCode);
				value = value.Replace("TODAY", dateChange);
			}
			DebugOutput.Log($"Size = {value.Length}");
			if (value.Length < 1000)
			{
				DebugOutput.Log($"Returning after TextReplacement {value}");
			}
			return value;
        }

		/// <summary>
		///     Take a value string and change everything to lowercase
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string GetAllCharToLower(string value)
		{
			DebugOutput.Log($"Proc - GetAllCharToLower {value}");
			return string.IsNullOrEmpty(value) ? string.Empty : value.ToLower();
		}

		public static string ProjectSpecificTextChanges(string value)
		{
			DebugOutput.Log($"Proc - ProjectSpecificTextChanges {value}");
			return "";
        }

		/// <summary>
		/// Add tabs (5 spaces) to BEFORE a string
		/// </summary>
		/// <param name="number"></param>
		/// <returns></returns>
		public static string Tabs(int number, string message)
		{
            string tabs = "";
            for (int i = 0; i < number; i++)
            {
                tabs = tabs + "     ";
            }
            return tabs + message;
        }

		public static string ReplaceAllSlashes(string value, string replaceWith)
        {
			value = ReplaceAllForwardSlashes(value, replaceWith);
			value = ReplaceAllBackSlashes(value, replaceWith);
			return value;
        }

		public static string ReplaceAllBackSlashes(string value, string replaceWith)
		{
			//DebugOutput.Log($"Proc - ReplaceAllForwardSlashes {value} {replaceWith}");
			if (string.IsNullOrEmpty(value))
			{
				return value;
			}
			return value.Replace(@"\", replaceWith);
		}

		public static string ReplaceAllForwardSlashes(string value, string replaceWith)
		{
			//DebugOutput.Log($"Proc - ReplaceAllForwardSlashes {value} {replaceWith}");
			if (string.IsNullOrEmpty(value))
			{
				return value;
			}
			return value.Replace(@"/",replaceWith);
		}

		/// <summary>
		/// Take a string and remove any hidden HTML chars and return
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string RemoveHtmlFromEnd(string value)
		{
			//DebugOutput.Log($"Proc - RemoveHtmlFromEnd {value}");

			if (string.IsNullOrEmpty(value))
			{
				return value;
			}

			value = Regex.Replace(value, @"<[^>]+>|&nbsp|\n;", "").Trim();

			return value;
		}

		/// <summary>
		/// Certain chars are fine on screen, but in comparison.. BAD
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string RemoveReserveredChars(string value)
		{

			if (string.IsNullOrEmpty(value))
			{
				return value;
			}

			value = value.Replace("$", "");
			value = value.Replace("-", "");
			value = value.Replace("(", "");
			value = value.Replace(")", "");
			value = value.Replace("%", "");
			value = value.Replace(" ", "");
			return value;
		}

		/// <summary>
		/// Remove everything bar upper and lower case chars and numbers!
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string RemoveAllNonAlphaNumericChars(string value)
		{

			if (string.IsNullOrEmpty(value))
			{
				return value;
			}

			value = Regex.Replace(value, "[^a-zA-Z0-9]", String.Empty);
			return value;
		}

		/// <summary>
		/// Certain chars are fine on screen, but in comparison.. BAD
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string RemoveReserveredCharsExceptSpace(string value)
		{

			if (string.IsNullOrEmpty(value))
			{
				return value;
			}

			value = value.Replace("$", "");
			value = value.Replace("-", "");
			value = value.Replace("(", "");
			value = value.Replace(")", "");
			value = value.Replace("%", "");
			value = value.Replace("\"", "");
			return RemoveHtmlFromEnd(value);
		}

		public static string RemoveRequiredFieldAstrixFromHeader(string value)
		{
			DebugOutput.Log($"Proc - RemoveRequiredFieldAstrixFromHeader {value}");
			value = value.Replace(" *", "");
			return value;
		}

		/// <summary>
		/// Take a string and remove all spaces from it.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string RemoveAllSpaces(string value)
		{
			DebugOutput.Log($"Proc - RemoveAllSpaces {value}");
			var returnedValue = value.Replace(" ", "");
			return returnedValue;
		}

		/// <summary>
		/// Remove number chars from a string.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="number"></param>
		/// <returns></returns>
		public static string RemoveLastNChars(string value, int number)
		{
			DebugOutput.Log($"Proc - RemoveLastNChars {value}");
			return string.IsNullOrEmpty(value) ? value : value.Substring(0, value.Length - number);
		}

		/// <summary>
		/// pass in string using bang ! as delimited and replace with numbers
		/// </summary>
		/// <param name="xpath"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public static string FillDynamicXPathWildcardBang(string xpath, params int[] values)
		{
			DebugOutput.Log($"Proc - FillDynamicXPathWildcardBang {xpath}");
			const char delimiter = '!';
			return BreakStringUpByDelmited(xpath, delimiter, values);
		}

		/// <summary>
		/// pass in a string using astrix * as demited and replace with numbers
		/// </summary>
		/// <param name="xpath"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		public static string FillDynamicXPathWildcards(string xpath, params int[] values)
		{
			DebugOutput.Log($"Proc - FillDynamicXPathWildcardBang {xpath}");
			const char delimiter = '*';
			DebugOutput.Log($"Proc - FillDynamicXPathWildcards {xpath} with {delimiter} ");
			return BreakStringUpByDelmited(xpath, delimiter, values);
		}

		public static string[] BreakUpByDelimited(string value, string delimiter)
		{
			DebugOutput.Log($"Proc - BreakUpByDelimited {value} with {delimiter} ");
			return value.Split(delimiter);
		}

		public static string? ReturnIntAsStringWithPaddingZeros(int number, int howManyChars)
		{
            DebugOutput.Log($"ReturnIntAsStringWithSoManyChars {number} {howManyChars}");
			if (number < 0)
			{
				DebugOutput.Log($"Can not pad negative numbers!");
				return "";
			}
			return number.ToString().PadLeft(howManyChars, '0');
		}

		public static string? GetTextInCase(string value, string textCase = "camel")
		{
			textCase = textCase.ToLower();
			DebugOutput.Log($"GetTextInCase {value} {textCase}");
            if (textCase.ToLower() == "lower") return value.ToLower();
            if (textCase.ToLower() == "upper") return value.ToUpper();
			if (textCase.ToLower() == "camel")
			{
				var returnText = "";
				var brokenUpText = BreakUpByDelimitedToList(value, " ");
				foreach (var word in brokenUpText)
				{
					var firstLetter = word.Substring(0,1);
					var restOfWord = word.Substring(1,word.Length-1);
					firstLetter = firstLetter.ToUpper();
					returnText = firstLetter + restOfWord;
					return returnText;
				}
			}
			DebugOutput.Log($"Failed to do case!");
			return null;
		}


		public static List<string> BreakUpByDelimitedToList(string value, string delimiter = "|")
		{
            DebugOutput.Log($"BreakUpByDelimitedToList {value} {delimiter}");
			var oneValueList = new List<string>();
			if (!value.Contains(delimiter))
			{
				oneValueList.Add(value);
				return oneValueList;
			}
			var arrayReturn = BreakUpByDelimited(value, delimiter);
			return ConvertArrayToList(arrayReturn);
		}

		public static List<string> ConvertArrayToList(string[] array)
        {
            DebugOutput.Log($"ConvertArrayToList {array.Count()}");
            List<string> returnList = new List<string>();
            foreach (var item in array)
            {
                returnList.Add(item);
            }
            return returnList;
        }

		

		/// <summary>
		/// Break up a string passing in the delimiter value
		/// </summary>
		/// <param name="xpath"></param>
		/// <param name="delimiter"></param>
		/// <param name="values"></param>
		/// <returns></returns>
		private static string BreakStringUpByDelmited(string xpath, char delimiter, params int[] values)
		{
			DebugOutput.Log($"Proc - BreakStringUpByDelmited {xpath} with {delimiter} ");
			var wildcardCount = xpath.Count(x => x == delimiter);
			var valueCount = values.Length;

			if (wildcardCount != valueCount)
			{
				throw new ArgumentException(
					"Wrong number of integer parameters supplied. If you're surprised that you're seeing this message, then check that you aren't using a dynamic element selector in an `id`-based XPath!");
			}

			string[] XPathBuilder = xpath.Split(delimiter);
			var finalXPath = "";
			int counter = 0;

			foreach (var value in values)
			{
				DebugOutput.Log($"{counter} = {value}");
				finalXPath = finalXPath + XPathBuilder[counter] + value.ToString();
				counter++;
			}

			finalXPath = finalXPath + XPathBuilder[counter];
			DebugOutput.Log(finalXPath);
			return finalXPath;
		}
	}
}
