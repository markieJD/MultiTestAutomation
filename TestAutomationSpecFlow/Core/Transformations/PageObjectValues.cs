﻿using Core.Configuration;
using Core.FileIO;
using Core.Logging;
using Core.Transformations.Elements;
using Core.Transformations.PageModels;
using OpenQA.Selenium;
using System.Text.RegularExpressions;

namespace Core.Transformations
{
    public static class PageObjectValues
    {

        private static readonly string topDirectory = @"\AppTargets\Forms\";
        private static readonly string jSonDirectory = @"\ElementsJson\";

        public static bool PopulatePageObject(string pageName, string directory)
        {
            pageName = StringValues.GetTextInCase(TargetConfiguration.Configuration.AreaPath) + StringValues.GetTextInCase(pageName);
            DebugOutput.Log($"PopulatePageObject {pageName}");
            var pageObjectFileName = pageName + "Page.cs";
            var pathAndFileName = $".\\{topDirectory}\\{directory}\\{pageObjectFileName}";
            if (!FileChecker.FileCheck(pathAndFileName))
            {
                if (!FileChecker.DirectoryCheck($"{topDirectory}\\{directory}"))
                {
                    if (!FileChecker.DirectoryCreation($"{topDirectory}\\{directory}"))
                    {
                        DebugOutput.Log($"Well that went horrible!");
                        return false;
                    }
                }
                if (!CreateAndPopulatePageFile(pageName, pathAndFileName, directory))
                {
                    DebugOutput.Log($"FAILED TO CREATE FILE");
                    return false;
                }
                else
                {
                    DebugOutput.Log($"{pathAndFileName} File has been created!");
                }
            }
            else
            {
                DebugOutput.Log($"File already exists!  WE CAN NOT MODIFY A FILE ALREADY IN EXISTANCE!");
                return false;
            }
                return true;
        }

        public static bool ComparePageNowAndThen(string pageName, string directory)
        {
            List<ElementModels> allCurrentElements = GetPopulatedElementModelListForThisPage();

            //Get Json for page as Model

            var fileNameAndLocation = GetCurrentPageJsonFileLocationAndName(pageName, directory);
            if (!FileChecker.FileCheck(fileNameAndLocation))
            {
                DebugOutput.Log($"There is no file storage Json for {pageName}Page.json in project directory {directory}");
                return false;
            }
            List<ElementModels>? previousPageElementModels = ElementModelsApp.GetElementModelsFromJson(fileNameAndLocation);
            if (previousPageElementModels == null) return false;
            if (allCurrentElements.Count == 0 || previousPageElementModels.Count == 0)
            {
                DebugOutput.Log($"Can not comapre ZERO!");
                return false;
            }
            return ElementModelsApp.IsEqual(allCurrentElements, previousPageElementModels);
        }

        /// <summary>
        /// Put numbers on FULL XPaths so unique
        /// </summary>
        /// <param name="allElements"></param>
        /// <returns></returns>
        private static List<ElementModels> PutCounterOnFullXPath (List<ElementModels> allElements)
        {
            var returnAllElements = new List<ElementModels>();
            var listOfFullXPaths = new List<string>();
            foreach (ElementModels element in allElements)
            {
                var fullXPath = element.FullXPath;
                if (fullXPath != null)
                {
                    int counter = 1;
                    foreach (string listedFullXPath in listOfFullXPaths)
                    {
                        if (fullXPath == listedFullXPath)
                            counter++;
                    }
                    element.FullXPath = element.FullXPath + "[" + counter + "]";
                    returnAllElements.Add(element);
                    listOfFullXPaths.Add(fullXPath);
                }                
            }
            return returnAllElements;
        }

        private static List<ElementModels> PutLocatorNameFromName (List<ElementModels> allElements)
        {
            var returnAllElements = new List<ElementModels>();
            foreach (ElementModels element in allElements)
            {
                if (element.Name != "" | element.Name is not null)
                {
                        element.LocatorName = element.Name;
                }
                returnAllElements.Add(element);
            }
            return returnAllElements;
        }

        private static List<ElementModels> PutTagAndCoutnerForName (List<ElementModels> allElements)
        {
            var returnAllElements = new List<ElementModels>();
            var listOfTags = new List<string>();
            foreach (ElementModels element in allElements)
            {
                var name = element.Name;
                if (name == null)
                {
                    var id = element.Id;
                    if (id != null)
                    {
                        element.Name = id;
                    }
                    else
                    {
                        var title = element.Title;
                        if (title != null)
                        {
                            element.Name = title;
                        }
                        else
                        {
                            var text = element.Text;
                            if (text != null)
                            {
                                DebugOutput.Log($"no name, no id no title, but there is some text!");
                                element.Name = text;
                            }
                            else
                            {
                                int counter = 1;
                                var tag = element.Tag;
                                foreach (string tagInList in listOfTags)
                                {
                                    if (tag == tagInList)
                                    {
                                        counter++;
                                    }
                                }
                                if (tag != null) // it can never be null - but c# want a check for null!
                                {
                                    listOfTags.Add(tag);
                                    element.Name = tag + " " + counter;
                                }
                            }
                        }
                    }
                }
                returnAllElements.Add(element);
            }
            return returnAllElements;
        }

        private static string GetCurrentPageJsonFileLocationAndName(string pageName, string projectDir)
        {
            var fileLocation = topDirectory + projectDir + jSonDirectory;
            return fileLocation + pageName + "Page.json";
        }

        private static string GetPreviousPageJsonFileLocationAndName(string pageName, string projectDir)
        {
            var fileLocation = topDirectory + projectDir + jSonDirectory;
            return fileLocation + pageName + "Page-previous.json";
        }

        private static bool CreatePageJsonFile(string pageName, string jsonString, string projectDir)
        {
            bool created = false;
            var writeToFile = GetCurrentPageJsonFileLocationAndName(pageName, projectDir);
            var oldFile = GetPreviousPageJsonFileLocationAndName(pageName, projectDir);
            if (FileChecker.FileCheck(writeToFile))
            {
                if (!FileChecker.MoveRenameFile(writeToFile, oldFile))
                {
                    return created;
                }
            }
            try
            {
                if (!FileChecker.FilePopulate(writeToFile, jsonString))
                {
                    return created;
                }
                created = true;
                return created;
            }
            catch
            {
                DebugOutput.Log($"FAILED TO DO SOMETHING FILE SIDE");
                return created;
            }
        }

        private static bool EnterElementInToFile(StreamWriter sw, List<ElementModels> allElements, string pageName)
        {
            DebugOutput.Log($"Proc - EnterElementInToFile - adding {allElements.Count} elements");
            var directory = ".\\AppSpecFlow\\TestOutput\\PageImages\\" + TargetConfiguration.Configuration.AreaPath + "\\" + pageName;
            if (!FileChecker.DirectoryCheck(directory))
            {
                if (!FileChecker.DirectoryCreation(directory)) return false;
            }
            DebugOutput.Log($"populating with elements!");
            foreach (var element in allElements)
            {
                var elementName = element.Name;
                By locator;
                string locatorString;
                IWebElement? actualElement;
                if (!string.IsNullOrEmpty(element.Id))
                {
                    locator = By.Id(element.Id);
                    actualElement = SeleniumUtil.GetElement(By.Id(element.Id));
                }
                else
                {
                    if (!string.IsNullOrEmpty(element.LocatorName))
                    {
                        locator = By.Name(element.LocatorName);
                        actualElement = SeleniumUtil.GetElement(By.Name(element.LocatorName));
                    }
                    else
                    {
                        locator = By.XPath(element.FullXPath);
                        actualElement = SeleniumUtil.GetElement(By.XPath(element.FullXPath));
                    }
                }
                if (actualElement == null)
                {
                    DebugOutput.Log($"That is a blank element?  WEIRD!");
                    return false;
                } 
                locatorString = ReturnStringLocator(locator);
                locatorString = locatorString.Replace("(//","//");
                locatorString = locatorString.Replace(")[","[");
                locatorString = locatorString.Replace("])","]");
                if (elementName == null) return false;
                elementName = StringValues.RemoveAllNonAlphaNumericChars(elementName);
                DebugOutput.Log($"{locatorString}");
                sw.WriteLine($"                  Elements.Add(\"{elementName.ToLower()}\", {locatorString}");
                var image = SeleniumUtil.GetImageOfElementOnPage(actualElement);
                if (image != null)
                {
                    var fullFileName = directory +  "\\" + elementName.ToLower() + ".png";
                    DebugOutput.Log($"Saving to {fullFileName}");
                    image.Save(fullFileName, System.Drawing.Imaging.ImageFormat.Png);

                }
                //rqwqwrqwrqwrqwrqwrqwr
            }
            return false;
        }

        private static string ReturnStringLocator(By locator)
        {
            DebugOutput.Log($"ReturnStringLocator {locator}");
            string locatorString = locator.ToString();
            if (locatorString.Contains("By.Id"))  //By.Id: top);   - //By.Id("top); -//By.Id("top); - //By.Id("top");  > By.Id("PageID"));
            {
                locatorString = locatorString.Replace(": ", "(\"");
                locatorString += "\"));";
            }
            if (locatorString.Contains("By.Name"))  //By.Name: viewport - By.Name("viewwport - 
            {
                locatorString = locatorString.Replace(": ", "(\"");
                locatorString += "\"));";
            }
            if (locatorString.Contains("By.XPath")) //By.XPath: (//html/head/link)[2]  - By.XPath"(//html/head/link)[2] - By.XPath"(//html/head/link)[2])"));
            {
                locatorString = locatorString.Replace(": ", "(\"");
                locatorString += ")\"));";
            }
            return locatorString;
        }

        public static List<ElementModels> GetPopulatedElementModelListForThisPage ()
        {
            var allPageElements = SeleniumUtil.GetAllElements();
            var allElements = new List<ElementModels>();
            int counter = 0;
            foreach (var element in allPageElements)
            {
                counter++; //number the elements
                var elementModel = new ElementModels();
                elementModel = ElementModelsApp.GetElementModelValues(element, counter, false);
                //we have our element model!
                if (elementModel.Tag == null)
                {

                }   
                else if (!ElementClasses.GetBarredTags().Contains(elementModel.Tag.ToLower()))
                {
                    if (element.Displayed)
                    {
                        allElements.Add(elementModel);
                    }
                }
                else
                {
                    DebugOutput.Log($"SKIP Cause of {elementModel.Tag.ToLower()}");
                    //This element is of tag we do not expect or want to record!
                    //Do Nothing!
                }
            }

            //adds the counter to the FullXPath for uniqueness
            allElements = PutCounterOnFullXPath(allElements);
            allElements = PutLocatorNameFromName(allElements);
            allElements = PutTagAndCoutnerForName(allElements);

            return allElements;
        }

        private static void AddElementsToFile(StreamWriter sw, string pageName, string projectDir, bool jSonCreate = true)
        {
            List<ElementModels> allElements = GetPopulatedElementModelListForThisPage();

            //Create the Json Record
            if (jSonCreate)
            {
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(allElements);
                CreatePageJsonFile(pageName, jsonString, projectDir);
            }

            EnterElementInToFile(sw, allElements, pageName);

        }

        private static List<string> CheckDuplicates (List<string> listItems)
        {
            var hashset = new HashSet<string>(); 
            int counter = 0;
            int numberOfDuplicatesFound = 0;
            var newItemList = new List<string>();
            foreach (var item in listItems)
            {
                if (!hashset.Add(item))
                {
                    numberOfDuplicatesFound++;
                    var newName = item + " DUPLICATE";
                    newItemList.Add(newName);
                }
                else
                {
                    newItemList.Add(item);
                }
                counter++;
            }
            if (numberOfDuplicatesFound > 0)
            {
                List<string> oldItemList = newItemList;
                newItemList = CheckDuplicates(oldItemList);
            }    
            return newItemList;
        }

        /// <summary>
        /// Add to the list tags you want to exclude from writing to the Page Object File
        /// </summary>
        /// <returns>tags to be ignored</returns>
        private static List<string> GetExcludedTags()
        {
            var excludeNameList = new List<string>
            {
                "list",
                "section",
                "span"
            };

            return excludeNameList;
        }

        private static bool CreateAndPopulatePageFile(string pageName, string pathAndFileName, string project)
        {
            DebugOutput.Log($"CreateAndPopulatePageFile {pageName} {pathAndFileName}");
            try
            {
                var pageNameLowerCase = pageName.ToLower();
                var className =     $"      public class {pageName}Page : FormBase ";
                var pageClassName = $"          public {pageName}Page() : base(By.Id(\"{pageName}\"), \"{pageName} page\")";
                using StreamWriter sw = File.CreateText(pathAndFileName);
                sw.WriteLine("using Core;");
                sw.WriteLine("using OpenQA.Selenium;");
                sw.WriteLine("");
                sw.WriteLine("  namespace AppTargets.Forms");
                sw.WriteLine("  {");
                sw.WriteLine(className);
                sw.WriteLine("      {");
                sw.WriteLine(pageClassName);
                sw.WriteLine("         {");
                sw.WriteLine("              /// Add Elements");
                AddElementsToFile(sw, pageName, project, false);
                //EnterText(sw, "HELLO THERE!");
                sw.WriteLine("");
                sw.WriteLine("             // Page Dictionary");
                sw.WriteLine("");
                sw.WriteLine("         }");
                sw.WriteLine("      }");
                sw.WriteLine("  }");
                sw.WriteLine("");
            }
            catch
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get all elements in a list of IWebElements
        /// </summary>
        /// <returns></returns>
        public static List<IWebElement> GetAllPageElements()
        {
            return SeleniumUtil.GetAllElements();
        }

        /// <summary>
        /// return the number of elements found in total!
        /// </summary>
        /// <returns></returns>
        public static int HowManyPageElementsReturned()
        {
            return SeleniumUtil.GetAllElements().Count;   
        }

        /// <summary>
        /// Get all elements of type/tag
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static List<IWebElement> GetAllPageElementsOfType(string type)
        {
            var locator = By.XPath($"//{type}");
            var topElement = SeleniumUtil.GetTopElement();
            if (topElement == null)
            {
                return new List<IWebElement>();  // can never be null but c# wants a check!
            }
            return SeleniumUtil.GetElementsUnder(topElement, locator);
        }
    }
}
