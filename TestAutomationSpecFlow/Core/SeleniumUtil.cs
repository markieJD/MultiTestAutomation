﻿
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Core.Transformations.PageModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Drawing;

namespace Core
{
    public static class SeleniumUtil
    {
        public static IWebDriver? webDriver = null;
        public static WindowsDriver<WindowsElement>? winDriver = null;
        public static string outputFolder = @"\AppSpecFlow\TestOutput\";
        public static string compareFolder = @"\AppSpecFlow\TestCompare\";
        public static string failedFindElement = "Failed to find element!";
        public static int test = TargetConfiguration.Configuration.Debug;
        public static int DefaultPositiveTimeOut = TargetConfiguration.Configuration.PositiveTimeout;
        public static string currentDirectory = Directory.GetCurrentDirectory();

        /// <summary>
        /// Get the Alert Element displayed on a web site
        /// </summary>
        /// <returns>IAlert if found, null if not found</returns>
        private static IAlert? GetAlert()
        {
            DebugOutput.Log($"Sel - GetAlert");
            IAlert? alert = null;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return alert;
            try
            {
                if (webDriver == null)
                {
                    return null;
                }
                Thread.Sleep(500);
                alert = webDriver.SwitchTo().Alert();
                return alert;
            }
            catch
            {
                DebugOutput.Log($"Failed to Navigate To Alert");
            }
            return alert;
        }

        public static By GetParentXPathLocator()
        {
            return By.XPath($"./..");
        }

        public static bool AlertLogin(string username, string password)
        {
            DebugOutput.Log($"Sel - AlertLogin {username} {password}");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            IAlert? alert_win = GetAlert();
            if (alert_win == null)
            {
                DebugOutput.Log($"FAILED TO FIND ALERT WINDOW!");
                try
                {
                    if (webDriver == null) return success;
                    var x = webDriver.WindowHandles;
                    DebugOutput.Log($"We have {x.Count} windows currently available to us!");
                    var elements = webDriver.FindElements(By.XPath($"//*"));
                    DebugOutput.Log($"AND we have {elements.Count} ELEMENTS TO PLAY WITH");
                    if (!EnterText(elements[0], "JUST WORK!")) DebugOutput.Log($"NOPE NOTHING I CAN DO HERE!");
                }
                catch
                {
                    DebugOutput.Log("FFS!");
                }
                return success;
            }
            try
            {
                alert_win.SetAuthenticationCredentials(username, password);
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failure entering username and password {username}");
                return success;
            }
        }

        /// <summary>
        /// Get Alert and Send Keys
        /// </summary>
        /// <param name="text"></param>
        /// <returns>true if successful</returns>
        public static bool AlertInput(string text)
        {
            DebugOutput.Log($"Sel - AlterInput {text}");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            IAlert? alert_win = GetAlert();
            if (alert_win == null) return success;
            try
            {
                alert_win.SendKeys(text);
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed to send {text} to Alert");
            }
            return success;
        }

        /// <summary>
        /// Get an Alert and confirm the message 
        /// </summary>
        /// <param name="alertMessage"></param>
        /// <returns>true if alert message is displayed</returns>
        public static bool AlertDisplayed(string alertMessage)
        {
            DebugOutput.Log($"Sel - AlertDisplayed {alertMessage}");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            IAlert? alert_win = GetAlert();
            if (alert_win == null) return success;
            if (alertMessage.ToLower() == alert_win.Text.ToLower())
            {
                success = true;
                return success;
            }
            DebugOutput.Log($"We have an alert, but Not the message, we got '{alert_win.Text.ToLower()}' was expecting '{alertMessage.ToLower()}'");
            if (alert_win.Text.ToLower().Contains(alertMessage.ToLower()))
            {
                DebugOutput.Log($"message is found in message!");
                success = true;
                return success;
            }
            return success;
        }

        /// <summary>
        /// Get Alert, then dismiss it (cancel)
        /// </summary>
        /// <returns>true if alert message dismissed</returns>
        public static bool AlertClickCancel()
        {
            DebugOutput.Log($"Sel - AlertClickAccept");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            IAlert? alert_win = GetAlert();
            if (alert_win == null) return success;
            try
            {
                alert_win.Dismiss();
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed on the accept of alert");
            }
            return success;
        }

        /// <summary>
        /// Get Alert Message and Accept
        /// </summary>
        /// <returns>true if alert message accepted</returns>
        public static bool AlertClickAccept()
        {
            DebugOutput.Log($"Sel - AlertClickAccept");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            IAlert? alert_win = GetAlert();
            if (alert_win == null) return success;
            try
            {
                alert_win.Accept();
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed on the accept of alert");
            }
            return success;
        }

        /// <summary>
        /// Pass in Element, move to it then click
        /// We use movetoelement rather than just element.click as there maybe an element on top which blocks a human from clicking on it
        /// It moves to the centre point of the element
        /// </summary>
        /// <param name="element"></param>
        /// <returns>true if successfully clicked</returns>
        public static bool Click(IWebElement element)
        {
            DebugOutput.Log($"Sel - Click {element}");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.MoveToElement(element);
                action.Build();
                action.Perform();
                action.Click();
                action.Build();
                action.Perform();
                success = true;
                return success;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to click {element} {ex}");
                return success;
            }
        }

        /// <summary>
        /// Pass in an element, move to its centre, then moves x and y from that centre point, then clicks
        /// </summary>
        /// <param name="element"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>true if successfully clicked</returns>
        public static bool ClickCoordinates(IWebElement element, int x = 0, int y = 0)
        {
            DebugOutput.Log($"Sel - ClickCoordinates {element} {x} {y}");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.MoveToElement(element);
                action.MoveByOffset(x, y);
                action.Click();
                action.Build();
                action.Perform();
                success = true;
                return success;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to click {element} {ex}");
                return success;
            }
        }

        /// <summary>
        /// Pass in element, move to its centre then double click
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns true if successful</returns>
        public static bool DoubleClick(IWebElement element)
        {
            DebugOutput.Log($"Sel - Click {element}");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.MoveToElement(element);
                action.DoubleClick();
                action.Build();
                action.Perform();
                success = true;
                return success;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to click {element} {ex}");
                return success;
            }
        }

        /// <summary>
        /// Pass in element, move to its centre then right clicks
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns true if successful</returns>
        public static bool RightClick(IWebElement element)
        {
            DebugOutput.Log($"Sel - Click {element}");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.MoveToElement(element);
                action.ContextClick();
                action.Build();
                action.Perform();
                success = true;
                return success;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to click {element} {ex}");
                return success;
            }
        }

        public static IWebElement? GetNthElementBy(By locator, string nTh)
        {
            DebugOutput.Log($"Sel - ClickNthElementBy {locator}");
            var elements = GetElements(locator);
            if (elements == null) return null;
            {
                if (elements.Count == 0) return null;
            }
            DebugOutput.Log($"We have {elements.Count} elements of locator {locator}");
            switch (nTh.ToLower())
            {
                default:
                case "1st":
                {
                    try
                    {
                        return elements[0];
                    }
                    catch
                    {
                        DebugOutput.Log($"Failed to return the {nTh} element - it may not exist with {locator}");
                        return null;
                    }
                }
                case "2nd":
                {
                    try
                    {
                        return elements[1];
                    }
                    catch
                    {
                        DebugOutput.Log($"Failed to return the {nTh} element - it may not exist with {locator}");
                        return null;
                    }
                }
                case "3rd":
                {
                    try
                    {
                        return elements[2];
                    }
                    catch
                    {
                        DebugOutput.Log($"Failed to return the {nTh} element - it may not exist with {locator}");
                        return null;
                    }
                }
            }

        }

        public static bool ClearThenEnterText(IWebElement element, string text, string key = "")
        {
            DebugOutput.Log($"Sel - ClearThenEnterText {element} {text} {key}");
            Click(element);
            SendKey(element, "clear");
            return EnterText(element, text, key);
        }

        /// <summary>
        /// Pass in an element, send the keys in text, and then press keyboard key
        /// Expected keys are 'enter', 'tab'
        /// </summary>
        /// <param name="element"></param>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <returns>returns true if successful</returns>
        public static bool EnterText(IWebElement element, string text, string key = "")
        {
            DebugOutput.Log($"Sel - EnterText {element} {text} {key}");
            text = StringValues.TextReplacementService(text);
            DebugOutput.Log($"NEW TEXT = '{text}'");
            try
            {
                element.SendKeys(text);
                if (string.IsNullOrEmpty(key)) return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to Enter Text Directly {element} {text}");

                return false;
            }
            return SendKey(element, key);
        }

        /// <summary>
        /// Pass in 2 elements, click and hold centre point of the first element, them move to centre of the second element and release
        /// </summary>
        /// <param name="elementA"></param>
        /// <param name="elementB"></param>
        /// <returns>returns true if successful</returns>
        public static bool DragElementToElement(IWebElement elementA, IWebElement elementB)
        {
            DebugOutput.Log($"Sel - DragElementToElement {elementA} {elementB}");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                DebugOutput.Log($"Starting action");
                var dragAndDrop = action.ClickAndHold(elementA).MoveToElement(elementB, 10, 10).Release(elementB).Build();
                dragAndDrop.Perform();
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to drag!");
                return false;
            }
        }
        
        /// <summary>
        /// An element can be loaded but not displayed - Selenium gets the element and reports instantly on is visabilit!
        /// In the real world, we need to wait for this to display!
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns true if displayed, false if timed out postive timeout</returns>
        public static bool IsDisplayed(IWebElement element, bool not = false)
        {
            DebugOutput.Log($"Sel - IsDisplayed {element} ");
            if (element == null) return false;
            int counter = 0;
            var timeOut = TargetConfiguration.Configuration.NegativeTimeout;
            if (not)
            {
                try
                {
                    DebugOutput.Log($"NOT displayed");
                    timeOut = TargetConfiguration.Configuration.NegativeTimeout;
                    while (element.Displayed)
                    {
                        counter++;
                        Thread.Sleep(1000);
                        if (counter > timeOut)
                        {
                            DebugOutput.Log($"WE HAVE the confirmation element! but its not displayed! waited {counter} seconds for it to appear!");
                            return true;
                        }
                    }
                    DebugOutput.Log($"Element was NOT displayed {counter} seconds AFTER the element loaded!");
                    return false;
                }
                catch
                {
                    DebugOutput.Log($"The reaons for try catch, is the element maybe GONE! what is called a stale element, it existed once, but no more! Querying a stale element restults in an exception - NEVER want those!");
                    DebugOutput.Log($"If its stale, it cant be displayed!");
                    return false;
                }
            } 
            if (!not) timeOut = TargetConfiguration.Configuration.PositiveTimeout;
            while (!element.Displayed)
            {
                counter++;
                Thread.Sleep(1000);
                if (counter > timeOut)
                {
                    DebugOutput.Log($"WE HAVE the confirmation element! but its not displayed! waited {counter} seconds for it to appear!");
                    return false;
                }
            }
            DebugOutput.Log($"Element was displayed {counter} seconds AFTER the element loaded!");
            return true;
        }

        /// <summary>
        /// Pass in an element, and click on centre point of element, and hold
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns true if successful</returns>
        public static bool MouseDown(IWebElement element)
        {
            DebugOutput.Log($"Sel - MouseDown {element} ");
            bool success = false;
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.ClickAndHold(element);
                action.Perform();
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed to click and hold / mouse down!");
                return success;
            }
        }

        /// <summary>
        /// Pass in element and move mouse over elements centre point
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns ture if successful</returns>
        public static bool MoveToElement(IWebElement element, int x = 0, int y = 0)
        {
            DebugOutput.Log($"Sel - MoveToElement {element} {x} {y} ");
            bool success = false;
            if (element == null)
            {
                DebugOutput.Log($"No element!");
                return success;
            }    
            var action = GetActions();
            if (action == null) return success;
            try
            {
                action.MoveToElement(element);
                action.Perform();
                DebugOutput.Log($"First move!");
                if (y != 0)
                {
                    y = y + 2;
                    if (!ScrollDownPage(element, y)) return success;
                }    
                DebugOutput.Log($"MOVED!");
                success = true;
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to move!");
                return success;
            }
        }

        private static bool ScrollDownPage(IWebElement elem, int y = 0)
        {
            DebugOutput.Log($"ScrollDownPage");
            try
            {
                if (webDriver == null) return false;
                IJavaScriptExecutor js = (IJavaScriptExecutor)webDriver;
                string script = $"window.scrollBy(0,{y})";
                js.ExecuteScript(script);
                //js.ExecuteScript("arguments[0].scrollIntoView(false)", elem);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Not here!");
                return false;
            }
        }

        /// <summary>
        /// Pass in an element, moves the slider to 0, then moves it to offset% of slider
        /// </summary>
        /// <param name="slider"></param>
        /// <param name="offSet"></param>
        /// <returns>returns true if movement of the slider is successful</returns>
        public static bool MoveSliderElement(IWebElement slider, int offSet)
        {
            DebugOutput.Log($"Sel - MoveSliderElement {slider} {offSet}");
            bool success = false;
            if (offSet > 100)
            {
                DebugOutput.Log($"It only handles 100%");
                return success;
            }
            var width = slider.Size.Width;
            DebugOutput.Log($"Width of Element {width}");
            //The default will be 50% as moveToElement will take it to the middle of the element
            var action = GetActions();
            if (action == null) return success;
            try
            {
                DebugOutput.Log($"0 Click");
                action.MoveToElement(slider, 0, 0);
                action.Click();
                action.Build().Perform();
                Thread.Sleep(100);
            }
            catch
            {
                DebugOutput.Log($"Failed to zero slider");
                return success;
            }
            //onePercent is the width split into 100 parts (50 to the left, 50 to the right)
            float onePercent = (float)width / 100;
            DebugOutput.Log($"One Percent =  {onePercent}");
            float floatMovement = onePercent * offSet;
            DebugOutput.Log($"Will be clicking on of Element FLOAT {floatMovement}");
            int movement = (int)Math.Round(floatMovement);
            try
            {
                action.MoveToElement(slider, movement, 0);
                action.Click();
                action.Build().Perform();
                Thread.Sleep(100);
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed to slide!");
            }
            return false;
        }

        /// <summary>
        /// Pass in a url to the webdriver and automaticlly navigate to url
        /// beaware some urls will forward you on!
        /// </summary>
        /// <param name="url"></param>
        /// <returns>returns true if able to send url to browser, returns false if unable to pass url to browser, or if not using a browser</returns>
        public static bool NavigateToURL(string url)
        {
            DebugOutput.Log($"Sel - NavigateToURL {url} ");
            var appType = TargetConfiguration.Configuration.ApplicationType.ToLower();
            if (appType == "web")
            {
                try
                {
                    if (webDriver == null)
                    {
                        DebugOutput.Log($"We do not have a web driver!");
                        return false;
                    }
                    webDriver.Navigate().GoToUrl(url);
                    return true;
                }
                catch
                {
                    DebugOutput.Log($"Failed to Navigate GoToUrl");
                    return false;
                }
            }
            if (appType == "windows")
            {
                DebugOutput.Log($"It is a windows app?  If you need to navigate to a URL you will need to manually enter this in the app!");
                return false;
            }
            return false;
        }

        /// <summary>
        /// Navigate a URL that requires a username and password via a popup
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="url"></param>
        /// <returns>returns true if successful</returns>
        public static bool NavigateToURLAsUserWithPassword(string userName, string password, string url)
        {
            DebugOutput.Log($"Sel - NavigateToURL {url} ");
            bool success = false;
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() != "web") return success;
            url = url.ToLower();
            var theWebAddress = url.Replace("http://", "");
            theWebAddress = url.Replace("https://", "");
            var combinedurl = "http://" + userName + ":" + password + "@" + theWebAddress;
            DebugOutput.Log($"Navigation is equal to {combinedurl}");
            try
            {
                if (webDriver == null)
                {
                    DebugOutput.Log($"We do not have a web driver!");
                    return success;
                }
                webDriver.Navigate().GoToUrl(combinedurl);
                success = true;
                return success;
            }
            catch
            {
                DebugOutput.Log($"Failed navigation");
                return success;
            }
        }

        public static Bitmap? GetImageOfElementOnPage(IWebElement element)
        {
            DebugOutput.Log($"Proc - GetImageOfElementOnPage{element}");
            Bitmap? img;
            try
            {
                if (webDriver == null)
                {
                    DebugOutput.Log($"This is web only at the moment!");
                    return null;
                }
                img = GetElementScreenShot(element);
                return img;
            }
            catch
            {
                DebugOutput.Log($"FAILED TO IMAGE {element}");
            }
            return null;
        }

        /// <summary>
        /// Takes a screengrab of the page displayed
        /// saves it as pageName.png supplied
        /// </summary>
        /// <param name="pageName"></param>
        /// <returns>returns true if successful</returns>
        public static bool GetCurrentPageImage(string pageName)
        {
            DebugOutput.Log($"Proc - GetCurrentPageImage {pageName}");
            try
            {
                if (webDriver == null)
                {
                    DebugOutput.Log($"We do not have a web driver!");
                    return false;
                }
                Screenshot sc = ((ITakesScreenshot)webDriver).GetScreenshot();
                var img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap; DebugOutput.Log($"Current directory = {currentDirectory}");
                var fullfileName = currentDirectory + outputFolder + pageName + ".png";
                DebugOutput.Log($"putting  file {fullfileName}");
                if (img == null)
                {
                    DebugOutput.Log($"We failed in the image from stream");
                    return false;
                }
                img.Save(fullfileName, System.Drawing.Imaging.ImageFormat.Png);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to take page image");
                return false;
            }
        }

        /// <summary>
        /// Takes a screengrab of the page displayed
        /// but unlike GetCurrentPageImage it does not care what page is displayed
        /// This is more useful for fails
        /// </summary>
        /// <returns>returns true if succesful</returns>
        public static bool ScreenShotPage()
        {
            DebugOutput.Log($"Proc - ScreenShotPage");
            try
            {
                if (webDriver == null)
                {
                    DebugOutput.Log($"We do not have a web driver!");
                    return false;
                }
                Screenshot sc = ((ITakesScreenshot)webDriver).GetScreenshot();
                var img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap; DebugOutput.Log($"Current directory = {currentDirectory}");
                var fullfileName = currentDirectory + outputFolder + "currentPage.png";
                DebugOutput.Log($"putting  file {fullfileName}");
                if (img == null)
                {
                    DebugOutput.Log($"We failed in the image from stream");
                    return false;
                }
                img.Save(fullfileName, System.Drawing.Imaging.ImageFormat.Png);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to take page image");
                return false;
            }
        }

       /// <summary>
       /// Pass in an element and take a photo of it!
       /// saves it as elementName.png
       /// </summary>
       /// <param name="element"></param>
       /// <param name="elementName"></param>
       /// <returns>returns true if successful in finding and taking image of element</returns>
        public static bool ScreenShotElement(IWebElement element, string elementName, string pageName = "", string outputDir = "")
        {
            DebugOutput.Log($"Proc - ScreenShotElement {element} {elementName} {pageName} {outputDir}"); try
            {
                if (webDriver == null && TargetConfiguration.Configuration.ApplicationType.ToLower() == "web")
                {
                    DebugOutput.Log($"We do not have a web driver!");
                    return false;
                }
                if (winDriver == null && TargetConfiguration.Configuration.ApplicationType.ToLower() == "windows")
                {
                    DebugOutput.Log($"We do not have a WINDOWS driver!");
                    return false;
                }
                var img = GetElementScreenShot(element);
                if (img == null)
                {
                    DebugOutput.Log($"Can not save screen shot, if no screen shot!");
                    return false;
                }
                SetCurrentDirectoryToTop();
                DebugOutput.Log($"Current directory = {currentDirectory}");
                var fullFileName = "";
                if (outputDir == "")
                {
                    fullFileName = currentDirectory + outputFolder + $"{elementName}.png";
                }
                else
                {
                    var project = TargetConfiguration.Configuration.ProjectName;
                    fullFileName = currentDirectory + @"\AppTargets\Forms\" + project + @"\ElementImages\" + elementName + "_" + pageName + ".png";
                }
                DebugOutput.Log($"putting  file {fullFileName}");
                if (File.Exists(fullFileName))
                {
                    try
                    {
                        File.Delete(fullFileName);
                    }
                    catch
                    {
                        DebugOutput.Log($"failed to delete - will hopefully overwrite");
                    }
                }
                DebugOutput.Log($"Clean and ready to save {img.Size.Width} x {img.Size.Height}");
                img.Save(fullFileName, System.Drawing.Imaging.ImageFormat.Png);
                DebugOutput.Log($"Saved file");
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the current working directory to the top level of the build
        /// When running in debug the working directory can be different, this makes sure that it is always working in correct top directory
        /// </summary>
        /// <returns>returns true if able to navigate to top directory</returns>
        public static bool SetCurrentDirectoryToTop()
        {
            DebugOutput.Log($"Proc - SetCurrentDirectoryToTop");
            if (currentDirectory.Contains("AppSpecFlow") || 
                currentDirectory.Contains("AppTargets") || 
                currentDirectory.Contains("Core") ||
                    currentDirectory.Contains("Generic"))
            {
                string? tempCurrentDirectory = Path.GetDirectoryName(currentDirectory);
                DebugOutput.Log($"temp current directory = {tempCurrentDirectory}");
                string? oneUpDirectory = Path.GetDirectoryName(tempCurrentDirectory);
                DebugOutput.Log($"one up directory = {tempCurrentDirectory}");
                if (oneUpDirectory != null)
                {
                    currentDirectory = oneUpDirectory;
                }
            }
            else
            {
                DebugOutput.Log($"We at the top! {currentDirectory}");
                Directory.SetCurrentDirectory(currentDirectory);
                return true;
            }
            DebugOutput.Log($"We need to recheck");
            SetCurrentDirectoryToTop(); 
            return false;
        }

        public static int GetWidthOfElement(IWebElement element)
        {
            DebugOutput.Log($"sel - GetWidthOfElement {element} ");
            return element.Size.Width;
        }

        public static bool ListOfElementNamesContains(string partualName)
        {
            DebugOutput.Log($"sel - ListOfElementNamesContains {partualName} ");
            List<string> elementNames = GetAllEmenetsNamesAsList();
            foreach(string elementName in elementNames)
            {
                DebugOutput.Log($"NAME = {elementName}");   
                if (elementName.Contains(partualName)) return true;
            }
            DebugOutput.Log($"Looked through {elementNames.Count} element names - did not find!");
            return false;
        }

        private static List<string> GetAllEmenetsNamesAsList()
        {
            DebugOutput.Log($"sel - GetAllEmenetsNames ");
            var elements = GetAllElements();
            List<string> result = new List<string>();
            foreach (var element in elements)
            {
                var name = GetElementAttributeValue(element, "Name");
                if (name != null) result.Add(name);
            }
            return result;
        }

        /// <summary>
        /// Create a bitmap image of an element passed in
        /// not the same as ScreenShotElement as that creates and saves the image, this just creates it
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns the bitmap, if fails returns null</returns>
        private static Bitmap? GetElementScreenShot(IWebElement element)
        {
            DebugOutput.Log($"sel - GetElementScreenShot {element}");
            Bitmap? img = null;
            try
            {
                if (TargetConfiguration.Configuration.ApplicationType.ToLower() == "web")
                {
                    if (webDriver == null) return null;
                    var driver = webDriver;
                    Screenshot sc = ((ITakesScreenshot)driver).GetScreenshot();
                    img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap;
                    if (img == null)
                    {
                        DebugOutput.Log($"Issue grabbing image Bitmap!");
                        return null;
                    }
                }
                else
                {
                    if (winDriver == null) return null;
                    var driver = winDriver;
                    Screenshot sc = ((ITakesScreenshot)driver).GetScreenshot();
                    img = Image.FromStream(new MemoryStream(sc.AsByteArray)) as Bitmap;
                    if (img == null)
                    {
                        DebugOutput.Log($"Issue grabbing image Bitmap!");
                        return null;
                    }
                }
                DebugOutput.Log($"We have our image {img}");
                var rect = new Rectangle(element.Location, element.Size);
                DebugOutput.Log($"We have a rect {element} {element.Size}");
                return img.Clone(rect, img.PixelFormat);
            }
            catch
            {
                DebugOutput.Log($"Failed to capture image!");
                return null;
            }
        }

        /// <summary>
        /// Set the windows size
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>returns true if successful</returns>
        public static bool SetWindowSize(int x, int y)
        {
            DebugOutput.Log($"Sel - SetWindowSize {x}x{y}");
            var appType = TargetConfiguration.Configuration.ApplicationType.ToLower();
            if (appType == "web")
            {
                try
                {
                    if (webDriver == null) return false;
                    webDriver.Manage().Window.Size = new System.Drawing.Size(x, y);
                    return true;
                }
                catch
                {
                    DebugOutput.Log($"Failed to set it!");
                    return false;
                }
            }
            if (appType == "windows")
            {
                if (winDriver == null) return false;
                try
                {
                    winDriver.Manage().Window.Size = new System.Drawing.Size(x, y);
                    return true;
                }
                catch (Exception ex)
                {
                    DebugOutput.Log($"{ex}");
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Send single or multiple keypresses to an element, by name
        /// default just sends the key(s) supplied
        /// down, down arrow = Keys.Down
        /// clear = Ctl+A then delete
        /// close = Alt+F4
        /// enter = Keys.Return
        /// escape = Keys.Escape
        /// tab = Keys.Tab
        /// </summary>
        /// <param name="element"></param>
        /// <param name="key"></param>
        /// <returns>returns true if successful</returns>
        public static bool SendKey(IWebElement element, string key)
        {
            DebugOutput.Log($"Sel - SendKey {element} {key}");
            if (string.IsNullOrEmpty(key)) return false;
            key = key.ToLower();    
            try
            {
                switch (key)
                {
                    default:
                        {
                            element.SendKeys(key);
                            return true;
                        }
                    case "down":
                    case "down arrow":
                        {
                            element.SendKeys(Keys.Down);
                            return true;
                        }
                    case "clear":
                        {
                            element.SendKeys(Keys.Control + "a");
                            element.SendKeys(Keys.Delete);
                            return true;
                        }
                    case "close":
                        {
                            element.SendKeys(Keys.Alt + Keys.F4);
                            return true;
                        }
                    case "enter":
                    case "return":
                        {
                            element.SendKeys(Keys.Return);
                            return true;
                        }
                    case "escape":
                        {
                            element.SendKeys(Keys.Escape);
                            return true;
                        }
                    case "tab":
                        {
                            element.SendKeys(Keys.Tab);
                            return true;
                        }
                }
            }
            catch
            {
                DebugOutput.Log($"problem sending key!");
                return false;
            }
        }

        /// <summary>
        /// Supply an element, is it enabled (i.e. can it be interacted with)
        /// </summary>
        /// <param name="element"></param>
        /// <returns>return true if the element can be interacted with, false if no interaction is possible</returns>
        public static bool IsEnabled(IWebElement element)
        {
            DebugOutput.Log($"Sel - IsEnabled {element} ");
            try
            {
                return element.Enabled;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Had a failed returning elements Enabled flag! {ex}");
                return false;
            }
        }

        /// <summary>
        /// Supply an element, is it selected?  Normally used for checkboxes and radio buttons
        /// </summary>
        /// <param name="element"></param>
        /// <returns>return true if the element is selected, false if not</returns>
        public static bool IsSelected(IWebElement element)
        {
            DebugOutput.Log($"Sel - IsSelected {element} ");
            try
            {
                DebugOutput.Log($"IT IS {element.Selected}");
                var select = element.Selected;
                DebugOutput.Log($"WE have a the selected element {select}");
                if (select) return select;
                DebugOutput.Log($"Still using class, can we check the text?");
                var classTitle = SeleniumUtil.GetElementAttributeValue(element, "class");
                DebugOutput.Log($"We have {classTitle} which may say ACTIVE?");
                if (classTitle.ToLower().Contains("active")) return true;
                DebugOutput.Log($"Problem is it is 'active' not selected! so neg to the selected");
                if (element.GetAttribute("class").Contains("active"))
                {
                    DebugOutput.Log($"We can check the class and it contains the class active!");
                    return true;
                }
                DebugOutput.Log($"If we have a space in the class name, we don't see the active part! so CSS attempt");
                var cSSText = element.GetCssValue("class");
                DebugOutput.Log($"CSS has given us {cSSText}");
                if (cSSText.ToLower().Contains("active")) return true;
                DebugOutput.Log($"Tried every way but loose! I wonder if it has a child that has the actual active flag?");
                return ChildIsSelected(element);
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Had a failed returning elements Selected flag! {ex}");
                return false;
            }
        }

        private static bool ChildIsSelected(IWebElement parentElement)
        {
            DebugOutput.Log($"Sel - ChildIsSelected {parentElement} ");
            try
            {
                var allChildElements = SeleniumUtil.GetAllElementsUnderElement(parentElement);
                foreach (var childElement in allChildElements)
                {
                    var select = childElement.Selected;
                    DebugOutput.Log($"WE have a the selected element {select}");
                    if (select) return select;
                    DebugOutput.Log($"Still using class, can we check the text?");
                    var classTitle = SeleniumUtil.GetElementAttributeValue(childElement, "class");
                    DebugOutput.Log($"We have {classTitle} which may say ACTIVE?");
                    if (classTitle.ToLower().Contains("active")) return true;
                }
                DebugOutput.Log($"Still no active flag - so it must not be active!");
                return false;
            }
            catch
            {
                DebugOutput.Log($"We have hit an issue when checking on Child Elements if they are selected!");
            }
            return false;
        }

        /// <summary>
        /// Supply an element, and return its parent element
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns the parent element of the supplied element, or null if top element supplied</returns>
        public static IWebElement? GetElementParent(IWebElement element)
        {
            //DebugOutput.Log($"Sel - GetElementParent {element} ");
            try
            {
                return element.FindElement(By.XPath(".."));
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// By using a locator, find and return an element within the timeout supplied,
        /// if no timeout supplied or timeout 0, will use app default
        /// This is used by web and windows applications
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns>returns the element found, or null if no element found</returns>
        public static IWebElement? GetElement(By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetElement {locator} {timeout}");
            var appType = TargetConfiguration.Configuration.ApplicationType.ToLower();
            if (appType == "web")
            {
                return GetWebElement(locator, timeout);
            }
            if (appType == "windows")
            {
                return GetWindowsElement(locator, timeout);
            }
            return null;
        }

        public static string? GetElementIDAsString(IWebElement element)
        {
            return element.ToString();
        }

        private static List<IWebElement>? GetWindowsElements(By locator, int timeout = 0)
        {
            DebugOutput.Log($"GetWindowsElements {locator} Using {timeout}");
            timeout = GetTimeout(timeout);
            try
            {
                if (winDriver == null) return null;
                var list = winDriver.FindElements(locator);
                var listOfElements = new List<IWebElement>();
                foreach( var singleElement in list)
                {
                    listOfElements.Add(singleElement);
                }
                return listOfElements;
            }
            catch
            {                
                DebugOutput.Log("FAILED GET ANY ELEMENTs Sucessfully");
                string locatorXPath = locator.ToString();
                if (locatorXPath.Contains("By.Id: "))
                {
                    locatorXPath = locatorXPath.Replace("By.Id: ", "");
                    DebugOutput.Log($"Using Accesibiluy = {locatorXPath}");
                    try
                    {
                        if (winDriver == null) return null;
                        var list = winDriver.FindElementsByAccessibilityId(locatorXPath);
                        var listOfElements = new List<IWebElement>();
                        foreach( var singleElement in list)
                        {
                            listOfElements.Add(singleElement);
                        }
                        return listOfElements;
                    }
                    catch
                    {
                        DebugOutput.Log($"Not even accessibility");
                    }
                }
                DebugOutput.Log($"LOCATOR = {locator}");
            }
            return null;
        }

        /// <summary>
        /// By using a locator, find and return an element within the timeout supplied,
        /// if no timeout supplied or timeout 0, will use app default
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns>returns element if found, null if not</returns>
        private static IWebElement? GetWindowsElement(By locator, int timeout = 0)
        {
            DebugOutput.Log($"GetWindowsElement {locator} Using default POSITIVE TIMEOUT {timeout}");
            timeout = GetTimeout(timeout);
            try
            {
                var wait = new WebDriverWait(winDriver, TimeSpan.FromSeconds(timeout));
                return wait.Until(drv => drv.FindElement(locator));
            }
            catch
            {
                DebugOutput.Log("FAILED GET ELEMENT");
                string locatorXPath = locator.ToString();
                if (locatorXPath.Contains("By.Id: "))
                {
                    locatorXPath = locatorXPath.Replace("By.Id: ", "");
                    DebugOutput.Log($"Using Accesibiluy = {locatorXPath}");
                    try
                    {
                        if (winDriver == null) return null;
                        return winDriver.FindElementByAccessibilityId(locatorXPath);
                    }
                    catch
                    {
                        DebugOutput.Log($"Not even accessibility");
                    }
                }
                DebugOutput.Log($"LOCATOR = {locator}");
                return null;
            }
        }

        /// <summary>
        /// By using a locator, find and return an element within the timeout supplied,
        /// if no timeout supplied or timeout 0, will use app default
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        private static IWebElement? GetWebElement(By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetWebElement {locator} {timeout}");
            if (webDriver == null) return null;
            timeout= GetTimeout(timeout);
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout));
                return wait.Until(drv => drv.FindElement(locator));
            }
            catch
            {
                DebugOutput.Log("FAILED GET ELEMENT");
                return null;
            }
        }

        public static int GetPageWidth()
        {
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() == "web")
            {
                if (webDriver == null) return 0;
                return webDriver.Manage().Window.Size.Width;
            }
            if (winDriver == null) return 0;
            return winDriver.Manage().Window.Size.Width;
        }

        public static int GetPageHeight()
        {
            if (TargetConfiguration.Configuration.ApplicationType.ToLower() == "web")
            {
                if (webDriver == null) return 0;
                return webDriver.Manage().Window.Size.Height;
            }
            if (winDriver == null) return 0;
            return winDriver.Manage().Window.Size.Height;
        }

        public static int GetTimeout(int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetTimeout {timeout}");
            if (timeout == 0) timeout = TargetConfiguration.Configuration.PositiveTimeout;
            timeout *= TargetConfiguration.Configuration.TimeoutMultiplie;
            DebugOutput.Log($"Timeout set to {timeout}");
            return timeout;
        }

        /// <summary>
        /// Find the very top element of the driver
        /// in a PC this COULD be above the application in question
        /// </summary>
        /// <returns>returns the top element</returns>
        public static IWebElement? GetTopElement()
        {
            DebugOutput.Log("FIND THE TOP ELEMENT");
            var locator = By.XPath("//*");
            var element = GetElement(locator);
            DebugOutput.Log($" WE HAVE THE ELEMENT = {element}");
            return element;
        }

        /// <summary>
        /// Get all the elements and put them in a list
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns>returns a list of all elements found</returns>
        public static List<IWebElement> GetAllElements(int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetAllElements (plural) {timeout}");
            var element = GetTopElement();
            var locator = By.XPath("//*");
            List<IWebElement> elements = new List<IWebElement>();
            if (element == null) return elements;
            elements = GetElementsUnder(element, locator);
            DebugOutput.Log($"WE have {elements.Count} elements IN TOTAL");
            return elements;
        }

        public static IWebElement? GetAllInputElementsByText(string text)
        {
            DebugOutput.Log($"Sel - GetAllInputElementsByText (plural) {text}");
            var locator = By.TagName("input");
            var allInputElements = GetElements(locator, 1);
            if (allInputElements == null) return null;
            DebugOutput.Log($"We have {allInputElements.Count} INPUT elements found!");
            foreach (var element in allInputElements)
            {
                var elementText = GetElementText(element);
                if (elementText.ToLower() == text.ToLower())
                {
                    DebugOutput.Log($"FOUND AN INPUT ELEMENT USING THE TEXT {text} we return the first found!");
                    return element;
                }
            }
            DebugOutput.Log($"Failed to find any element with that text!");
            return null;
        }

        public static IWebElement? GetInputElementByParentText(string text)
        {
            DebugOutput.Log($"Sel - GetInputElementByParentText (plural) {text}");
            var locator = By.TagName("input");
            var allInputElements = GetElements(locator, 1);
            if (allInputElements == null) return null;
            DebugOutput.Log($"We have {allInputElements.Count} INPUT elements found! Which fit for our location");
            foreach (var element in allInputElements)
            {
                var parentOfElement = GetElementParent(element);
                if (parentOfElement != null)
                {
                    var elementText = GetElementText(parentOfElement);
                    if (elementText.ToLower() == text.ToLower())
                    {
                        DebugOutput.Log($"FOUND AN INPUT ELEMENT USING THE TEXT OF ITS PARENT! {text} we return the first found!");
                        return element;
                    }
                }
            }
            DebugOutput.Log($"Failed to find any element with that text even the inputs parent!!");
            return null;
        }

        public static List<IWebElement> GetAllElementsUnderElement(IWebElement parent)
        {
            DebugOutput.Log($"Sel - GetAllElementsUnderElement (plurals) {parent}");
            var locator = By.XPath($"//*");
            var allElements = GetElementsUnder(parent, locator);
            DebugOutput.Log($"WE have {allElements.Count} elements IN TOTAL UNDER THE PARENT {parent}");
            return allElements;
        }

        /// <summary>
        /// Get all the elements found by using a locator
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns>returns list of elements</returns>
        public static List<IWebElement>? GetElements(By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetElements (plural) {locator} {timeout}");
            var elementList = new List<IWebElement>();
            if (winDriver != null)
            {
                return GetWindowsElements(locator, timeout);
            }
            timeout = GetTimeout(timeout);
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout));
                var elements = wait.Until(drv => drv.FindElements(locator));
                foreach (var element in elements)
                {
                    elementList.Add(element);
                }
                DebugOutput.Log($"We have {elementList.Count} elements found");
                return elementList;
            }
            catch
            {
                DebugOutput.Log("FAILED GET ELEMENTSSSS");
                return elementList;
            }
        }

        /// <summary>
        /// Get first element using a locator found UNDER a supplied element - Web ONLY
        /// </summary>
        /// <param name="parentElement"></param>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns>returns element found, or null if not found</returns>
        public static IWebElement? GetElementUnderElement(IWebElement parentElement, By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetElementUnderElement {parentElement} {locator} {timeout}");
            if (webDriver == null) return null;
            timeout = GetTimeout(timeout);
            try
            {
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(timeout));
                return wait.Until(driv => parentElement.FindElement(locator));
            }
            catch
            {
                DebugOutput.Log("failed to find element");
                return null;
            }
        }

        /// <summary>
        /// Get list of elements using a locator found UNDER a supplied element
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static List<IWebElement> GetWebElementsUnder(IWebElement parent, By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetWebElementsUnder {parent} {locator} {timeout}");

            var elementList = new List<IWebElement>();

            var elementsQuick = parent.FindElements(locator);
            foreach(var element in elementsQuick)
            {
                DebugOutput.Log($"adding - {element}");
                elementList.Add(element);
            }    
            DebugOutput.Log($"returning {elementList.Count()} elements found under {parent} using {locator}");
            return elementList;
        }

        /// <summary>
        /// Get List of Elements found under a supplied element by locator
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static List<IWebElement> GetWindowsElementsUnder(IWebElement parent, By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetWindowsElementsUnder {parent} {locator} ");
            var elementList = new List<IWebElement>();
            try
            {
                var windowElements = parent.FindElements(locator);
                foreach(var element in windowElements)
                {
                    elementList.Add((IWebElement)element);
                }
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"FAILED GET ELEMENTSSsssss {ex}");
            }
            DebugOutput.Log($"FOUND {elementList.Count} windows elements under {parent}");
            return elementList;
        }

        /// <summary>
        /// Get all elements found element by locator - web or windows
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="locator"></param>
        /// <param name="timeout"></param>
        /// <returns>returns list of elements</returns>
        public static List<IWebElement> GetElementsUnder(IWebElement parent, By locator, int timeout = 0)
        {
            DebugOutput.Log($"Sel - GetElementsUnder (plural) {parent} {locator} {timeout}");
            var elementList = new List<IWebElement>();
            var appType = TargetConfiguration.Configuration.ApplicationType.ToLower();
            if (appType == "web")
            {
                return GetWebElementsUnder(parent, locator, timeout);
            }
            if (appType == "windows")
            {
                return GetWindowsElementsUnder(parent, locator, timeout);
            }
            return elementList;
        }

        public static string? GetElementtextDirect(IWebElement element)
        {
            DebugOutput.Log($"Sel - GetElementtextDirect {element} ");
            try
            {
                var text = element.Text;
                if (!string.IsNullOrEmpty(text))
                {
                    DebugOutput.Log($"WE GOT A VALUE FROM .Text = '{text}'");
                    return text;
                }
            }
            catch
            {
                DebugOutput.Log($"Failed to read direct");
            }
            return null;
        }

        /// <summary>
        /// Get the text of an element
        /// Some elements store text as text, value or textcontent.  Will try in that order and return the first that is not null
        /// </summary>
        /// <param name="element"></param>
        /// <returns>returns a string of text or if none found in the 3 attributes, return ""</returns>
        public static string GetElementText(IWebElement element)
        {
            DebugOutput.Log($"Sel - GetElementText {element} ");
            if (element == null) return failedFindElement;
            string? returnText = null; 
            if (TargetConfiguration.Configuration.ApplicationType == "windows")
            {
                DebugOutput.Log($"For WINDOWS Will try Direct first!");
                var direct = GetElementtextDirect(element);
                if (!string.IsNullOrEmpty(direct)) return direct;
                DebugOutput.Log($"For WINDOWS Will try Name Second - it has to have a name!");
                DebugOutput.Log($"Name sometimes gets the value");
                if (!string.IsNullOrEmpty(GetElementAttributeValue(element, "Name"))) return SeleniumUtil.GetElementAttributeValue(element, "textContent");
            }
            try
            {
                returnText = element.Text;
                if (returnText != "0" || returnText != null)
                {
                    DebugOutput.Log($"We have THE TEXT  '{returnText}' from the element {element}");
                    if (returnText != "")
                    {
                        DebugOutput.Log($"We have the non null text being returned of '{returnText}");  
                        return returnText;
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"Failed to get text in direct query!");
            }
            DebugOutput.Log($"Attribute Text");
            if (!string.IsNullOrEmpty(GetElementAttributeValue(element, "text"))) return SeleniumUtil.GetElementAttributeValue(element, "text");
            DebugOutput.Log($"Attribute Value");
            if (!string.IsNullOrEmpty(GetElementAttributeValue(element, "value"))) return SeleniumUtil.GetElementAttributeValue(element, "value");
            DebugOutput.Log($"Attribute textContent");
            if (!string.IsNullOrEmpty(GetElementAttributeValue(element, "textContent"))) return SeleniumUtil.GetElementAttributeValue(element, "textContent");
            DebugOutput.Log($"Attribute original title");
            if (!string.IsNullOrEmpty(GetElementAttributeValue(element, "data-original-title"))) return SeleniumUtil.GetElementAttributeValue(element, "textContent");
            DebugOutput.Log($"FAILED ALL TEXT KNOWN TO MAN, i.e. me!");
            return "";
        }

        /// <summary>
        /// In web you find a tag by tag, but in windows its tagName.  same with Class and Id
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns>returns a string of the correct attribute name for type of test</returns>
        private static string GetCorrectAttributeName(string attributeName)
        {
            var appType = TargetConfiguration.Configuration.ApplicationType.ToLower();
            if (attributeName.ToLower() == "tag")
            {
                return "TagName";
            }
            if (attributeName.ToLower() == "class" || attributeName.ToLower() == "classname" || attributeName.ToLower() == "tagClass")
            {
                if (appType == "windows")
                {
                    return "ClassName";
                }
            }
            if (attributeName.ToLower() == "id")
            {
                if (appType == "windows")
                {
                    return "AutomationId";
                }
            }
            return attributeName;
        }

        /// <summary>
        /// Return a list of expected attributes.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attributes"></param>
        /// <returns>return a list of expected attributes.</returns>
        public static List<string> GetElementMultipleAttributesValues(IWebElement element, List<string> attributes)
        {
            var attributeValueList = new List<string>();
            string modifiedAttributeName;
            foreach (var attribute in attributes)
            {
                modifiedAttributeName = GetCorrectAttributeName(attribute);
                var value = "";
                //Everything has a tag so is not classed as an element
                if (modifiedAttributeName == "TagName")
                {
                    value = element.TagName;
                }
                else
                {
                    value = GetElementAttributeValue(element, modifiedAttributeName);
                }
                if (value.Length < 1)
                {
                    attributeValueList.Add("NULL");
                }
                else
                {
                    attributeValueList.Add(value);
                }
            }
            return attributeValueList;
        }


        /// <summary>
        /// Given an element return a full XPath
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static string GetFullXPathOfElement(IWebElement element)
        {
            DebugOutput.Log($"Sel - GetFullXPathOfElement {element}");
            IWebElement? parentElement;
            string fullXPath = "";
            bool gotFullXPath = false;
            var currentElementTag = GetElementAttributeValue(element, "tag");
            fullXPath = $"{currentElementTag}";
            while (!gotFullXPath)
            {
                parentElement = GetElementParent(element);
                if (parentElement != null)
                {
                    var parentTag = GetElementAttributeValue(parentElement, "tag");
                    fullXPath = parentTag + @"/" + fullXPath;
                    element = parentElement;
                }
                else
                {
                    fullXPath = @"(//" + fullXPath + ")";
                    gotFullXPath = true;    
                }
            }
            return fullXPath;
        }

        /// <summary>
        /// Supply an element and what attribute you want the value of
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attribute"></param>
        /// <returns>return string value of attribute of given element</returns>
        public static string GetElementAttributeValue(IWebElement element, string attribute)
        {
            //DebugOutput.Log($"Sel - GetElementAttributeValue {element} {attribute}");
            string blank = "";
            if (string.IsNullOrEmpty(attribute)) return blank;
            if (attribute.ToLower() == "elementid")
            {
                if (element == null) return blank;
                var str = element.ToString();
                if (str == null) return blank;
                return str;
            }
            if (attribute.ToLower() == "locatorname")
            {
                attribute = "Name";
            }
            if (attribute.ToLower() == "tagname" || attribute.ToLower() == "tag")
            {
                string value = blank;
                try
                {
                    value = element.TagName;
                }
                catch
                {
                    DebugOutput.Log($"There should be no element EVERY with no tag, but we've found one!");
                }
                if (value.Length > 0) return value;
            }
            if (attribute.ToLower() == "tagclass")
            {
                attribute = "class";
            }
            if (attribute.ToLower() == "fullxpath")
            {
                //Get the element - get every element above it
                return GetFullXPathOfElement(element);
            }
            try
            {
                var attributeValue = element.GetAttribute(attribute);
                if (attributeValue == null)
                {
                    DebugOutput.Log($"We read the attribute {attribute} and got a null return! so need to return that!");
                    return blank;
                }
                DebugOutput.Log($"We read the attribute {attribute} and got '{attributeValue}' Which is better than nothing!");
                return attributeValue;
            }
            catch
            {
                DebugOutput.Log($"Failed to read attribute {attribute}");
                return blank;
            }
        }

        /// <summary>
        /// Get the Actions for each type of Driver
        /// </summary>
        /// <returns>Correct action driver for type of application under test</returns>
        private static Actions? GetActions()
        {
            var type = TargetConfiguration.Configuration.ApplicationType.ToLower();
            DebugOutput.Log($"GetActions = {type} ");
            if (type == "web")
            {
                Actions action = new Actions(webDriver);
                return action;
            }
            if (type == "windows")
            {
                Actions action = new Actions(winDriver);
                return action;
            }
            DebugOutput.Log($"Failed to get ACTION!");
            return null;
        }


    }
}
