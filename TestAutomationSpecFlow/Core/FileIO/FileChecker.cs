﻿using Core.Logging;
using Core.Transformations;

namespace Core.FileIO
{
    public class FileChecker
    {


        public static StreamReader GetStream(string pathAndFileName, bool OS = false)
        {
            DebugOutput.Log($"proc GetStream {pathAndFileName}");
            string fullFileName = pathAndFileName;
            if (OS == false)
            {
                fullFileName = GetCorrectDirectory(pathAndFileName);
            }
            StreamReader r = new StreamReader(fullFileName);
            return r;
        }

        public static bool OSDeleteDirectoryIfExists(string directory)
        {
            DebugOutput.Log($"Proc - DeleteDirectoryIfExists {directory}");
            if (!DoesDirectoryExist(directory)) return true;
            DebugOutput.Log($"Need to delete it!");
            return FileChecker.OSDirectoryDeletion(directory);
        }

        public static bool DirectoryCheck(string directory)
        {
            var fullFileName = GetCorrectDirectory(directory);
            DebugOutput.Log($"proc DirectoryCheck {directory}");
            try
            {
                return Directory.Exists(fullFileName);
            }
            catch
            {
                DebugOutput.Log($"Failed to check directory {fullFileName}");
                return false;
            }
        }
        public static bool DoesDirectoryExist(string directory)
        {
            DebugOutput.Log($"Proc - DoesDirectoryExist {directory}");
            return OSDirectoryCheck(directory);
        }

        public static bool FileCheck(string pathAndFileName)
        {
            DebugOutput.Log($"proc FileCheck {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            DebugOutput.Log($"WE IS LOOKING FOR {fullFileName}");
            return File.Exists(fullFileName);
        }

        public static string? GetFileContents(string pathAndFileName)
        {
            DebugOutput.Log($"proc GetFileContents {pathAndFileName} ");
            string readContents = "";
            using (StreamReader streamReader = new StreamReader(pathAndFileName))
            {
                readContents = streamReader.ReadToEnd();                
            }
            if (readContents == "") return null;
            return readContents;
        }

        public static bool FileContains(string pathAndFileName, string findString)
        {
            DebugOutput.Log($"proc FileContains {pathAndFileName} {findString}");
            if (pathAndFileName == null) return false;
            if (findString == null) return false;
            if (!File.Exists(pathAndFileName)) return false;
            DebugOutput.Log($"File exists!");
            var readContents = GetFileContents(pathAndFileName);
            if (readContents == null) return false;
            if (readContents.Contains(findString)) return true;
            DebugOutput.Log($"Have string contents of file - but failed to find!");
            return false;
        }

        public static bool FileDeletion(string pathAndFileName)
        {
            bool deleted = false;
            DebugOutput.Log($"proc FileDeletion {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            if (FileCheck(pathAndFileName))
            {
                try
                {
                    DebugOutput.Log($"STARTING DELETE! {fullFileName}");
                    File.Delete(fullFileName);
                    deleted = true;
                    DebugOutput.Log($"DELETING! {fullFileName}");
                    return deleted;
                }
                catch (Exception ex)
                {
                    DebugOutput.Log($"Failed to delete file {fullFileName} reason = {ex}");
                    return deleted;
                }
            }
            DebugOutput.Log($"File {fullFileName} does not exist - so can not delete, but still passed!");
            deleted = true;
            return deleted;
        }

        public static string? GetFileContentsAsString(string pathAndFileName)
        {
            DebugOutput.Log($"proc GetFileContentsAsString {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            return OSGetFileContentsAsString(fullFileName);
        }

        public static string? OSGetFileContentsAsString(string fullPathAndFileName)
        {
            DebugOutput.Log($"proc OSGetFileContentsAsString {fullPathAndFileName}");
            try
            {
                return File.ReadAllText(fullPathAndFileName);
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to read all text in {fullPathAndFileName} {ex}.");
                return null;
            }
        }

        public static bool DirectoryCreation(string pathAndFileName)
        {
            DebugOutput.Log($"proc DirectoryCreation {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            try
            {
                Directory.CreateDirectory(fullFileName);
                DebugOutput.Log($"Directory created {fullFileName}");
                return true;
            }
            catch
            {
                DebugOutput.Log($"an issue with creating directory {fullFileName}");
                return false;
            }
        }

        public static bool FilePopulate(string pathAndFileName, string text)
        {
            DebugOutput.Log($"proc FilePopulate {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            DebugOutput.Log($"OUTPUTTING TO {fullFileName}");
            try
            {
                File.WriteAllText(fullFileName, text);
                DebugOutput.Log($"CREATED FILE {fullFileName}");
                return true;
            }
            catch
            {
                DebugOutput.Log($"FAILED TO CREATE FILE {fullFileName}");
                return false;
            }
        }

        public static bool FileCreation(string pathAndFileName)
        {
            DebugOutput.Log($"proc FileCreation {pathAndFileName}");
            var fullFileName = GetCorrectDirectory(pathAndFileName);
            try
            {
                File.Create(pathAndFileName);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to create file {pathAndFileName}");
                return false;
            }
        }

        public static bool OSFileCreation(string pathAndFileName)
        {
            DebugOutput.Log($"proc OSFileCreation {pathAndFileName}");
            try
            {
                File.Create(pathAndFileName);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to create file {pathAndFileName}");
                return false;
            }
        }

        public static bool CopyDirectoryFromProjectToOS(string projectDirectory, string osDirectory)
        {
            DebugOutput.Log($"proc MoveDirectoryFromProjectToOS {projectDirectory} to {osDirectory}");
            projectDirectory = GetCorrectDirectory(projectDirectory);
            return CopyDirectory(projectDirectory, osDirectory);
        }

        public static bool CopyDirectory(string sourceDir, string destinationDir, bool recursive = true)
        {
            DebugOutput.Log($"proc CopyDirectory {sourceDir} to {destinationDir} recursive {recursive}");
            // Get information about the source directory
            var dir = new DirectoryInfo(sourceDir);
            // Check if the source directory exists
            if (!dir.Exists) return false;

            // Cache directories before we start copying
            DirectoryInfo[] dirs = dir.GetDirectories();
            // Create the destination directory
            Directory.CreateDirectory(destinationDir);
            DebugOutput.Log($"Attempt copy!");
            try
            {
                // Get the files in the source directory and copy to the destination directory
                foreach (FileInfo file in dir.GetFiles())
                {
                    string targetFilePath = Path.Combine(destinationDir, file.Name);
                    file.CopyTo(targetFilePath);
                }
            }
            catch
            {
                DebugOutput.Log($"Problem with the Copy of Files!");
                return false;
            }
            try
            {
                DebugOutput.Log($"Attempt recusive");
                // If recursive and copying subdirectories, recursively call this method
                if (recursive)
                {
                    foreach (DirectoryInfo subDir in dirs)
                    {
                        string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                        CopyDirectory(subDir.FullName, newDestinationDir, true);
                    }
                }
            }
            catch
            {
                DebugOutput.Log($"FAiled on recursive");
                return false;
            }
            DebugOutput.Log($"COPY COMPELTE!");
            return true;
        }

        public static bool MoveRenameFile(string pathAndFileName, string newPathAndFileName)
        {
            DebugOutput.Log($"proc MoveRenameFile {pathAndFileName} to {newPathAndFileName}");
            bool moved = false;
            if (FileCheck(newPathAndFileName))
            {
                DebugOutput.Log($"Moving a file where a file already exists!");
                if (!FileDeletion(newPathAndFileName))
                {
                    DebugOutput.Log($"FAILED TO DELETE!");
                    return moved;
                }
            }
            var oldFile = GetCorrectDirectory(pathAndFileName);
            var newFile = GetCorrectDirectory(newPathAndFileName);
            try
            {
                File.Move(oldFile, newFile);
                moved = true;
                DebugOutput.Log($"Move completed {oldFile} has moved to {oldFile}");
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Has been an issue moving the files! {ex}");
            }
            return moved;
        }

        public static bool OSDirectoryDeletion(string fullPath)
        {
            bool deleted = false;
            DebugOutput.Log($"proc OSDirectoryDeletion {fullPath}");
            if (OSDirectoryCheck(fullPath))
            {
                DebugOutput.Log($"Directory Exists!  TO DELETE");
                try
                {
                    Directory.Delete(fullPath, true);
                    deleted = true;
                    DebugOutput.Log($"DELETED! {fullPath}");
                    return deleted;
                }
                catch (Exception ex)
                {
                    DebugOutput.Log($"problem deleting directory!  {fullPath} = {ex}");
                    return deleted;
                }
            }
            DebugOutput.Log($"Directory {fullPath} no longer exists - so maybe ALREADY deleted!");
            deleted = true;
            return deleted;
        }

        public static bool OSDirectoryCheck(string fullPath)
        {
            bool exists = false;
            DebugOutput.Log($"proc OSDirectoryCheck {fullPath}");
            try
            {
                exists = Directory.Exists(fullPath);
                return exists;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to find directory {fullPath} {ex}");
                return exists;
            }
        }

        public static bool OSFileCheck(string fullFileName)
        {
            DebugOutput.Log($"proc OSFileCheck {fullFileName}");
            bool exists = false;
            try
            {
                exists = File.Exists(fullFileName);
                return exists;
            }
            catch (Exception ex)
            {
                DebugOutput.Log($"Failed to file {ex}");
                return exists;
            }
        }

        public static bool OSFileDeletion(string fullName)
        {
            bool deleted = false;
            DebugOutput.Log($"proc OSFileDeletion {fullName}");
            if (OSFileCheck(fullName))
            {
                DebugOutput.Log($"File Exists!  TO DELETE");
                try
                {
                    File.Delete(fullName);
                    deleted = true;
                    DebugOutput.Log($"DELETED! {fullName}");
                    return deleted;
                }
                catch (Exception ex)
                {
                    DebugOutput.Log($"problem deleting!  {fullName} = {ex}");
                    return deleted;
                }
            }
            DebugOutput.Log($"File does not exist! So can not delete something not there - but then! ITS GONE ANYWAY!");
            deleted = true;
            return deleted;
        }

        public static bool OSDeleteFilesOfTypeInADirectory(string directory, string fileExtension = "log")
        {
            DebugOutput.Log($"OSDeleteFilesOfTypeInADirectory {directory} {fileExtension}");
            var listOfFilesInDirectory = OSGetListOfFilesInDirectory(directory, fileExtension);
            foreach (var file in listOfFilesInDirectory)
            {
                if (file.ToLower().Contains(fileExtension.ToLower()))
                {
                    var fullFile = directory + file;
                    DebugOutput.Log($"Deleting {fullFile}");
                    try
                    {
                        OSFileDeletion(fullFile);
                    }
                    catch
                    {
                        DebugOutput.Log($"Failed to delete {fullFile}");
                        return false;
                    }
                }
            }
            return true;
        }

        public static bool OSDeleteFilesOfADirectory(string directory)
        {
            DebugOutput.Log($"OSDeletFilesOfADirectory {directory}");
            var listOfFilesInDirectory = OSGetListOfFilesInDirectory(directory);
            foreach(var file in listOfFilesInDirectory)
            {
                var fullFile = directory + file;
                DebugOutput.Log($"Deleting {fullFile}");
                try
                {
                    OSFileDeletion(fullFile);
                }
                catch
                {
                    DebugOutput.Log($"Failed to delete {fullFile}");
                    return false;
                }
            }
            return true;
        }

        public static bool OSDeleteContentsOfADirectory(string directory)
        {
            DebugOutput.Log($"OSDeleteContentsOfADirectory {directory}");
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(directory);
                foreach (FileInfo file in di.EnumerateFiles())
                {
                    DebugOutput.Log($"File = {file.FullName}");
                    file.Delete(); 
                }
                foreach (DirectoryInfo dir in di.EnumerateDirectories())
                {
                    dir.Delete(true); 
                }
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed cause of some reason;");
                return false;
            }
        }

        public static List<string> OSGetListOfFilesInDirectoryOfTypeContainingWord(string directory, string extension = "", string findString = "")
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectoryOfTypeContainingWord {directory}  {extension} {findString}");
            List<string> fileNames = new List<string>();
            DirectoryInfo d = new DirectoryInfo(directory); 
            string fileExtension = "*";
            if (fileExtension != "")
            {
                fileExtension = "*." + extension;
            }
            FileInfo[] Files = d.GetFiles(fileExtension); //Getting files
            foreach(FileInfo file in Files)
            {
                if (findString == "")
                {
                    fileNames.Add(file.Name);
                }
                else
                {
                    if (file.Name.ToLower().Contains(findString.ToLower()))
                    {
                        fileNames.Add(file.Name);
                    }
                    else
                    {
                        DebugOutput.Log($"Not added as the file name {file.Name} does not inclued {findString}");
                    }
                }
            }
            DebugOutput.Log($"Returing {fileNames.Count} File Names");
            return fileNames;


        }

        public static List<string> OSGetListOfFilesInDirectoryOfTypeContainStringInName(string directory, string extension = "", string containString = "")
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectoryOfTypeContainStringInName {directory} {extension} {containString}");   
            var foundFileNames = new List<string>();     
            var listOfLogFiles = FileChecker.OSGetListOfFilesInDirectoryOfType(directory, extension);
            foreach (var fileName in listOfLogFiles)
            {
                DebugOutput.Log($"File Name = {fileName} does it contain {containString}");
                if (fileName.Contains(containString))
                {
                    foundFileNames.Add(fileName);
                }
            }
            return foundFileNames;
        }

        public static List<string> OSGetListOfFilesInDirectoryOfType(string directory, string extension = "")
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectoryOfType {directory}");
            return OSGetListOfFilesInDirectoryOfTypeContainingWord(directory, extension);
        }

        public static List<string> OSGetListOfJsonFilesInDirectory(string directory)
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectory {directory}");
            List<string> fileNames = new List<string>();
            fileNames = OSGetListOfFilesInDirectoryOfType(directory, "json");
            return fileNames;
        }

        public static List<string> OSGetListOfDirectoriesInDirectory(string directory)
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectory {directory}");
            var directoryInformation = new DirectoryInfo(directory);
            var listOfDirectories = new List<string>();
            foreach (var dir in directoryInformation.GetDirectories())
            {
                listOfDirectories.Add(dir.FullName);
            }
            return listOfDirectories;
        }


        public static List<string> OSGetListOfFilesInDirectory(string directory, string extension = "json")
        {
            DebugOutput.Log($"OSGetListOfFilesInDirectory {directory}");
            List<string> fileNames = new List<string>();
            fileNames = OSGetListOfFilesInDirectoryOfType(directory, extension);
            return fileNames;
        }

        public static bool OSMoveFileFromTo(string fullFileName, string moveToDirectory)
        {
            DebugOutput.Log($"Proc - OSMoveFileFromTo {fullFileName} to {moveToDirectory}");
            var fileName = Path.GetFileName(fullFileName);
            DebugOutput.Log($"File name = {fileName}");
            var newFileName = moveToDirectory + fileName;
            DebugOutput.Log($"New name = {fileName}");
            try
            {
                File.Move(fullFileName, newFileName, false);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failed to move - CAN NOT OVERWRITE!");
                return false;
            }
        }

        public static bool OSRenameDirectory(string oldpathAndFileName, string newpathAndFileName)
        {
            DebugOutput.Log($"Proc - RenameDirectory {oldpathAndFileName} to {newpathAndFileName}");
            try
            {
                Directory.Move(oldpathAndFileName, newpathAndFileName);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failure in renaming Directory");
            }
            return false;   
        }

        public static bool OSRenameFile(string oldpathAndFileName, string newpathAndFileName)
        {
            DebugOutput.Log($"Proc - OSRenameFile {oldpathAndFileName} to {newpathAndFileName}");
            try
            {
                File.Move(oldpathAndFileName, newpathAndFileName);
                return true;
            }
            catch
            {
                DebugOutput.Log($"Failure in renaming File");
            }
            return false;

        }

        public static string OSGetAllTextInFile(string pathAndFileName)
        {
            string allText = "";
            try
            {
                allText = File.ReadAllText(pathAndFileName);
            }
            catch
            {
                DebugOutput.Log($"Failed to read ll text of {pathAndFileName}");
                return "";
            }
            return allText;
        }

        public static bool OSReplaceFullTextInFile(string pathAndFileName, string allText)
        {
            DebugOutput.Log($"Proc - OSReplaceFullTextInFile {pathAndFileName} ");
            try
            {
                File.WriteAllText(pathAndFileName, allText);
                DebugOutput.Log($"{pathAndFileName} is updated!");
                return true;
            }
            catch
            {
                DebugOutput.Log($"Problem writing the allText with updates to file {pathAndFileName}");
                return false;
            }
        }

        public static bool OSReplaceTextInFile(string pathAndFileName, string toBeReplaced = "", string withWhat = "")
        {
            DebugOutput.Log($"Proc - OSReplaceTextInFile {pathAndFileName} to {toBeReplaced} with {withWhat}");
            string allText = OSGetAllTextInFile(pathAndFileName);
            if (allText == "")
            {
                DebugOutput.Log($"File is EMPTY!");
                return false;
            }
            allText = StringValues.TextReplacementService(allText);
            if (toBeReplaced != "")
            {
                if (withWhat != "")
                {
                    try
                    {
                        allText = allText.Replace(toBeReplaced, withWhat);
                    }
                    catch
                    {
                        DebugOutput.Log($"Failed on the single text replacement in file!");
                    }
                }
                else
                {
                    DebugOutput.Log($"FAILED TO REPLACE with NOTHING!  can not do this in this method!");
                    return false;
                }
            }
            return OSReplaceFullTextInFile(pathAndFileName, allText);
        }

        ///PRIVATE

        private static string GetCorrectDirectory(string pathAndFileName)
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            DebugOutput.Log($"Current Directory is {currentDirectory}");
            if (currentDirectory.Contains("bin") && currentDirectory.Contains("Debug"))
            {
                DebugOutput.Log($"Need to move up directory");
                MoveUpToProjectDirectory();
            }
            currentDirectory = Directory.GetCurrentDirectory();
            var fullFileName = currentDirectory + pathAndFileName;
            DebugOutput.Log($"RETURNING FOR FULL NAME {fullFileName}");
            return fullFileName;
        }

        private static void MoveUpToProjectDirectory()
        {
            Environment.CurrentDirectory = "../../../../";
            DebugOutput.Log($"New Directory is { Directory.GetCurrentDirectory()}");
        }

    }
}
