using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Core.Transformations.PageModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.Drawing;
using System.Diagnostics;

namespace Core
{
    public static class CmdUtil
    {
        public static void ExecuteCommand (string command)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            var process = Process.Start(processInfo);
            if (process != null)
            {
                process.OutputDataReceived += (object sender, DataReceivedEventArgs e) =>
                    DebugOutput.Log("output>>" + e.Data);
                process.BeginOutputReadLine();

                process.ErrorDataReceived += (object sender, DataReceivedEventArgs e) =>
                    DebugOutput.Log("error>>" + e.Data);
                process.BeginErrorReadLine();

                process.WaitForExit();

                DebugOutput.Log($"ExitCode: {0} {process.ExitCode}");
                process.Close();

            }
        }
    }
}