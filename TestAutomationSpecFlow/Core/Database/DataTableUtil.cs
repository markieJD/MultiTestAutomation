using Core.Logging;
using System;
using System.Data;

namespace Core
{
    public static class DataTableUtil
    {
        public static void Hello(DataTable test)
        {
            foreach (DataRow r in test.Rows)
            {
                DebugOutput.Log($"TEST: {r[0]} \t {r[1]}");
            }            
        }

        public static bool DoesValueExistsInColumn(DataTable table, string columnName, string value)
        {
            DebugOutput.Log($"proc - DataTableUtil - DoesValueExistsInColumn {table} {columnName} {value}");
            DebugOutput.Log($"WE CURRENTLY HAVE {table.Rows.Count}") ;
            DataView dv = new DataView(table);
            dv.RowFilter = $"{columnName} = '{value}'";
            if (dv.Count > 0) return true;
            DebugOutput.Log($"FAiled to find");
            return false;
        }

        public static DataView GetFilteredDataWhereColoumIncludesValue(DataTable table, string columnName, string value)
        {
            DebugOutput.Log($"proc - DataTableUtil - GetFilteredDataWhereColoumIncludesValue {table} {columnName} {value}");
            DebugOutput.Log($"WE CURRENTLY HAVE {table.Rows.Count}") ;
            DataView dv = new DataView(table);
            dv.RowFilter = $"{columnName} = '{value}'";
            return dv;
        }
    }
}