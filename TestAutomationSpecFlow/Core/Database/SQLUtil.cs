
using Core.Configuration;
using Core.Logging;
using Core.Transformations;
using Core.Transformations.PageModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

namespace Core
{
    public class ConnectionDetails
    {
        public string? dataSource { get; set; }
        public string? initialCatalog { get; set; }
        public string? id { get; set; }
        public string? password { get; set; }
        public int? connectTimeout { get; set; }
        public bool? encrypt { get; set; }
        public bool? trustServerCertificate { get; set; }
        public string? applicationIntent { get; set; }
        public bool? multiSubnetFailover { get; set; }
        public bool? multipleActiveResultSets { get; set; }
    }

    public static class SQLUtil
    {

        private static ConnectionDetails? GetConnectionDetails()
        {
            var thisConnection = new ConnectionDetails();
            var jSonText = JsonValues.ReadDatabaseResourceFile("SQLSettings.json");
            if (jSonText == null) return null;
            thisConnection = JsonConvert.DeserializeObject<ConnectionDetails>(jSonText);
            return thisConnection;            
        }

        private static string? GetConnectionString(ConnectionDetails connectionDetails)
        {
            DebugOutput.Log($"GetConnectionString");
            string returnString = "Data Source = " + connectionDetails.dataSource + ";Initial Catalog = " + connectionDetails.initialCatalog + ";User ID = " + connectionDetails.id + ";Password = " + connectionDetails.password;
            if (connectionDetails.connectTimeout != null) returnString = returnString + ";Connect Timeout=" + connectionDetails.connectTimeout;
            if (connectionDetails.encrypt != null) returnString = returnString + ";Encrypt=" + connectionDetails.encrypt;
            if (connectionDetails.trustServerCertificate != null) returnString = returnString + ";TrustServerCertificate=" + connectionDetails.trustServerCertificate;
            if (connectionDetails.applicationIntent != null) returnString = returnString + ";ApplicationIntent=" + connectionDetails.applicationIntent;
            if (connectionDetails.multiSubnetFailover != null) returnString = returnString + ";MultiSubnetFailover=" + connectionDetails.multiSubnetFailover;
            if (connectionDetails.multipleActiveResultSets != null) returnString = returnString + ";MultipleActiveResultSets=" + connectionDetails.multipleActiveResultSets;
            DebugOutput.Log($"Connection string = {returnString}");
            return returnString;
        }

        public static DataTable? GetTableOfResults(string sqlCommand)
        {
            DebugOutput.Log($"GetTableOfResults {sqlCommand}");
            var table = new DataTable();            
            var connectionDetails = GetConnectionDetails();
            if (connectionDetails == null)
            {
                DebugOutput.Log($"We are not able to get connection details - check SQLSettings.Json in Resources!");
                return null;
            }
            var connectionString = GetConnectionString(connectionDetails);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlCommand, connection))
                {
                    connection.Open();  
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        table.Load(reader);
                        DebugOutput.Log(table.Columns.Count + " columns");
                        DebugOutput.Log(table.Rows.Count + " ROWS");
                    }
                }
            }
            return table;
        }


        public static void Hello()
        {
            var connectionDetails = GetConnectionDetails();
            if (connectionDetails == null)
            {
                DebugOutput.Log($"We are not able to get connection details - check SQLSettings.Json in Resources!");
                return;
            }
            var connectionString = GetConnectionString(connectionDetails);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("select * from AtlasPNDStaging.Reference.VehicleColour", connection))
                {
                    connection.Open();  
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var table = new DataTable();
                        table.Load(reader);
                        DebugOutput.Log(table.Columns.Count + " columns");
                        DebugOutput.Log(table.Rows.Count + " ROWS");

                    }
                }
            }

            

        }


    }
}